<?php

/**
 * @file
 * Views hooks implemented for the Spider Video Player module.
 */

/**
 * Implements hook_views_data_alter().
 */
function spider_video_player_views_data_alter(&$data) {
  $data['node']['spider_video_player_player_view'] = array(
    'group' => t('Spider Video Player'),
    'field' => array(
      'title' => t('Video Player'),
      'help' => t('The Video Player display for this node.'),
      'handler' => 'spider_video_player_handler_field_player_view',
    ),
  );
}

/**
 * Implements hook_views_handlers().
 */
function spider_video_player_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'spider_video_player') . '/views',
    ),
    'handlers' => array(
      'spider_video_player_handler_field_player_view' => array(
        'parent' => 'views_handler_field',
        'file' => 'spider_video_player_handler_field_player_view.inc',
      ),
    ),
  );
}
