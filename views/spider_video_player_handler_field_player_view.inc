<?php

/**
 * @file
 * Views handler to display the content of a Spider Video Player.
 */

/**
 * Field handler to present the Video Player to the user.
 */
class spider_video_player_handler_field_player_view extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
  }
  function option_definition() {
    $options = parent::option_definition();
    $options['label'] = array('default' => 'Player', 'translatable' => TRUE);
    return $options;
  }
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
  function render($values) {
    $node = node_load($values->{$this->aliases['nid']});
    if (node_access('view', $node)) {
      // Populate $node->content['spider_video_player'] by reference.
      spider_video_player_node_view($node, 'full');
      // $video_player = isset($node->content['spider_video_player']) ? $node->content['spider_video_player'] : NULL;
      // $row = db_query("SELECT * FROM {spider_video_player_form_table} WHERE vid=:vid", array(':vid' => $node->nid))->fetchObject();
      // if ($row->priority == 1) {
      $video_player = isset($node->content['spider_video_player']) ? $node->content['spider_video_player'] : NULL;
      // }
      // else {
        // $theme_row = db_query("SELECT * FROM {spider_video_player_theme} WHERE id=:id", array(':id' => $row->theme))->fetchObject();
        // $video_player = '<br /><iframe style="width:' . $theme_row->appWidth . 'px;height:' . $theme_row->appHeight . 'px;border:none;" id="' . $node->nid . '" src="' . url('spider_video_player/html5', array('query' => array('theme' => $row->theme, 'playlist' => $row->playlist), 'absolute' => TRUE)) . '"></iframe>
                         // <script>
                          // jQuery("iframe").load( function() {
                            // jQuery("iframe").contents().find("head").append(jQuery("<style> body {margin:0;overflow:hidden;} </style>"));
                          // });
                         // </script>';
      // }
    }
    else {
      return;
    }
    if (strpos(current_path(), 'view')) {
      return NULL;
    }
    else {
      return $video_player;
    }
  }
}
