﻿package {
	import view.View;

	import flash.display.LoaderInfo;
	import flash.display.Sprite;

	/**
	 * @author Robert Soghbatyan (robert.sogbatyan@gmail.com)
	 */
	public class VideoPlayer extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _settingsUrl : String; //url of settings .xml file
		private var _playlistUrl : String; //url of playlists .xml file
		private var _trackUrl : String; //url of single track
		private var _defaultAlbumId : String; //url of single track
		private var _defaultTrackId : String; //url of single track

		private var _controller : Controller; //instance of controller
		private var _view : View; //view


		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		public function VideoPlayer() : void {			
			//getting parameters from param object
			var paramObj : Object = LoaderInfo(this.root.loaderInfo).parameters;
			_settingsUrl = paramObj["settingsUrl"] == undefined ? "" : String(paramObj["settingsUrl"]).split("@").join("&");
			_playlistUrl = paramObj["playlistUrl"] == undefined ? "" : String(paramObj["playlistUrl"]).split("@").join("&");
			_trackUrl = paramObj["trackUrl"] == undefined ? "" : String(paramObj["trackUrl"]);
			_defaultAlbumId = paramObj["defaultAlbumId"] == undefined ? "" : String(paramObj["defaultAlbumId"]);
			_defaultTrackId = paramObj["defaultTrackId"] == undefined ? "" : String(paramObj["defaultTrackId"]);
			
			//initing controller and view
			_controller = Controller.getInstance();
			_view = new View(this.root.stage);
			
			_controller.init(this.root.stage, _settingsUrl, _playlistUrl, _trackUrl, _defaultAlbumId, _defaultTrackId);
		}
	}
}