package view.VideoFrame {
	import model.Playlist;

	import utils.Library;

	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;

	/**
	 * @author Robert Soghbatyan (robert.sogbatyan@gmail.com)
	 */
	public class ShareIconsView {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller :Controller;
		private var _playList : Playlist;
			
		private var _sprite : Sprite;
		
		private var _fbIcon : Sprite;
		private var _twIcon : Sprite;
		private var _gIcon : Sprite;
		private var _embedIcon : Sprite;
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		public function ShareIconsView() : void {
			_controller = Controller.getInstance();
			_playList = _controller.playlist;
			
			_sprite = Library.createSprite("shareIcons");
			
			_fbIcon = Sprite(_sprite.getChildByName("fb"));
			_twIcon = Sprite(_sprite.getChildByName("tw"));
			_gIcon = Sprite(_sprite.getChildByName("g"));
			_embedIcon = Sprite(_sprite.getChildByName("embed"));
			
			_fbIcon.addEventListener(MouseEvent.CLICK, clickHandler);
			_twIcon.addEventListener(MouseEvent.CLICK, clickHandler);
			_gIcon.addEventListener(MouseEvent.CLICK, clickHandler);
			_embedIcon.addEventListener(MouseEvent.CLICK, clickHandler);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		// Getters & Setters                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		public function get sprite() : Sprite {
			return _sprite;
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Private Methods                                                                    //
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private function clickHandler(event : MouseEvent) : void {
			switch (event.target) {
				case _fbIcon:
					ExternalInterface.call("flashShare", "fb", _playList.curAlbumId, _playList.curTrackId);
					break;
				case _twIcon:
					ExternalInterface.call("flashShare", "tw", _playList.curAlbumId, _playList.curTrackId);
					break;
				case _gIcon:
					ExternalInterface.call("flashShare", "g", _playList.curAlbumId, _playList.curTrackId);
					break;
				case _embedIcon:
					_controller.showEmbedPopUp();
					break;
			}
		}
	}
}
