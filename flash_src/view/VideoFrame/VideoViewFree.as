﻿package view.VideoFrame {
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import graphics.Rect;

import model.Settings;

import uiElements.media.Video.VideoLoader;
import uiElements.media.Video.VideoLoaderEvent;
import uiElements.media.Video.VideoTypes;

import utils.Library;

/**
 * @author Family
 */
public class VideoViewFree extends Sprite {
    ////////////////////////////////////////////////////////////////////////////////////////
    // Constants                                                                          //
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    // Variables                                                                          //
    ////////////////////////////////////////////////////////////////////////////////////////
    private var _controller : Controller;

    private var _width : Number;
    private var _height : Number;

    private var _videoSprite : Sprite;

    private var _bg : Rect;

    private var _video : VideoLoader;

    private var _centerBtn : MovieClip; //center play / pause btn
    private var _centerBtnAlpha : Number; //center btn alpha

    private var _loadingClip : Sprite; //loading anim

    private var _watermark : Sprite;

    private var _watermarkPos : uint; //watermark pos
    private var _watermarkSize : Number; //watermark width / height
    private var _watermarkSpacing : Number; //watermark distance from corner
    private var _watermarkAlpha : Number; //watermark alpha

    private var _videoPopUp : VideoPopUp;


    ////////////////////////////////////////////////////////////////////////////////////////
    // Constructor                                                                        //
    ////////////////////////////////////////////////////////////////////////////////////////

    public function VideoViewFree(width : Number, height : Number) : void {
        _controller = Controller.getInstance();

        _width = width;
        _height = height;

        var keepAspectRatio : Boolean = _controller.settings.keepAspectRatio;

        _centerBtnAlpha = _controller.settings.centerBtnAlpha;


		_watermarkPos = Settings.WATERMARK_POS_B_R;
        _watermarkSize = 80;
        _watermarkSpacing = 20;
        _watermarkAlpha = 0.8;

        var clickOnVid : Boolean = _controller.settings.clickOnVid;
        var spaceOnVid : Boolean = _controller.settings.spaceOnVid;

        _videoSprite = new Sprite();
        addChild(_videoSprite);

        var bgColor : int = _controller.settings.vidBgColor;
        _bg = new Rect();
        _bg.fillColor = bgColor == -1 ? 0x000000 : bgColor;
        _bg.fillAlpha = bgColor == -1 ? 0 : 1;
        _bg.width = _width;
        _bg.height = _height;
        _videoSprite.addChild(_bg);

        _video = new VideoLoader();
        _video.autoplay = _controller.settings.autoPlay;
        _video.width = _width;
        _video.height = _height;
        _video.keepAspectRatio = keepAspectRatio;
        _videoSprite.addChild(_video);

        var loadingAnimType : String = _controller.settings.loadingAnimType;
        _loadingClip = Library.createSprite("loadingAnim" + loadingAnimType);
        _loadingClip.mouseEnabled = false;
        _loadingClip.mouseChildren = false;
        _loadingClip.x = _width / 2;
        _loadingClip.y = _height / 2;
        _loadingClip.visible = false;
        addChild(_loadingClip);

        //centerBtn
        if (_centerBtnAlpha != 0) {
            _centerBtn = Library.createMC("centerBtn");
            _centerBtn.mouseEnabled = false;
            _centerBtn.mouseChildren = false;
            _centerBtn.x = _width / 2;
            _centerBtn.y = _height / 2;
            _centerBtn.alpha = _centerBtnAlpha;
            _centerBtn.gotoAndStop("playEnd");
            addChild(_centerBtn);
        }

        //creating watermark
        _watermark = Library.createSprite("watermark");
        _watermark.buttonMode = true;
        _watermark.addEventListener(MouseEvent.CLICK, watermarkClickHandler);
        _watermark.x = _width - _watermark.width - _watermarkSpacing;
        _watermark.y = _height - _watermark.height - _watermarkSpacing;
        _watermark.alpha = _watermarkAlpha;
        addChild(_watermark);


        _videoPopUp = new VideoPopUp();
        addChild(_videoPopUp.sprite);


        //listeners
        _video.addEventListener(VideoLoaderEvent.VIDEO_LOADING, videoListener);
        _video.addEventListener(VideoLoaderEvent.VIDEO_LOADING_FAILED, videoListener);
        _video.addEventListener(VideoLoaderEvent.VIDEO_START, videoListener);
        _video.addEventListener(VideoLoaderEvent.VIDEO_PLAY, videoListener);
        _video.addEventListener(VideoLoaderEvent.VIDEO_PAUSE, videoListener);
        _video.addEventListener(VideoLoaderEvent.VIDEO_STOP, videoListener);
        _video.addEventListener(VideoLoaderEvent.VIDEO_SEEK, videoListener);
        _video.addEventListener(VideoLoaderEvent.VIDEO_END, videoListener);
        _video.addEventListener(VideoLoaderEvent.VIDEO_BUFFER_EMPTY, videoListener);
        _video.addEventListener(VideoLoaderEvent.VIDEO_BUFFER_FULL, videoListener);
        _video.addEventListener(VideoLoaderEvent.VIDEO_VOL, videoListener);

        //click on vid
        if (clickOnVid == true) {
            _videoSprite.addEventListener(MouseEvent.CLICK, clickOnVidListener);
        }

        //space on vid, when added to stagte
        if (spaceOnVid == true) {
            _controller.stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownListener);
        }

        _videoPopUp.addEventListener(VideoPopUp.POP_UP_REPLAY, popUpHandler);
        _videoPopUp.addEventListener(VideoPopUp.POP_UP_NEXT, popUpHandler);

        arrange();
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Public Methods                                                                     //
    ////////////////////////////////////////////////////////////////////////////////////////

    //setting width and height
    public function setWidth(width : Number) : void {
        _width = width;
        arrange();
    }

    public function setHeight(height : Number) : void {
        _height = height;
        arrange();
    }


    //load
    public function load(url : String, type : String = "", pos : Number = 0, fmsUrl : String = "") : void {
        if ((type != VideoTypes.RTMP) && (type != VideoTypes.YOUTUBE)) {
            type = VideoTypes.HTTP;
        }

        _videoPopUp.hide();

        if (url == "") {
            _loadingClip.visible = false;
            _videoPopUp.showStreamNotFoundError();

            if (_centerBtn != null) {
                _centerBtn.gotoAndStop("playEnd");
            }
        } else {
            _loadingClip.visible = true;
            _video.load(type, url, pos, fmsUrl);
            _video.autoplay == true ? _video.play() : _video.pause();
        }
    }

    //play
    public function play() : void {
        if (_videoPopUp.isOpened == true) {
            _video.replay();
        } else {
            _video.play();
        }

        if ((_centerBtn != null) && (_centerBtn.currentLabel == "play")) {
            _centerBtn.play();
        }
    }

    //pause
    public function pause() : void {
        _video.pause();
    }

    //stop
    public function stop() : void {
        _video.stop();
    }

    //seek
    public function seek(offset : Number) : void {
        if (offset > 0.95) {
            offset = 0.95;
        }

        _video.seekToPos(offset);

        if ((_centerBtn != null) && (_centerBtn.currentLabel == "play")) {
            _centerBtn.play();
        }
    }

    //for you tube video. switch hd
    public function youTubeSwitchHD(hd : Boolean) : Boolean {
        var qualitiesCount : int = _video.getQualitiesCount();

        if (qualitiesCount == 0) {
            return false;
        }

        if (hd == true) {
            _video.setQualityLevel(qualitiesCount - 1);
        } else {
            _video.setQualityLevel(0);
        }

        return true;
    }

    public function showAlbumIsEmptyError() : void {
        _loadingClip.visible = false;

        _video.clear();

        _videoPopUp.showAlbumIsEmptyError();
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Getters & Setters                                                                  //
    ////////////////////////////////////////////////////////////////////////////////////////
    public function get video() : VideoLoader {
        return _video;
    }

    //media type
    public function get mediaType() : String {
        return _video.type;
    }

    //volume
    public function get vol() : Number {
        return _video.vol;
    }

    public function set vol(vol : Number) : void {
        _video.vol = vol;
    }

    //getting time and duration
    public function get time() : Number {
        return _video.time;
    }

    public function get duration() : Number {
        return _video.duration;
    }

    //getting video pos and loaded percent
    public function get pos() : Number {
        return _video.pos;
    }

    public function get loaded() : Number {
        return _video.loaded;
    }


    //is video playing or not
    public function get isPlaying() : Boolean {
        return _video.isPlaying;
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Private Methods                                                                    //
    ////////////////////////////////////////////////////////////////////////////////////////

    //arranging video
    private function arrange(e : Event = null) : void {
        _bg.width = _width;
        _bg.height = _height;

        if (_video != null) {
            _video.width = _width;
            _video.height = _height;
        }

        //center btn
        if (_centerBtn != null) {
            _centerBtn.x = _width / 2;
            _centerBtn.y = _height / 2;
        }

        //loading anim
        _loadingClip.x = _width / 2;
        _loadingClip.y = _height / 2;

        //watermark
        if (_watermark != null) {
            _watermark.x = ((_watermarkPos == Settings.WATERMARK_POS_B_L) || (_watermarkPos == Settings.WATERMARK_POS_T_L)) ? _watermarkSpacing : _width - _watermark.width - _watermarkSpacing;
            _watermark.y = ((_watermarkPos == Settings.WATERMARK_POS_T_L) || (_watermarkPos == Settings.WATERMARK_POS_T_R)) ? _watermarkSpacing : _height - _watermark.height - _watermarkSpacing;
        }

        _videoPopUp.width = _width;
        _videoPopUp.height = _height;
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Listeners                                                                          //
    ////////////////////////////////////////////////////////////////////////////////////////
    //click on video
    private function clickOnVidListener(e : MouseEvent) : void {
        trace('clickOnVidListener: ' + (clickOnVidListener));
        if (_videoPopUp.isOpened == false) {
            if (_centerBtn != null) {
                _video.isPlaying == true ? _centerBtn.gotoAndPlay("pause") : _centerBtn.gotoAndPlay("play");
            }
            _video.togglePlayPause();
        }
    }

    //space on video
    private function keyDownListener(e : KeyboardEvent) : void {
        if ((e.keyCode == 32) && (_videoPopUp.isOpened == false)) {
            if (_centerBtn != null) {
                _video.isPlaying == true ? _centerBtn.gotoAndPlay("pause") : _centerBtn.gotoAndPlay("play");
            }
            _video.togglePlayPause();
        }
    }

    //net status listener is called, when stream is unavaible, video ends, etc...
    private function videoListener(e : Event) : void {
        dispatchEvent(e);
        switch (e.type) {
            case VideoLoaderEvent.VIDEO_LOADING:
                _videoPopUp.hide();
                if (_centerBtn != null) {
                    _centerBtn.gotoAndStop("playEnd");
                }
                _loadingClip.visible = true;
                break;
            case VideoLoaderEvent.VIDEO_LOADING_FAILED:
                if (_centerBtn != null) {
                    _centerBtn.gotoAndStop("playEnd");
                }
                _loadingClip.visible = false;
                _videoPopUp.showStreamNotFoundError();
                break;
            case VideoLoaderEvent.VIDEO_START:
                _video.autoplay = true;
                if (_centerBtn != null) {
                    _centerBtn.gotoAndStop("playEnd");
                }
                _loadingClip.visible = false;
                _videoPopUp.hide();
                break;
            case VideoLoaderEvent.VIDEO_PLAY:
                if ((_centerBtn != null) && (_centerBtn.currentLabel == "play")) {
                    _centerBtn.play();
                }
                //					_loadingClip.visible = false;
                if (_videoPopUp.isError == false) {
                    _videoPopUp.hide();
                }
                break;
            case VideoLoaderEvent.VIDEO_PAUSE:
            case VideoLoaderEvent.VIDEO_STOP:
                if ((_centerBtn != null) && (_centerBtn.currentLabel == "pause")) {
                    _centerBtn.play();
                }
                //					_loadingClip.visible = false;
                if (_videoPopUp.isError == false) {
                    _videoPopUp.hide();
                }
                break;
            case VideoLoaderEvent.VIDEO_SEEK:
                if ((_centerBtn != null) && (_centerBtn.currentLabel == "play")) {
                    _centerBtn.play();
                }
                //					_loadingClip.visible = false;
                if (_videoPopUp.isError == false) {
                    _videoPopUp.hide();
                }
                break;
            case VideoLoaderEvent.VIDEO_END:
                if (_centerBtn != null) {
                    _centerBtn.gotoAndStop("playEnd");
                }
                _loadingClip.visible = false;
                _videoPopUp.showReplayNext();
                _controller.playlist.trackEnd();
                break;
            case VideoLoaderEvent.VIDEO_BUFFER_EMPTY:
                _loadingClip.visible = true;
                break;
            case VideoLoaderEvent.VIDEO_BUFFER_FULL:
                _loadingClip.visible = false;
                break;
            case VideoLoaderEvent.VIDEO_VOL:
                break;
        }

        dispatchEvent(e);
    }

    private function watermarkClickHandler(event : MouseEvent) : void {
        if (Controller.WATERMARK_URL != "") {
            navigateToURL(new URLRequest(Controller.WATERMARK_URL), "_blank");
        }
    }

    private function popUpHandler(event : Event) : void {
        switch (event.type) {
            case VideoPopUp.POP_UP_REPLAY:
                _video.replay();
                if ((_centerBtn != null) && (_centerBtn.currentLabel == "play")) {
                    _centerBtn.play();
                }
                break;
            case VideoPopUp.POP_UP_NEXT:
                _controller.playlist.playNextTrack();
                break;
        }
    }
}
}
