﻿package view.VideoFrame {
import debug.Debug;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.utils.Timer;

import model.Playlist;
import model.Settings;

import uiElements.media.Video.VideoLoaderEvent;
import uiElements.media.Video.VideoTypes;

import utils.RotationFrame;
import utils.UIElements.Slide;
import utils.UIElements.SlideBtn;

/**
 * @author Family
 */
public class VideoFrameView extends RotationFrame {
    ////////////////////////////////////////////////////////////////////////////////////////
    // Variables                                                                          //
    ////////////////////////////////////////////////////////////////////////////////////////
    private var _controller : Controller; //controller instance

    private var _bg : Sprite;

    private var _videoView : VideoViewFree; //video
    private var _ctrlsView : ControlsView; //controls
    private var _playlistView : PlaylistView; //playlist

    //settings
    private var _width : Number; //width
    private var _height : Number; //height

    private var _ctrlsPos : uint; //controls pos
    private var _ctrlsOverVid : Boolean; //ctrls over vid

    private var _playlistPos : uint; //playlist pos
    private var _playlistOverVid : Boolean; //playlist over vid
    private var _playlistAutoHide : Boolean; //playlist autohide

    private var _timer : Timer; //timer for autohide ctrls and playlist in fullscreen mode


    ////////////////////////////////////////////////////////////////////////////////////////
    // Constructor                                                                        //
    ////////////////////////////////////////////////////////////////////////////////////////

    public function VideoFrameView() : void {
        super();

        _controller = Controller.getInstance();

        //settings
        _width = _controller.settings.appWidth;
        _height = _controller.settings.appHeight;

        var defaultVol : Number = _controller.settings.defaultVol;

        _ctrlsPos = _controller.settings.ctrlsPos;
        _ctrlsOverVid = _controller.settings.ctrlsOverVid;

        var fullScreenAutohideTime : uint = _controller.settings.autohideTime;

        _playlistPos = _controller.settings.playlistPos;
        _playlistOverVid = _controller.settings.playlistOverVid;
        _playlistAutoHide = _controller.settings.playlistAutoHide;


        //creating bg
        _bg = new Sprite();
        _bg.graphics.beginFill(0XFFFFFF);
        _bg.graphics.drawRect(0, 0, _width, _height);
        _bg.graphics.endFill();
        _bg.alpha = 0;
        _sprite.addChild(_bg);

        //creating video and setting default vol
        _videoView = new VideoViewFree(_width, _height);
        _videoView.vol = defaultVol;
        _sprite.addChild(_videoView);

        //creating controls
        _ctrlsView = new ControlsView();
        _sprite.addChild(_ctrlsView);

        //if ctrls over vid == false
        if (_ctrlsOverVid == false) {
            _ctrlsView.addEventListener(ControlsView.HEIGHT_CHANGED, ctrlsHeightChangedListener);
            ctrlsHeightChangedListener(null);
        }

        //creating playlist
        if ((Controller.MODE == Controller.MODE_FULL) || (Controller.MODE == Controller.MODE_NORMAL)) {
            _playlistView = new PlaylistView();
            _playlistView.x = _playlistPos == Settings.PLAYLIST_POS_L ? 0 : _width;
//                _playlistView.open();
            _sprite.addChild(_playlistView);
        }

        _timer = new Timer(fullScreenAutohideTime * 1000, 1);

        //listeners
        //video view
        _videoView.addEventListener(VideoLoaderEvent.VIDEO_START, vidListener);
        _videoView.addEventListener(VideoLoaderEvent.VIDEO_PLAY, vidListener);
        _videoView.addEventListener(VideoLoaderEvent.VIDEO_PAUSE, vidListener);
        _videoView.addEventListener(VideoLoaderEvent.VIDEO_STOP, vidListener);
        _videoView.addEventListener(VideoLoaderEvent.VIDEO_END, vidListener);
        _videoView.addEventListener(VideoLoaderEvent.VIDEO_VOL, vidListener);

        //controls
        _ctrlsView.addEventListener(ControlsView.PLAY, ctrlsListener);
        _ctrlsView.addEventListener(ControlsView.PAUSE, ctrlsListener);
        _ctrlsView.addEventListener(ControlsView.STOP, ctrlsListener);
        _ctrlsView.vidSlide.addEventListener(Slide.ACTIVE_LEVEL_CHANGED, ctrlsListener);

        _ctrlsView.vidSlide.addEventListener(Slide.DRAG_STARTED, vidDragStartedListener);
        _ctrlsView.volBtn.addEventListener(SlideBtn.LEVEL_CHANGED, ctrlsListener);

        _ctrlsView.addEventListener(ControlsView.PLAY_PREV, ctrlsListener);
        _ctrlsView.addEventListener(ControlsView.PLAY_NEXT, ctrlsListener);

        _ctrlsView.addEventListener(ControlsView.REPEAT_ALL, ctrlsListener);
        _ctrlsView.addEventListener(ControlsView.REPEAT_ONE, ctrlsListener);
        _ctrlsView.addEventListener(ControlsView.REPEAT_OFF, ctrlsListener);

        _ctrlsView.addEventListener(ControlsView.SHUFFLE_ON, ctrlsListener);
        _ctrlsView.addEventListener(ControlsView.SHUFFLE_OFF, ctrlsListener);

        _ctrlsView.addEventListener(ControlsView.PLAYLIST_ON, ctrlsListener);
        _ctrlsView.addEventListener(ControlsView.PLAYLIST_OFF, ctrlsListener);

        _ctrlsView.addEventListener(ControlsView.LIB_ON, ctrlsListener);

        _ctrlsView.addEventListener(ControlsView.HD_ON, ctrlsListener);
        _ctrlsView.addEventListener(ControlsView.HD_OFF, ctrlsListener);


        //open and close playlist
        if (_playlistView != null) {
            _playlistView.addEventListener(PlaylistView.WIDTH_CHANGED, playlistWidthChangedListener);
        }

        _controller.playlist.addEventListener(Playlist.ALBUM_CHANGED, trackChangedListener);
        _controller.playlist.addEventListener(Playlist.TRACK_CHANGED, trackChangedListener);

        loadVid(_controller.playlist.currentTrack);

        var mouseWheel : Boolean = _controller.settings.mouseWheel;
        if (mouseWheel == true) {
            _videoView.addEventListener(MouseEvent.MOUSE_WHEEL, mWheelHandler);
            _ctrlsView.addEventListener(MouseEvent.MOUSE_WHEEL, mWheelHandler);
        }

        //mouse roll over and out
        addEventListener(MouseEvent.ROLL_OVER, rollOverListener);
        addEventListener(MouseEvent.ROLL_OUT, rollOutListener);

        addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener);

        _timer.addEventListener(TimerEvent.TIMER_COMPLETE, timeCompleteListener);

        addEventListener(Event.ENTER_FRAME, enterFrameListener);

        arrange(null);
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Public Methods                                                                     //
    ////////////////////////////////////////////////////////////////////////////////////////

    public function loadVid(track : XML, pos : Number = 0, hd : Boolean = false) : void {
        if (track == null) {
            _videoView.showAlbumIsEmptyError();
            return;
        }

        var vidUrl : String = hd == false ? track.@url : track.@urlHD;
        var type : String = track.@type;
        var fmsUrl : String = track.@fmsUrl;
        _videoView.load(vidUrl, type, pos, fmsUrl);
    }

    public function vidSwitchHD(hd : Boolean) : void {
        var pos : Number = _videoView.pos;
        if (_videoView.mediaType == VideoTypes.YOUTUBE) {
            _videoView.youTubeSwitchHD(hd);
        } else {
            loadVid(_controller.playlist.currentTrack, pos, hd);
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Getters & Setters                                                                  //
    ////////////////////////////////////////////////////////////////////////////////////////

    //ctrls view
    public function get ctrlsView() : ControlsView {
        return _ctrlsView;
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Protected Methods                                                                  //
    ////////////////////////////////////////////////////////////////////////////////////////

    //setting width and height
    protected override function setWidthHeight(width : Number, height : Number) : void {
        _width = width;
        _height = height;

        Debug.trace(_width + " " + _height);

        var playlistCurWidth : Number;

        //playlist
        if (_playlistView != null) {
            _playlistView.x = _playlistPos == Settings.PLAYLIST_POS_L ? 0 : _width;
            _playlistView.setHeight(_height);

            playlistCurWidth = _playlistView.curWidth;
        } else {
            playlistCurWidth = 0;
        }


        //ctrls
        if (_ctrlsView != null) {
            _ctrlsView.setWidthHeight(_width - playlistCurWidth, _height);
            _ctrlsView.x = _playlistPos == Settings.PLAYLIST_POS_L ? playlistCurWidth : 0;
        }

        //video
        if (_videoView != null) {
            if (_ctrlsOverVid == true) {
                _videoView.setHeight(_height);
            } else {
                _videoView.setHeight(_height - _ctrlsView.curHeight);
                _videoView.y = _ctrlsPos == Settings.CTRLS_POS_B ? 0 : _ctrlsView.curHeight;
            }

            if (_playlistOverVid == true) {
                _videoView.setWidth(_width);
            } else {
                _videoView.setWidth(_width - playlistCurWidth);
                _videoView.x = _playlistPos == Settings.PLAYLIST_POS_L ? playlistCurWidth : 0;
            }
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Listeners                                                                          //
    ////////////////////////////////////////////////////////////////////////////////////////

    //video listeners: synchronizing ctrls with video
    private function vidListener(e : Event) : void {
        switch (e.type) {
            case VideoLoaderEvent.VIDEO_START:
            case VideoLoaderEvent.VIDEO_PLAY:
                _ctrlsView.playPauseBtn.curLabel = ControlsView.PAUSE;
                break;
            case VideoLoaderEvent.VIDEO_PAUSE:
            case VideoLoaderEvent.VIDEO_STOP:
            case VideoLoaderEvent.VIDEO_END:
                _ctrlsView.playPauseBtn.curLabel = ControlsView.PLAY;
                break;
            case VideoLoaderEvent.VIDEO_VOL:
                _ctrlsView.volBtn.level = _videoView.vol;
                break;
        }
    }

    //controls listeners : synchronizing video with ctrls
    private function ctrlsListener(e : Event) : void {
        switch (e.type) {
            case ControlsView.PLAY:
                _videoView.play();
                break;
            case ControlsView.PAUSE:
                _videoView.pause();
                break;
            case ControlsView.STOP:
                _videoView.stop();
                //_ctrlsView.playPauseBtn.curLabel = ControlsView.PLAY;
                break;
            case Slide.ACTIVE_LEVEL_CHANGED:
                if (e.target == _ctrlsView.vidSlide) {
                    _videoView.seek(_ctrlsView.vidSlide.activeLevel);
                }
                break;
            case SlideBtn.LEVEL_CHANGED:
                _videoView.vol = _ctrlsView.volBtn.level;
                break;
            case ControlsView.PLAY_PREV:
                _controller.playlist.playPrevTrack();
                break;
            case ControlsView.PLAY_NEXT:
                _controller.playlist.playNextTrack();
                break;
            case ControlsView.REPEAT_ALL:
                _controller.playlist.repeat = Settings.REPEAT_ALL;
                break;
            case ControlsView.REPEAT_ONE:
                _controller.playlist.repeat = Settings.REPEAT_ONE;
                break;
            case ControlsView.REPEAT_OFF:
                _controller.playlist.repeat = Settings.REPEAT_OFF;
                break;
            case ControlsView.SHUFFLE_ON:
                _controller.playlist.shuffle = Settings.SHUFFLE_ON;
                break;
            case ControlsView.SHUFFLE_OFF:
                _controller.playlist.shuffle = Settings.SHUFFLE_OFF;
                break;
            case ControlsView.PLAYLIST_ON:
                _playlistView.open();
                break;
            case ControlsView.PLAYLIST_OFF:
                _playlistView.close();
                _playlistView.opened = false;
                break;
            case ControlsView.LIB_ON:
                close();
                break;
            case ControlsView.HD_ON:
                if ((_videoView.mediaType == VideoTypes.HTTP) || (_videoView.mediaType == VideoTypes.RTMP)) {
                    var urlHD : String = _controller.playlist.currentTrack.@urlHD;
                    if (urlHD != "") {
                        _ctrlsView.hdBtn.curLabel = ControlsView.HD_OFF;
                        vidSwitchHD(true);
                    } else {
                        _ctrlsView.hdBtn.curLabel = ControlsView.HD_ON;
                    }
                } else if (_videoView.mediaType == VideoTypes.YOUTUBE) {
                    if (_videoView.youTubeSwitchHD(true) == true) {
                        _ctrlsView.hdBtn.curLabel = ControlsView.HD_OFF;
                    } else {
                        _ctrlsView.hdBtn.curLabel = ControlsView.HD_ON;
                    }
                }
                break;
            case ControlsView.HD_OFF:
                if ((_videoView.mediaType == VideoTypes.HTTP) || (_videoView.mediaType == VideoTypes.RTMP)) {
                    var url : String = _controller.playlist.currentTrack.@url;
                    if (url != "") {
                        _ctrlsView.hdBtn.curLabel = ControlsView.HD_ON;
                        vidSwitchHD(false);
                    } else {
                        _ctrlsView.hdBtn.curLabel = ControlsView.HD_OFF;
                    }
                } else if (_videoView.mediaType == VideoTypes.YOUTUBE) {
                    if (_videoView.youTubeSwitchHD(false) == true) {
                        _ctrlsView.hdBtn.curLabel = ControlsView.HD_ON;
                    } else {
                        _ctrlsView.hdBtn.curLabel = ControlsView.HD_OFF;
                    }
                }
                break;
        }
    }

    //synchronizing vid slide and time
    private function enterFrameListener(e : Event) : void {
        _ctrlsView.vidSlide.activeLevel = _videoView.pos;
        _ctrlsView.vidSlide.inactiveLevel = _videoView.loaded;


        _ctrlsView.timeBtn.time = _videoView.time;
        _ctrlsView.timeBtn.duration = _videoView.duration;
    }

    //when draging vid slider, video must be paused
    private function vidDragStartedListener(e : Event) : void {
        if (_videoView.isPlaying == true) {
            _ctrlsView.vidSlide.addEventListener(Slide.DRAG_ENDED, vidDragEndedListener);
        }

        _videoView.pause();
    }

    //resuming when vid slide drag ended
    private function vidDragEndedListener(e : Event) : void {
        _ctrlsView.vidSlide.removeEventListener(Slide.DRAG_ENDED, vidDragEndedListener);

        _videoView.play();
    }

    //when ctrls slide in or out, adjusting video pos and size and watermark pos
    private function ctrlsHeightChangedListener(e : Event) : void {
        _videoView.setHeight(_height - _ctrlsView.curHeight);
        _videoView.y = _ctrlsPos == Settings.CTRLS_POS_B ? 0 : _ctrlsView.curHeight;
    }

    //playlist width changed
    private function playlistWidthChangedListener(e : Event) : void {
        _ctrlsView.setWidthHeight(_width - _playlistView.curWidth, _height);
        _ctrlsView.x = _playlistPos == Settings.PLAYLIST_POS_L ? _playlistView.curWidth : 0;

        if (_playlistOverVid == false) {
            _videoView.setWidth(_width - _playlistView.curWidth);
            _videoView.x = _playlistPos == Settings.PLAYLIST_POS_L ? _playlistView.curWidth : 0;
        }
    }


    //on mouse roll over and out
    private function rollOverListener(e : MouseEvent) : void {
        /*_ctrlsView.open();
         if (_playlistView.opened == true) {
         _playlistView.open();
         }*/
    }

    private function rollOutListener(e : MouseEvent) : void {
        /*_ctrlsView.close();

         if ((_playlistView != null) && (_playlistAutoHide == true)) {
         _playlistView.close();
         ctrlsView.playlistBtn.curLabel = ControlsView.PLAYLIST_ON;
         }*/
    }

    //on mouse roll over and out
    private function mouseMoveListener(e : MouseEvent) : void {
        _ctrlsView.open();
        if (_playlistView.opened == true) {
            _playlistView.open();
            ctrlsView.playlistBtn.curLabel = ControlsView.PLAYLIST_OFF;
        }
        _timer.reset();
        _timer.start();
    }

    //autohide time complete
    private function timeCompleteListener(e : TimerEvent) : void {
        _ctrlsView.close();

        if ((_playlistView != null) && (_playlistAutoHide == true)) {
            _playlistView.close();
            ctrlsView.playlistBtn.curLabel = ControlsView.PLAYLIST_ON;
        }
    }


    //loading vid when track changed
    private function trackChangedListener(e : Event) : void {
        _ctrlsView.hdBtn.curLabel = ControlsView.HD_ON;

        loadVid(_controller.playlist.currentTrack, 0);
    }

    private function mWheelHandler(event : MouseEvent) : void {
        _videoView.vol += int(event.delta) / Math.abs(event.delta) * 0.2;
    }
}
}
