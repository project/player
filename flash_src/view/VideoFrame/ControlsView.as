package view.VideoFrame {
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Strong;

	import model.Settings;

	import utils.Library;
	import utils.UIElements.Btn;
	import utils.UIElements.Slide;
	import utils.UIElements.SlideBtn;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author Family
	 */
	public class ControlsView extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const PLAY : String = "play"; //play btn pressed
		public static const PAUSE : String = "pause"; //pause btn pressed
		public static const STOP : String = "stop"; //stop btn pressed
		public static const PLAY_PREV : String = "playPrev"; //play prev btn pressed
		public static const PLAY_NEXT : String = "playNext"; //play next  btn pressed

		public static const REPEAT_ALL : String = "repeatAll"; //repeat state set to repeat all		public static const REPEAT_ONE : String = "repeatOne"; //repeat state set to repeat one		public static const REPEAT_OFF : String = "repeatOff"; //repeat state set to repeat off

		public static const SHUFFLE_ON : String = "shuffleOn"; //shuffle set on		public static const SHUFFLE_OFF : String = "shuffleOff"; //shuffle set off

		public static const HD_ON : String = "hdOn"; //HD set on
		public static const HD_OFF : String = "hdOff"; //HD set off

		public static const PLAYLIST_ON : String = "playlistOn"; //playlist on btn pressed
		public static const PLAYLIST_OFF : String = "playlistOff"; //playlist off btn pressed

		public static const LIB_ON : String = "libOn"; //lib on btn pressed		public static const LIB_OFF : String = "libOff"; //lib off btn pressed

		public static const FULL_SCREEN_ON : String = "fullScreenOn"; //full screen on btn pressed		public static const FULL_SCREEN_OFF : String = "fullScreenOff"; //full screen off btn pressed
				public static const HEIGHT_CHANGED : String = "heightChanged"; //controls height changed
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private static const CTRLS_HEIGHT : Number = 26; //btns height

		private static const VID_SLIDE_HEIGHT : Number = 8; //video slide height		private static const VOL_SLIDE_WIDTH : Number = 60; //vol slide width

		private static const BTN_WIDTH : Number = 30; //btns width
		private static const BTN_PLAY_WIDTH : Number = 45; //play btn width

		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller; //controller instance

		private var _width : Number; //width

		private var _sprite : Sprite; //sprite container
		private var _mask : Sprite; //ctrls mask

		private var _allowedCtrlsNames : Array; //allowed controls names. depends on mode

		private var _leftCtrls : Array; //ctrls on left		private var _rightCtrls : Array; //ctrls on right

		private var _ctrlsWidth : Number; //btns width 
		private var _ctrlsBg : Sprite; //bg
		private var _ctrlsHeight : Number; //ctrls height without vid slide

		private var _playBtn : Btn; //play btn
		private var _pauseBtn : Btn; //pause btn
		private var _playPauseBtn : Btn; //play / pause btn
		private var _stopBtn : Btn; //stop btn
		private var _playPrevBtn : Btn; //play prev btn
		private var _playNextBtn : Btn; //play next btn

		private var _vidSlide : Slide; //vid slide

		private var _volBtn : SlideBtn; //vol btn
		private var _timeBtn : TimeView; //time view

		private var _repeatBtn : Btn; //repeat btn
		private var _shuffleBtn : Btn; //shuffle btn

		private var _hdBtn : Btn; //HD btn
				private var _shareBtn : Btn; //HD btn
		private var _shareIcons : ShareIconsView;

		private var _playlistBtn : Btn; //playlist btn		private var _libBtn : Btn; //lib btn
		private var _fullScreenBtn : Btn; //full screen btn

		private var _pos : uint; //ctrls pos
		private var _autoHide : Boolean; //slide out or not

		private var _tween : Tween; //tween for open and close		private var _closing : Boolean; //if ctrls closing or not
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////

		public function ControlsView() : void {
			_controller = Controller.getInstance();
			
			
			_width = _controller.settings.appWidth;			var appHeight : Number = _controller.settings.appHeight;
			
			var ctrlsBgColor : int = _controller.settings.framesBgColor;
			var ctrlsBgAlpha : Number = _controller.settings.framesBgAlpha;
			var ctrlsMainColor : int = _controller.settings.ctrlsMainColor;
			var ctrlsMainHoverColor : int = _controller.settings.ctrlsMainHoverColor;
			var ctrlsMainAlpha : Number = _controller.settings.ctrlsMainAlpha;
			var slideColor : int = _controller.settings.slideColor;
			
			var defaultVol : Number = _controller.settings.defaultVol;
			var defaultRepeat : String = _controller.settings.defaultRepeat;			var defaultShuffle : String = _controller.settings.defaultShuffle;
			
			_pos = _controller.settings.ctrlsPos;
			_autoHide = _controller.settings.ctrlsAutoHide;
			
			
			var playBtnHint : String = _controller.settings.playBtnHint;			var pauseBtnHint : String = _controller.settings.pauseBtnHint;			var playPauseBtnHint : String = _controller.settings.playPauseBtnHint;			var stopBtnHint : String = _controller.settings.stopBtnHint;			var playPrevBtnHint : String = _controller.settings.playPrevBtnHint;			var playNextBtnHint : String = _controller.settings.playNextBtnHint;			var volBtnHint : String = _controller.settings.volBtnHint;			var repeatBtnHint : String = _controller.settings.repeatBtnHint;			var shuffleBtnHint : String = _controller.settings.shuffleBtnHint;			var hdBtnHint : String = _controller.settings.hdBtnHint;			var shareBtnHint : String = _controller.settings.shareBtnHint;			var playlistBtnHint : String = _controller.settings.playlistBtnHint;			var libOnBtnHint : String = _controller.settings.libOnBtnHint;			var fullScreenBtnHint : String = _controller.settings.fullScreenBtnHint;
			
			
			this.y = _pos == Settings.CTRLS_POS_B ? appHeight : 0;
			
			
			//sprite and its mask
			_sprite = new Sprite;
			addChild(_sprite);
			
			_mask = new Sprite;
			_mask.graphics.beginFill(0xFF0000);
			_mask.graphics.drawRect(0, 0, _width, appHeight);
			_mask.graphics.endFill();
			_mask.y = _pos == Settings.CTRLS_POS_B ? -_mask.height : 0;
			addChild(_mask);
			
			_sprite.mask = _mask;
			
			if (Controller.MODE == Controller.MODE_FULL) {
				_allowedCtrlsNames = ["play","pause","playPrev","playPause","stop","playNext","vol","time","repeat","shuffle","hd","share","playlist","lib","fullScreen"];
			} else if (Controller.MODE == Controller.MODE_NORMAL) {
				_allowedCtrlsNames = ["play","pause","playPrev","playPause","stop","playNext","vol","time","repeat","shuffle","hd","share","playlist","fullScreen"];
			} else {
				_allowedCtrlsNames = ["play","pause","playPause","stop","vol","time","hd","share","fullScreen"];
			}
			
			_leftCtrls = new Array();			_rightCtrls = new Array();
			
			_ctrlsWidth = 0;			
			
			//setting bg, it's color and alpha
			_ctrlsBg = Library.createSprite("ctrlsBg");
			var colorField : Sprite = Sprite(_ctrlsBg.getChildByName("color"));
			ColorUtils.setColor(colorField, ctrlsBgColor);
			_ctrlsBg.alpha = ctrlsBgAlpha;
			_ctrlsBg.width = _width;
			_ctrlsHeight = CTRLS_HEIGHT;
			_sprite.addChild(_ctrlsBg);			
			
			//btns
			_playBtn = new Btn(BTN_PLAY_WIDTH, CTRLS_HEIGHT, "btnPlaySkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, playBtnHint);			_pauseBtn = new Btn(BTN_WIDTH, CTRLS_HEIGHT, "btnPauseSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, pauseBtnHint);			_playPauseBtn = new Btn(BTN_PLAY_WIDTH, CTRLS_HEIGHT, "btnPlayPauseSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, playPauseBtnHint);
			_stopBtn = new Btn(BTN_WIDTH, CTRLS_HEIGHT, "btnStopSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, stopBtnHint);
			_playPrevBtn = new Btn(BTN_WIDTH, CTRLS_HEIGHT, "btnPlayPrevSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, playPrevBtnHint);
			_playNextBtn = new Btn(BTN_WIDTH, CTRLS_HEIGHT, "btnPlayNextSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, playNextBtnHint);
			
			
			_vidSlide = new Slide(_width, VID_SLIDE_HEIGHT, "vidSlideSkin", ctrlsBgColor, -1, ctrlsBgAlpha, slideColor, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha);
						_volBtn = new SlideBtn(BTN_WIDTH, VOL_SLIDE_WIDTH, CTRLS_HEIGHT, "btnVolSkin", "volSlideSkin", slideColor, -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, volBtnHint);
			_volBtn.mouseChildren = true;
			_volBtn.level = defaultVol;
			
			_timeBtn = new TimeView();
						_repeatBtn = new Btn(BTN_WIDTH, CTRLS_HEIGHT, "btnRepeatSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, repeatBtnHint);
			_repeatBtn.curLabel = defaultRepeat == Settings.REPEAT_OFF ? Settings.REPEAT_ALL : defaultRepeat == Settings.REPEAT_ONE ? Settings.REPEAT_OFF : Settings.REPEAT_ONE;			_shuffleBtn = new Btn(BTN_WIDTH, CTRLS_HEIGHT, "btnShuffleSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, shuffleBtnHint);
			_shuffleBtn.curLabel = defaultShuffle == Settings.SHUFFLE_ON ? Settings.SHUFFLE_OFF : Settings.SHUFFLE_ON;
			
			_hdBtn = new Btn(BTN_WIDTH, CTRLS_HEIGHT, "btnHDSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, hdBtnHint);
			_hdBtn.curLabel = HD_ON;
			
			_shareBtn = new Btn(BTN_WIDTH, CTRLS_HEIGHT, "btnShareSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha);
			_shareIcons = new ShareIconsView();
			
			_playlistBtn = new Btn(BTN_WIDTH, CTRLS_HEIGHT, "btnPlaylistSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, playlistBtnHint);			_libBtn = new Btn(BTN_WIDTH, CTRLS_HEIGHT, "btnLibOnSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, libOnBtnHint);			_fullScreenBtn = new Btn(BTN_WIDTH, CTRLS_HEIGHT, "btnFullScreenSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, fullScreenBtnHint);
			_fullScreenBtn.curLabel = _controller.fullScreen == true ? ControlsView.FULL_SCREEN_OFF : ControlsView.FULL_SCREEN_ON;
			_controller.addEventListener(Controller.SWITCH_FULL_SCREEN, fullScreenSwitchedListener);
			
			
			//open / close tween
			_tween = _pos == Settings.CTRLS_POS_B ? new Tween(_sprite, "y", Strong.easeOut, getHeight(), 0, 0.1, true) : new Tween(_sprite, "y", Strong.easeOut, -getHeight(), 0, 0.1, true);
			_tween.stop();
			
			_closing = false;
			
			_sprite.y = _autoHide == false ? 0 : _sprite.y;
			
			parseCtrls();
			
			_tween.addEventListener(TweenEvent.MOTION_CHANGE, motionChangeListener);
			
			_volBtn.addEventListener(SlideBtn.CUR_WIDTH_CHANGED, btnWidthChanged);			_timeBtn.addEventListener(TimeView.WIDTH_CHANGED, btnWidthChanged);
			
			_shareBtn.addEventListener(MouseEvent.ROLL_OVER, shareBtnRollOver);			_shareBtn.addEventListener(MouseEvent.ROLL_OUT, shareBtnRollOut);
			_shareIcons.sprite.addEventListener(MouseEvent.ROLL_OVER, shareBtnRollOver);
			_shareIcons.sprite.addEventListener(MouseEvent.ROLL_OUT, shareBtnRollOut);		}

		

		private function shareBtnRollOver(event : MouseEvent) : void {
			showShareIcons();
		}

		private function shareBtnRollOut(event : MouseEvent) : void {
			hideShareIcons();
		}
		
		private function showShareIcons() : void {
			addChild(_shareIcons.sprite);
		}
		
		private function hideShareIcons() : void {
			if (_shareIcons.sprite.parent != null) {
				_shareIcons.sprite.parent.removeChild(_shareIcons.sprite);
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//open and close
		public function open() : void {
			_closing = false;
			
			if (_autoHide == false) {
				return;
			}
			
			_tween.func = Strong.easeOut;
			_tween.continueTo(0, 0.1);
		}

		public function close() : void {
			_closing = true;
			
			if (_autoHide == false) {
				return;
			}
			
			hideShareIcons();
			
			_tween.func = Strong.easeIn;
			_pos == Settings.CTRLS_POS_B ? _tween.continueTo(getHeight(), 0.9) : _tween.continueTo(-getHeight(), 0.9);
		}

		
		//set width and height
		public function setWidthHeight(width : Number, height : Number) : void {
			_width = width;
			
			this.y = _pos == Settings.CTRLS_POS_B ? height : 0;
			
			arrange();
			
			if (_closing == true) {
				close();			
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Getters & Setters                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//current height of ctrls
		public function get curHeight() : Number {
			return getHeight() - Math.abs(_sprite.y);
		}

		
		//playPause btn
		public function get playPauseBtn() : Btn {
			return _playPauseBtn;
		}

		//vid slide
		public function get vidSlide() : Slide {
			return _vidSlide;
		}

		//vol btn
		public function get volBtn() : SlideBtn {
			return _volBtn;
		}

		//time btn
		public function get timeBtn() : TimeView {
			return _timeBtn;
		}

		//playlist btn
		public function get playlistBtn() : Btn {
			return _playlistBtn;
		}

		//playlist btn
		public function get hdBtn() : Btn {
			return _hdBtn;
		}
		
		public function get shareBtn() : Btn {
			return _shareBtn;
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Private Methods                                                                    //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//parsing controls string
		private function parseCtrls() : void {
			var ctrlsNames : String = _controller.settings.ctrlsStack;
			if (ctrlsNames.indexOf("+") != -1) {
				var leftCtrlsNames : Array = ctrlsNames.substring(0, ctrlsNames.indexOf("+")).split(",");
				var rightCtrlsNames : Array = ctrlsNames.substring(ctrlsNames.indexOf("+") + 1).split(",");
			} else {
				leftCtrlsNames = ctrlsNames.split(",");
				rightCtrlsNames = new Array;
			}
			
			//parsing left ctrls
			var i : int = 0;					var index : uint = 0;		
			for (i = 0;i < leftCtrlsNames.length;i++) {
				if (_allowedCtrlsNames.indexOf(leftCtrlsNames[i]) == -1) {
					continue;
				}
				
				var ctrl = this["_" + leftCtrlsNames[i] + "Btn"];
				if ((ctrl != undefined) && (ctrl.parent != _sprite)) {
					_leftCtrls[index] = ctrl;
					_sprite.addChild(_leftCtrls[index]);
					
					_ctrlsWidth += _leftCtrls[index] is SlideBtn ? _leftCtrls[index].fullWidth : _leftCtrls[index].width;
					index++;
					
					_leftCtrls[index] = Library.createSprite("vDiv");
					_leftCtrls[index].height = CTRLS_HEIGHT;
					_sprite.addChild(_leftCtrls[index]);
					
					_ctrlsWidth += _leftCtrls[index].width;
					index++;
				}
			}
			
			//parsing right ctrls
			index = 0;			
			for (i = 0;i < rightCtrlsNames.length;i++) {
				if (_allowedCtrlsNames.indexOf(rightCtrlsNames[i]) == -1) {
					continue;
				}
				
				ctrl = this["_" + rightCtrlsNames[i] + "Btn"];
				if ((ctrl != undefined) && (ctrl.parent != _sprite)) {
					_rightCtrls[index] = Library.createSprite("vDiv");
					_rightCtrls[index].height = CTRLS_HEIGHT;
					_sprite.addChild(_rightCtrls[index]);
					
					_ctrlsWidth += _rightCtrls[index].width;
					index++;
					
					_rightCtrls[index] = this["_" + rightCtrlsNames[i] + "Btn"];
					_sprite.addChild(_rightCtrls[index]);
					_ctrlsWidth += _rightCtrls[index] is SlideBtn ? _rightCtrls[index].fullWidth : _rightCtrls[index].width;
					index++;
				}
			}
			
			_sprite.addChild(_vidSlide);
			
			arrange();
		}

		
		//arranging controls and divs
		private function arrange() : void {
			var i : int;
			var scale : Number;
			var fullScreen : Boolean = _controller.fullScreen;
			
			//calculating scale
			scale = _width / _ctrlsWidth > 1 ? 1 : _width / _ctrlsWidth;
			
			_mask.width = _width;
			
			_ctrlsBg.width = _width;
			_ctrlsBg.scaleY = scale;
			
			_vidSlide.setWidth(_width);
			
			_ctrlsBg.y = _pos == Settings.CTRLS_POS_B ? -_ctrlsBg.height : 0;
			_vidSlide.y = _pos == Settings.CTRLS_POS_B ? -_ctrlsBg.height - _vidSlide.hght : _ctrlsBg.height;
			
			for (i = 0;i < _leftCtrls.length;i++) {
				_leftCtrls[i].scaleX = scale;
				_leftCtrls[i].scaleY = scale;
				
				var prevItemWidth : uint = i == 0 ? 0 : (_leftCtrls[i - 1] is SlideBtn ? _leftCtrls[i - 1].curWidth : _leftCtrls[i - 1].width);
				_leftCtrls[i].x = i == 0 ? 0 : _leftCtrls[i - 1].x + prevItemWidth;
				_leftCtrls[i].y = _leftCtrls[i] != _timeBtn ? _ctrlsBg.y : _ctrlsBg.y + _ctrlsBg.height / 2 - _timeBtn.height / 2;
			}
			
			for (i = _rightCtrls.length - 1;i >= 0;i--) {
				_rightCtrls[i].scaleX = scale;
				_rightCtrls[i].scaleY = scale;
				
				var curItemWidth : uint = _rightCtrls[i] is SlideBtn ? _rightCtrls[i].curWidth : _rightCtrls[i].width;
				_rightCtrls[i].x = i == _rightCtrls.length - 1 ? _width - curItemWidth : _rightCtrls[i + 1].x - curItemWidth;
				_rightCtrls[i].y = _leftCtrls[i] != _timeBtn ? _ctrlsBg.y : _ctrlsBg.y + _ctrlsBg.height / 2 - _timeBtn.height / 2;
			}
			
			_shareIcons.sprite.x = _shareBtn.x + _shareBtn.width / 2 - _shareIcons.sprite.width / 2;			_shareIcons.sprite.y = _pos == Settings.CTRLS_POS_B ? _shareBtn.y - _shareIcons.sprite.height : _shareBtn.y + _shareBtn.height;
		}

		
		//getting controls height
		private function getHeight() : uint {
			return _ctrlsBg.height + _vidSlide.hght;
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//full screen switched
		private function fullScreenSwitchedListener(e : Event) : void {
			_fullScreenBtn.curLabel = _controller.fullScreen == true ? ControlsView.FULL_SCREEN_OFF : ControlsView.FULL_SCREEN_ON;
		}

		//vol btn width changed
		private function btnWidthChanged(e : Event) : void {
			arrange();
		}

		//open close motion change
		private function motionChangeListener(e : TweenEvent) : void {
			dispatchEvent(new Event(HEIGHT_CHANGED));
		}
	}
}
