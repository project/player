package view.VideoFrame {
	import fl.transitions.Tween;
	import fl.transitions.easing.Strong;

	import graphics.Rect;

	import model.Settings;

	import uiElements.SimpleBtn;
	import uiElements.text.StringScroller;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;

	/**
	 * @author Robert Soghbatyan (robert.sogbatyan@gmail.com)
	 */
	public class VideoPopUp extends EventDispatcher {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const POP_UP_REPLAY : String = "popUpReplay";		public static const POP_UP_NEXT : String = "popUpNext";
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private static const BG_COLOR : int = 0x000000;		private static const BG_ALPHA : Number = 0.6;
		
		private static const TWEEN_FUNCTION : Function = Strong.easeInOut;
		private static const TWEEN_DURATION : Number = 0.3;

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller;
		private var _sprite : Sprite;
		
		private var _isOpened : Boolean;
		private var _isError : Boolean;

		private var _width : Number;
		private var _height : Number;

		private var _bg : Rect;
		
		private var _contentSprite : Sprite;

		private var _errorMsg : StringScroller;

		private var _replayNextBtnsSprite : Sprite;
		private var _btnReplay : SimpleBtn;		private var _btnNext : SimpleBtn;
		
		private var _tweener : Tweener;

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		public function VideoPopUp() : void {
			_controller = Controller.getInstance();
			
			_sprite = new Sprite();
			
			_bg = new Rect();
			_bg.fillColor = BG_COLOR;			_bg.fillAlpha = BG_ALPHA;
			_sprite.addChild(_bg);
			
			_isOpened = true;
			_isError = false;
			
			_contentSprite = new Sprite();
			_sprite.addChild(_contentSprite);
			
			_errorMsg = new StringScroller();
			_errorMsg.align = StringScroller.ALIGN_CENTER;
			_errorMsg.font = Settings.TEXT_FONT;			_errorMsg.size = Settings.ERROR_MSG_FONT_SIZE;			_errorMsg.color = Settings.ERROR_MSG_FONT_COLOR;
			_errorMsg.autoHeight = true;
			
			_replayNextBtnsSprite = Library.createSprite("replayNextBtns");
			var btnReplaySkin : Sprite = _replayNextBtnsSprite.getChildByName("btnReplay") as Sprite;
			var btnReplayText : String = _controller.settings.endReplayBtnText;
			_btnReplay = new SimpleBtn(btnReplaySkin, btnReplayText, btnReplayClickHandler);
			
			var btnNextSkin : Sprite = _replayNextBtnsSprite.getChildByName("btnNext") as Sprite;
			if (Controller.MODE == Controller.MODE_LIGHT) {
				btnNextSkin.parent.removeChild(btnNextSkin);
			} else {
				var btnNextText : String = _controller.settings.endNextBtnText;
				_btnNext = new SimpleBtn(btnNextSkin, btnNextText, btnNextClickHandler);
			}
			
			_tweener = new Tweener();
			
			hide();
			
			update();
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		public function showStreamNotFoundError() : void {
			clearContent();
			
			_isError = true;
			_errorMsg.text = Settings.STREAM_NOT_FOUND_MSG;
			_contentSprite.addChild(_errorMsg);
			
			show();
		}

		public function showAlbumIsEmptyError() : void {
			clearContent();
			
			_isError = true;
			_errorMsg.text = Settings.ALBUM_IS_EMPTY_MSG;
			_contentSprite.addChild(_errorMsg);
			
			show();
		}

		public function showReplayNext() : void {
			clearContent();
			
			_isError = false;
			_contentSprite.addChild(_replayNextBtnsSprite);
			
			show();
		}

		public function hide() : void {
			if (_isOpened == false) {
				return;
			}
			
			_isOpened = false;
			
			_sprite.mouseEnabled = false;
			_sprite.mouseChildren = false;
			
			_tweener.reset();
			var alphaTween : Tween = new Tween(_sprite, "alpha", TWEEN_FUNCTION, _sprite.alpha, 0, TWEEN_DURATION, true);
			_tweener.addTween(alphaTween);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Getters & Setters                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		public function get sprite() : Sprite {
			return _sprite;
		}
		
		public function get isOpened() : Boolean {
			return _isOpened;
		}
		
		public function get isError() : Boolean {
			return _isError;
		}
		
		public function get width() : Number {
			return _width;
		}

		public function set width(width : Number) : void {
			_width = width;
			update();
		}

		public function get height() : Number {
			return _height;
		}

		public function set height(height : Number) : void {
			_height = height;
			update();
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Private Methods                                                                    //
		////////////////////////////////////////////////////////////////////////////////////////
		private function update() : void {
			_bg.width = _width;			_bg.height = _height;
			
			updateContentPos();
			
			_errorMsg.width = _width;		}
		
		private function show() : void {
			updateContentPos();
			
			if (_isOpened == true) {
				return;
			}
			
			_isOpened = true;
			
			_sprite.mouseEnabled = true;
			_sprite.mouseChildren = true;
			
			_tweener.reset();
			var alphaTween : Tween = new Tween(_sprite, "alpha", TWEEN_FUNCTION, _sprite.alpha, 1, TWEEN_DURATION, true);
			_tweener.addTween(alphaTween);
		}
		
		private function updateContentPos() : void {
			_contentSprite.x = (_width - _contentSprite.width) / 2;
			_contentSprite.y = (_height - _contentSprite.height) / 2;
		}
		
		private function clearContent() : void {
			while (_contentSprite.numChildren > 0) {
				_contentSprite.removeChildAt(0);
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private function btnReplayClickHandler(event : MouseEvent) : void {
			hide();
			dispatchEvent(new Event(POP_UP_REPLAY));
		}
		
		private function btnNextClickHandler(event : MouseEvent) : void {
			hide();
			dispatchEvent(new Event(POP_UP_NEXT));
		}
	}
}
