package view.VideoFrame {
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Strong;

	import model.Settings;

import utils.ColorUtils;

import utils.ImgLoader;
	import utils.Library;
	import utils.UIElements.ScrollTextField;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author Family
	 */
	public class PlaylistItem extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const HEIGHT_CHANGED : String = "heightChanged"; //height changed during open / close
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private const SPACE : Number = 5; //space beetween thumb, texts

		private const THUMB_WIDTH : Number = 130; //thumb max width
		private const THUMB_HEIGHT : Number = 70; //thumb max height

		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller; //controller instance

		private var _width : Number; //width

		private var _bgHoverColor : uint; //bg hover color
		private var _bgSelectedColor : uint; //bg selected color
		private var _bgAlpha : Number; //bg alpha

		private var _textFont : String; //text font
		private var _textSize : uint; //font size
		private var _textColor : uint; //text color
		private var _textHoverColor : uint; //text hover color
		private var _textSelectedColor : uint; //text selected color

		
		private var _sprite : Sprite; //containing sprite
		private var _spriteMask : Sprite; //sprite mask

		private var _bg : Sprite; //bg
		private var _thumb : ImgLoader; //thumb
		private var _artist : ScrollTextField; //artist text
		private var _album : ScrollTextField; //album text
		private var _track : ScrollTextField; //track id and title text

		private var _div : Sprite; //div under item

		private var _tween : Tween; //open / close tween

		private var _hover : Boolean;
		private var _selected : Boolean;

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////

		public function PlaylistItem(track : String, trackId : String, artist : String, thumbUrl : String, album : String, year : String) : void {
			_controller = Controller.getInstance();
			
			_width = _controller.settings.playlistWidth;
			
			_bgHoverColor = _controller.settings.itemBgHoverColor;
			_bgSelectedColor = _controller.settings.itemBgSelectedColor;
			_bgAlpha = _controller.settings.itemBgAlpha;
			_textFont = Settings.TEXT_FONT;
			_textSize = _controller.settings.playlistTextSize;
			_textColor = _controller.settings.textColor;
			_textHoverColor = _controller.settings.textHoverColor;
			_textSelectedColor = _controller.settings.textSelectedColor;
			
			var text : String;
			
			//main sprite, and its mask
			_sprite = new Sprite();
			_sprite.mouseChildren = false;
			addChild(_sprite);
			
			_spriteMask = new Sprite();
			_spriteMask.graphics.beginFill(0xFFFFFF);
			_spriteMask.graphics.drawRect(0, 0, _width, SPACE);
			_spriteMask.graphics.endFill();
			addChild(_spriteMask);
			
			_sprite.mask = _spriteMask;
			
			
			//bg
			_bg = new Sprite;
			_bg.graphics.beginFill(_bgHoverColor);
			_bg.graphics.drawRect(0, 0, _width, SPACE);
			_bg.graphics.endFill();
			_bg.alpha = 0;
			_sprite.addChild(_bg);
			
			//thumb
			if (thumbUrl != "") {
				_thumb = new ImgLoader(thumbUrl, THUMB_WIDTH, THUMB_HEIGHT, -1);
				_thumb.x = _bg.width / 2 - _thumb.width / 2;
				_thumb.y = _bg.height;
				
				_sprite.addChild(_thumb);
				
				_bg.height = _thumb.y + _thumb.height + SPACE;
			}
			
			//artist
			if (artist != "") {
				_artist = new ScrollTextField(_width, artist, _textFont, _textSize, _textColor);
				_artist.y = _bg.height;
				_sprite.addChild(_artist);
				_bg.height = _artist.y + _artist.height + SPACE;
			}
			
			//album and year
//			if ((album != "") || (year != "")) {
//				text = album == "" ? year : year == "" ? album : album + " (" + year + ")";
//				_album = new ScrollTextField(_width, text, _textFont, _textSize, _textColor);
//				_album.y = _bg.height;
//				_sprite.addChild(_album);
//				_bg.height = _album.y + _album.height + SPACE;
//			}
			
			//trackId and song
			text = trackId != "" ? trackId + " - " + track : track != "" ? track : "Unknown";
			_track = new ScrollTextField(_width, text, _textFont, _textSize, _textColor);
			_track.y = _bg.height;
			_sprite.addChild(_track);
			_bg.height = _track.y + _track.height + SPACE;
			
			//creating div
			_div = Library.createSprite("hDiv");
			_div.y = _bg.height;
			_div.width = _width;
			_sprite.addChild(_div);
			
			_bg.height = _div.y + _div.height;
			
			//defining mask height
			_spriteMask.height = _bg.height;
			
			
			//open and close tween
			_tween = new Tween(_sprite, "y", Strong.easeOut, -_track.y + SPACE, 0, 0.1, true);
			_tween.stop();
			
			_hover = false;
			_selected = false;

			//listeners
			_tween.addEventListener(TweenEvent.MOTION_CHANGE, motionChangeListener);
			
			_sprite.addEventListener(MouseEvent.ROLL_OVER, rollOverListener);
			_sprite.addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//selecting and deselecting
		public function select() : void {
			_sprite.removeEventListener(MouseEvent.ROLL_OVER, rollOverListener);
			_sprite.removeEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			
			_bg.alpha = _bgAlpha;
			ColorUtils.setColor(_bg, _bgSelectedColor);
			
			if (_artist != null) {
				_artist.setColor(_textSelectedColor);
			}
			if (_album != null) {
				_album.setColor(_textSelectedColor);
			}
			_track.setColor(_textSelectedColor);
			
			_selected = true;
			
			open();
		}

		public function deselect() : void {
			if (_selected == false) {
				return;
			}
			
			_selected = false;
			
			_sprite.addEventListener(MouseEvent.ROLL_OVER, rollOverListener);
			_sprite.addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			
			if (this.hitTestPoint(this.root.stage.mouseX, this.root.stage.mouseY) == true) {
				rollOverListener(null);
			} else {
				rollOutListener(null);
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Getters & Setters                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//getting current height
		public function get curHeight() : Number {
			return _sprite.height + _sprite.y;
		}

		//getting hover
		public function get hover() : Boolean {
			return _hover;
		}

		//getting hover
		public function get selected() : Boolean {
			return _selected;
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Private Methods                                                                    //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//open/close
		private function open() : void {
			_tween.func = Strong.easeOut;
			_tween.continueTo(0, 0.5);
		}

		private function close() : void {
			_tween.func = Strong.easeIn;
			_tween.continueTo(-_track.y + SPACE, 0.5);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//roll over and roll out
		private function rollOverListener(e : MouseEvent) : void {
			_bg.alpha = _bgAlpha;
			ColorUtils.setColor(_bg, _bgHoverColor);
			
			if (_artist != null) {
				_artist.setColor(_textHoverColor);
			}
			if (_album != null) {
				_album.setColor(_textHoverColor);
			}
			if (_track != null) {
				_track.setColor(_textHoverColor);
			}
			
			_hover = true;
			
			open();
		}

		private function rollOutListener(e : MouseEvent) : void {
			_hover = false;
			
			_bg.alpha = 0;
			
			if (_artist != null) {
				_artist.setColor(_textColor);
			}
			if (_album != null) {
				_album.setColor(_textColor);
			}
			if (_track != null) {
				_track.setColor(_textColor);
			}
			
			close();
		}

		//open/close motion change listener
		private function motionChangeListener(e : TweenEvent) : void {
			dispatchEvent(new Event(HEIGHT_CHANGED));
		}
	}
}
