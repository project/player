package view.VideoFrame {
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Strong;

	import model.Playlist;
	import model.Settings;

	import utils.Library;
	import utils.UIElements.Btn;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author Family
	 */
	public class PlaylistView extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const WIDTH_CHANGED : String = "widthChanged"; //width changed during open / close

		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private static const CTRLS_HEIGHT : uint = 26; // up and down arrows height
		private static const SCROLL_SPEED : Number = 5; //scroll pixels per cicle

		private static const SCROLL_DIR_UP : String = "dirUp"; //scroll dir up
		private static const SCROLL_DIR_DOWN : String = "dirDown"; //scroll dir down

		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller; //controller instance

		private var	_width : Number; //width
		private var _height : Number; //height
		private var _pos : uint; //pos : left or right

		private var _bgColor : int; //bg color
		private var _bgAlpha : Number; //bg alpha
		private var _opened : Boolean;

		
		private var _sprite : Sprite; //containing sprite
		private var _spriteMask : Sprite; //sprite mask

		private var _bg : Sprite; //bg
		private var _vidPlaylistDiv : Sprite; //vid playlist splitter

		private var _btnUp : Btn; //btn up
		private var _btnDown : Btn; //btn down

		private var _scrollDir : String; //scroll direction

		private var _listSprite : Sprite; //sprite of list items
		private var _listSpriteHeight : Number; //list sprite height
		private var _listMask : Sprite; //list sprite mask

		private var _listItems : Array; //list items
		private var _curListItemNum : int; //current list item num

		private var _tween : Tween; //open / close tween


		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////

		public function PlaylistView() : void {
			_controller = Controller.getInstance();
			
			_width = _controller.settings.playlistWidth;
			_height = _controller.settings.appHeight;
			_pos = _controller.settings.playlistPos;
			
			_bgColor = _controller.settings.framesBgColor;
			_bgAlpha = _controller.settings.framesBgAlpha;
			_opened = false;
			
			
			//sprite and its mask
			_sprite = new Sprite();
			addChild(_sprite);
			
			_spriteMask = new Sprite();
			_spriteMask.graphics.beginFill(0xFFFFFF);
			_spriteMask.graphics.drawRect(0, 0, _width, _height);
			_spriteMask.graphics.endFill();
			_spriteMask.x = _pos == Settings.PLAYLIST_POS_L ? 0 : -_width;
			addChild(_spriteMask);
			
			_sprite.mask = _spriteMask;
			
			
			//initing bg, if it doesn't exist, creating transparent one
			_bg = Library.createSprite("playlistBgSkin");
			_bg.width = _width;
			_bg.height = _height;
			var colorField : Sprite = Sprite(_bg.getChildByName("color"));
			ColorUtils.setColor(colorField, _bgColor);
			_bg.alpha = _bgAlpha;
			_sprite.addChild(_bg);
			
			_vidPlaylistDiv = Library.createSprite("vidPlaylistDiv");
			_vidPlaylistDiv.x = _controller.settings.playlistPos == Settings.PLAYLIST_POS_L ? _width - _vidPlaylistDiv.width : 0;
			_sprite.addChild(_vidPlaylistDiv);
			
			
			//up and down arrows and divs
			var ctrlsMainColor : int = _controller.settings.ctrlsMainColor;
			var ctrlsMainHoverColor : int = _controller.settings.ctrlsMainHoverColor;
			var ctrlsMainAlpha : Number = _controller.settings.ctrlsMainAlpha;
			
			_btnUp = new Btn(_width, CTRLS_HEIGHT, "btnArrowUpSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, "", false, false, false, true);
			_sprite.addChild(_btnUp);
			
			_btnDown = new Btn(_width, CTRLS_HEIGHT, "btnArrowDownSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, "", false, false, true);
			_btnDown.y = _height - _btnDown.height;
			_sprite.addChild(_btnDown);
			
			
			
			//main list and its mask
			_listSprite = new Sprite();
			_listSprite.y = _btnUp.height;
			_sprite.addChild(_listSprite);
			
			_listMask = new Sprite();
			_listMask.graphics.beginFill(0xFFFFFF);
			_listMask.graphics.drawRect(0, 0, _width, _btnDown.y - _btnUp.height);
			_listMask.graphics.endFill();
			_listMask.y = _btnUp.height;
			_sprite.addChild(_listMask);
			
			_listSprite.mask = _listMask;
			
			
			_listItems = new Array();
			_curListItemNum = -1;
			
			//tween for open and close
			_tween = new Tween(_sprite, "x", Strong.easeOut, 0, 0, 0.1, true);
			_tween.begin = _pos == Settings.PLAYLIST_POS_L ? -_width : 0;
			_tween.rewind();
			_tween.stop();
			_tween.addEventListener(TweenEvent.MOTION_CHANGE, tweenMotionChangeListener);

//            open();
//            _tween.fforward();
			
			
			//listeners
			_btnUp.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener);
			_btnDown.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener);
			
			_controller.playlist.addEventListener(Playlist.ALBUM_CHANGED, albumChangedListener);
			_controller.playlist.addEventListener(Playlist.TRACK_CHANGED, trackChangedListener);
			
			albumChangedListener(null);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//open and close
		public function open() : void {
			_opened = true;
			_tween.func = Strong.easeOut;
			_pos == Settings.PLAYLIST_POS_L ? _tween.continueTo(0, 0.1) : _tween.continueTo(-_width, 0.1);
		}

		public function close() : void {
			_tween.func = Strong.easeIn;
			_pos == Settings.PLAYLIST_POS_L ? _tween.continueTo(-_width, 0.9) : _tween.continueTo(0, 0.9);
		}

		
		//getting current width
		public function get curWidth() : Number {
			return _pos == Settings.PLAYLIST_POS_L ? _sprite.x + _width : -_sprite.x;
		}
		
		//setting width
		public function setHeight(height : Number) : void {
			_height = height;
			
			_bg.height = _height;
			_vidPlaylistDiv.height = _height;
			
			_spriteMask.height = _height;
			
			_btnDown.y = _height - _btnDown.height;
			
			_listMask.height = _btnDown.y - _btnUp.height;	
		}
		
		public function get opened() : Boolean {
			return _opened;
		}
		
		public function set opened(opened : Boolean) : void {
			_opened = opened;
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Private Methods                                                                    //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//arranging list
		private function arrange() : void {
			if (_listItems.length == 0) {
				return;
			}
			
			var i : uint;
			for (i = 0; i < _listItems.length; i++) {
				var prevItem : PlaylistItem = i == 0 ? null : _listItems[i - 1];
				var curItem : PlaylistItem = _listItems[i];
				
				curItem.y = i == 0 ? 0 : prevItem.y + prevItem.curHeight;
				
				if ((curItem.selected == false) && (curItem.hover == true)) {
					if (_listSprite.y + curItem.y + curItem.curHeight > _listMask.y + _listMask.height) {
						_listSprite.y = _listMask.y + _listMask.height - curItem.y - curItem.curHeight;
					}
					
					if (_listSprite.y + curItem.y < _listMask.y) {
						_listSprite.y = _listMask.y - curItem.y;
					}
				}
			}
			
			_listSpriteHeight = curItem.y + curItem.curHeight;
			if (_listSprite.y + _listSpriteHeight < _listMask.y + _listMask.height) {
				_listSprite.y = _listMask.y + _listMask.height - _listSpriteHeight;
			}
			
			if (_listSprite.y > _listMask.y) {
				_listSprite.y = _listMask.y;
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//mouse down and up
		private function mouseDownListener(e : MouseEvent) : void {
			switch (e.currentTarget) {
				case _btnUp:
					_scrollDir = SCROLL_DIR_DOWN;
					_sprite.addEventListener(Event.ENTER_FRAME, enterFrameListener);
					this.root.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
					break;
				case _btnDown:
					_scrollDir = SCROLL_DIR_UP;
					_sprite.addEventListener(Event.ENTER_FRAME, enterFrameListener);
					this.root.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
					break;
			}
		}

		private function mouseUpListener(e : MouseEvent) : void {
			_sprite.removeEventListener(Event.ENTER_FRAME, enterFrameListener);
		}

		
		//enter frame listener : manageing scroll
		private function enterFrameListener(e : Event) : void {
			if (_listSpriteHeight < _listMask.height) {
				return;
			}
			
			if (_scrollDir == SCROLL_DIR_UP) {
				if (_listSprite.y - SCROLL_SPEED + _listSpriteHeight >= _listMask.y + _listMask.height) {
					_listSprite.y -= SCROLL_SPEED;
				} else {
					_listSprite.y = _listMask.y + _listMask.height - _listSpriteHeight;
				}
			} else if (_scrollDir == SCROLL_DIR_DOWN) {
				if (_listSprite.y + SCROLL_SPEED <= _listMask.y) {
					_listSprite.y += SCROLL_SPEED;
				} else {
					_listSprite.y = _listMask.y;
				}
			}
		}

		
		//open/close motion change listener
		private function tweenMotionChangeListener(e : TweenEvent) {
			var endPos : Number = _pos == Settings.PLAYLIST_POS_L ? -_width : 0;
			if ((_tween.finish == endPos) && (_sprite.x == endPos)) {
				_opened = false;
			}
			
			dispatchEvent(new Event(WIDTH_CHANGED));
		}

		
		
		//changing album
		private function albumChangedListener(e : Event) : void {
			var i : int;
			
			_listSprite.y = _listMask.y;
			
			//clearing last items
			while (_listSprite.numChildren != 0) {
				if (_listSprite.getChildAt(0).hasEventListener(MouseEvent.CLICK) == true) {
					_listSprite.getChildAt(0).removeEventListener(MouseEvent.CLICK, clickOnItemListener);
				}
				_listSprite.removeChildAt(0);
			}
			_listItems.length = 0;
			
			
			//creating new
			var curAlbum : XML = _controller.playlist.curAlbum;
			var tracks : XMLList = curAlbum.track;
			
			for (i = 0;i < tracks.length();i++) {
				var track : String = tracks[i];
				var trackId : String = tracks[i].@trackId;
				var artist : String = tracks[i].@artist != undefined ? tracks[i].@artist : curAlbum.@artist;
				var thumbUrl : String = tracks[i].@thumb;
				var album : String = tracks[i].@album != undefined ? tracks[i].@album : curAlbum.@title;
				var year : String = tracks[i].@year != undefined ? tracks[i].@year : curAlbum.@year;
				
				_listItems[i] = new PlaylistItem(track, trackId, artist, thumbUrl, album, year);
				
				if (_controller.playlist.curTrackNum == i) {
					_listItems[i].select();
					_curListItemNum = i;
				}
				
				_listSprite.addChild(_listItems[i]);
				_listItems[i].addEventListener(MouseEvent.CLICK, clickOnItemListener);
				_listItems[i].addEventListener(PlaylistItem.HEIGHT_CHANGED, itemHeightChangedListener);
			}
			
			arrange();
		}

		//changing track
		private function trackChangedListener(e : Event) : void {
			var i : uint;
			for (i = 0;i < _listItems.length;i++) {
				if (PlaylistItem(_listItems[i]).selected == true) {
					PlaylistItem(_listItems[i]).deselect();
				}
			}
			
			if (_controller.playlist.curAlbum.track.length() == 0) {
				return;
			}
			
			_listItems[_controller.playlist.curTrackNum].select();
			_curListItemNum = _controller.playlist.curTrackNum;
		}
		
		//click on item
		private function clickOnItemListener(e : MouseEvent) : void {
			var i : uint;
			for (i = 0;i < _listItems.length;i++) {
				if (e.currentTarget == _listItems[i]) {
					_controller.playlist.curTrackNum = i;
					break;
				}
			}
		}

		//when item opens/closes
		private function itemHeightChangedListener(e : Event) : void {
			arrange();
		}
	}
}
