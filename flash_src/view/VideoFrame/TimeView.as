package view.VideoFrame {
	import model.Settings;

	import utils.Text;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author Family
	 */
	public class TimeView extends Sprite{
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const WIDTH_CHANGED : String = "widthChanged";
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private static const MODE_TIME:uint = 0; //time passed		private static const MODE_TIME_LEFT:uint = 1; //time left		private static const MODE_TIME_DURATION:uint = 2; //time passed / entire time
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller:Controller; //controller instance
		
		private var _time : Number; //time passed
		private var _duration : Number; //duration
		
		private var _txtFld : TextField; //Text field
		
		private var _mode:uint; //mode
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		public function TimeView():void {
			_controller = Controller.getInstance();
			
			_time = 0;
			_duration = 0;
			
			_txtFld = new TextField();
			addChild(_txtFld);
			
			_mode = MODE_TIME_DURATION;
			arrange();
			
			//listeners
			addEventListener(MouseEvent.CLICK, clickListener);
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//setting time and duration
		public function set time(time:uint):void {
			_time = time;
			arrange();
		}
		
		public function set duration(duration:uint):void {
			_duration = duration;
			arrange();
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Private Methods                                                                    //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//arranging view
		private function arrange():void {
			switch (_mode) {
				case MODE_TIME:
					_txtFld.text = Text.getTime(_time);
					break;
				case MODE_TIME_LEFT:
					_txtFld.text = Text.getTime(_time - _duration);
					break;
				case MODE_TIME_DURATION:
					_txtFld.text = Text.getTime(_time) + "/" + Text.getTime(_duration);
					break;
			}
			
			var font:String = Settings.TEXT_FONT;
			var size:Object = Settings.TIME_TEXT_FONT_SIZE;
			var color:uint = _controller.settings.ctrlsMainColor;
			Text.stylizeTextField(_txtFld, font, size, color);
			
			dispatchEvent(new Event(WIDTH_CHANGED));
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//changing Mode on click
		private function clickListener(e:MouseEvent):void {
			if (++_mode > 2) {
				_mode = 0;
			}
			
			arrange();
		}
	}
}
