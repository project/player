package view {
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.display.Stage;
import flash.display.StageAlign;
import flash.display.StageDisplayState;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.system.Capabilities;

import model.Settings;

import utils.Hint;
import utils.RotationFrame;
import utils.UIElements.ScrollTextField;

import view.LibraryFrame.LibraryFrameView;
import view.VideoFrame.ControlsView;
import view.VideoFrame.VideoFrameView;

/**
	 * @author Family
	 */
	public class View {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const SHOW_HINT : String = "showHint"; //hint
		public static const HIDE_HINT : String = "hideHint"; //hint
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private static const DIM_ALPHA : Number = 0.6;

		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller;
		private var _stage : Stage;

		private var _videoFrameView : VideoFrameView; //video, controls and playlist frame
		private var _libararyFrameView : LibraryFrameView; //library frame
		private var _embedPopUp : EmbedPopUp;

		private var _hint : Hint;

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////

		public function View(stg : Stage) : void {
			_controller = Controller.getInstance();
			_stage = stg;
			_stage.scaleMode = StageScaleMode.NO_SCALE;
			_stage.align = StageAlign.TOP_LEFT;
			
			//when settings inited, initing view
			_controller.addEventListener(Controller.INIT_VIEW, initView);
			_controller.addEventListener(Controller.NO_MEDIA_FOUND, showNoMediaError);
			_controller.addEventListener(Controller.SHOW_EMBED_POP_UP, showEmbedPopUpHandler);
			_controller.addEventListener(Controller.HIDE_EMBED_POP_UP, hideEmbedPopUpHandler);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//initing view. adding video frame, and if it's full mode adding library
		public function initView(e : Event) : void {
			_videoFrameView = new VideoFrameView();
			_stage.addChild(_videoFrameView);
			
			//Library only available in Full mode
			if (Controller.MODE == Controller.MODE_FULL) {
				_libararyFrameView = new LibraryFrameView();
				_stage.addChild(_libararyFrameView);
				
				if (_controller.settings.startWithLib == true) {
					_videoFrameView.visible = false;
				} else {
					_libararyFrameView.visible = false;
				}
				
				_videoFrameView.addEventListener(RotationFrame.FRAME_CLOSED, frameClosedListener);
				_libararyFrameView.addEventListener(RotationFrame.FRAME_CLOSED, frameClosedListener);
				
				_libararyFrameView.addEventListener(ControlsView.FULL_SCREEN_ON, switchFullScreen);
				_libararyFrameView.addEventListener(ControlsView.FULL_SCREEN_OFF, switchFullScreen);
				
				_libararyFrameView.addEventListener(SHOW_HINT, showHintListener);
			}
			
			_videoFrameView.addEventListener(ControlsView.FULL_SCREEN_ON, switchFullScreen);
			_videoFrameView.addEventListener(ControlsView.FULL_SCREEN_OFF, switchFullScreen);
			
			_hint = new Hint();
			_stage.addChild(_hint);
			
			_embedPopUp = new EmbedPopUp();
			
			_stage.addEventListener(Event.RESIZE, resizeListener);
			
			//showing hints
			_videoFrameView.addEventListener(SHOW_HINT, showHintListener);
			_stage.addEventListener(HIDE_HINT, hideHintListener);
			
			_controller.addEventListener(Controller.SWITCH_FULL_SCREEN, updateHandler);
			update();
		}

		public function showNoMediaError(e : Event) : void {
			var width : Number = _controller.settings.appWidth;
			var height : Number = _controller.settings.appHeight;
			
			//creating dim
			var dim : Sprite = new Sprite;
			dim.graphics.beginFill(0x000000);
			dim.graphics.drawRect(0, 0, width, height);
			dim.graphics.endFill();
			dim.alpha = DIM_ALPHA;
			
			//error msg
			var textFont : String = Settings.TEXT_FONT;
			var noMediaFoundMsgText : String = Settings.NO_MEDIA_FOUND_MSG;
			var errorMsgFontSize : uint = Settings.ERROR_MSG_FONT_SIZE;
			var errorMsgFontColor : uint = Settings.ERROR_MSG_FONT_COLOR;
			var noMediaFoundMsg : ScrollTextField = new ScrollTextField(width, noMediaFoundMsgText, textFont, errorMsgFontSize, errorMsgFontColor);
			noMediaFoundMsg.y = height / 2 - noMediaFoundMsg.height / 2;
			
			_stage.addChild(dim);
			_stage.addChild(noMediaFoundMsg);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Private methods                                                                    //
		////////////////////////////////////////////////////////////////////////////////////////
		private function update() : void {
			var fullScreen : Boolean = _controller.fullScreen;
			
			var width : Number;
			var height : Number;
			if (fullScreen == true) {
				width = Capabilities.screenResolutionX;
				height = Capabilities.screenResolutionY;
			} else {
				width = _controller.settings.appWidth;
				height = _controller.settings.appHeight;
			}
			
			_embedPopUp.sprite.x = (width - _embedPopUp.sprite.width) / 2;
			_embedPopUp.sprite.y = (height - _embedPopUp.sprite.height) / 2;
		}

		private function showEmbedPopUp() : void {
			_stage.addChild(_embedPopUp.sprite);
		}

		private function hideEmbedPopUp() : void {
			if (_embedPopUp.sprite.parent != null) {
				_embedPopUp.sprite.parent.removeChild(_embedPopUp.sprite);
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private function updateHandler(event : Event) : void {
			update();
		}

		//when closed one frame, automaticly opening another
		private function frameClosedListener(e : Event) : void {
			switch (e.target) {
				case _videoFrameView:
					_libararyFrameView.open();
					break;
				case _libararyFrameView:
					_videoFrameView.open();
					break;
			}
		}

		//switching full screen
		private function switchFullScreen(e : Event) : void {
			if (e.type == ControlsView.FULL_SCREEN_ON) {
				_stage.displayState = StageDisplayState.FULL_SCREEN;
			} else if (e.type == ControlsView.FULL_SCREEN_OFF) {
				_stage.displayState = StageDisplayState.NORMAL;
			}
		}

		private function resizeListener(e : Event) : void {
			if (_stage.displayState == StageDisplayState.NORMAL) {
				_controller.fullScreen = false;
			} else if (_stage.displayState == StageDisplayState.FULL_SCREEN) {
				_controller.fullScreen = true;
			}
		}

		//hint
		private function showHintListener(e : Event) : void {
			_hint.show(e.target.hintText, DisplayObject(e.target), RotationFrame(e.currentTarget).getBorderSprite());
		}

		private function hideHintListener(e : Event) : void {
			_hint.hide();
		}

		private function showEmbedPopUpHandler(event : Event) : void {
			showEmbedPopUp();
		}

		private function hideEmbedPopUpHandler(event : Event) : void {
			hideEmbedPopUp();
		}
	}
}
