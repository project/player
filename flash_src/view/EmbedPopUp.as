package view {
	import fl.controls.TextArea;

	import model.Playlist;
	import model.Settings;

	import uiElements.SimpleBtn;

	import utils.Library;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.system.System;

	/**
	 * @author Robert Soghbatyan (robert.sogbatyan@gmail.com)
	 */
	public class EmbedPopUp {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller;
		private var _settings : Settings;
		private var _playlist : Playlist;
		private var _sprite : Sprite;

		private var _embedText : String;

		private var _text : TextArea;
		private var _copyBtn : SimpleBtn;
		private var _closeBtn : SimpleBtn;

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		public function EmbedPopUp() : void {
			_controller = Controller.getInstance();
			_settings = _controller.settings;
			_playlist = _controller.playlist;
			
			_sprite = Library.createSprite("embedPopUp");
			
			_text = TextArea(_sprite.getChildByName("txt"));
			var btnCopySkin : Sprite = _sprite.getChildByName("copyBtn") as Sprite;
			_copyBtn = new SimpleBtn(btnCopySkin, "Copy", btnCopyClickHandler);
			var btnCloseSkin : Sprite = _sprite.getChildByName("closeBtn") as Sprite;
			_closeBtn = new SimpleBtn(btnCloseSkin, "Close", btnCloseClickHandler);
			
			updateCode();
			
			_playlist.addEventListener(Playlist.ALBUM_CHANGED, albumChangedHandler);
			_playlist.addEventListener(Playlist.TRACK_CHANGED, trackChangedHandler);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		// Getters & Setters                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		public function get sprite() : Sprite {
			return _sprite;
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Private Methods                                                                    //
		////////////////////////////////////////////////////////////////////////////////////////
		private function updateCode() : void {
			_embedText = '<iframe allowFullScreen allowTransparency="true" frameborder="0" width="' + (_settings.appWidth + 20) + '" height="' + (_settings.appHeight + 20) + '" src="' + _settings.embedUrl + '&defaultAlbumId=' + _playlist.curAlbumId + '&defaultTrackId=' + _playlist.curTrackId + '" type="text/html"></iframe>';
			_text.text = _embedText;
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private function btnCopyClickHandler(event : MouseEvent) : void {
			System.setClipboard(_embedText);
		}

		private function btnCloseClickHandler(event : MouseEvent) : void {
			_controller.hideEmbedPopUp();
		}

		private function trackChangedHandler(event : Event) : void {
			updateCode();
		}

		private function albumChangedHandler(event : Event) : void {
			updateCode();
		}
	}
}
