package view.LibraryFrame {
	import model.Settings;

	import utils.ImgLoader;
	import utils.UIElements.ScrollTextField;

	import flash.display.Sprite;
	import flash.events.MouseEvent;

	/**
	 * @author Family
	 */
	public class LibListItem extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private const SPACE : Number = 5;

		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller; //controller instance

		private var _width : Number; //width		private var _height : Number; //height

		private var _bgHoverColor : uint; //bg hover Color
		private var _bgSelectedColor : uint; //bg selected Color
		private var _bgAlpha : Number; //bg alpha

		private var _textFont : String; //text Font		private var _textSize : uint; //text Size		private var _textColor : uint; //text color
		private var _textHoverColor : uint; //text hover color
		private var _textSelectedColor : uint; //text selected color

		
		
		private var _bg : Sprite; //bg
		private var _thumb : ImgLoader; //thumb
		private var _artist : ScrollTextField; //artist text		private var _album : ScrollTextField; //album text
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////

		public function LibListItem(width : Number, height : Number, thumbUrl : String = "", artist : String = "", album : String = "", year : String = "") : void {
			_controller = Controller.getInstance();
			
			this.mouseChildren = false;
			
			_width = width;			_height = height;
			
			_bgHoverColor = _controller.settings.itemBgHoverColor;
			_bgSelectedColor = _controller.settings.itemBgSelectedColor;
			_bgAlpha = _controller.settings.itemBgAlpha;
			_textFont = Settings.TEXT_FONT;			_textSize = _controller.settings.libListTextSize;
			_textColor = _controller.settings.textColor;
			_textHoverColor = _controller.settings.textHoverColor;
			_textSelectedColor = _controller.settings.textSelectedColor;
			
			
			//bg
			_bg = new Sprite;
			_bg.graphics.beginFill(_bgHoverColor);
			_bg.graphics.drawRect(0, 0, _width, _height);
			_bg.graphics.endFill();
			_bg.alpha = 0;
			addChild(_bg);
			
			//album and year
			var text : String = album == "" ? year : year == "" ? album : album + " (" + year + ")";
			text = text == "" ? "Unknown Album" : text;
			_album = new ScrollTextField(_width, text, _textFont, _textSize, _textColor);
			_album.y = _height - SPACE - _album.height;
			addChild(_album);
			
			//artist
			_artist = new ScrollTextField(_width, artist, _textFont, _textSize, _textColor);
			_artist.y = _album.y - SPACE - _artist.height;
			addChild(_artist);
			
			//thumb
			_thumb = new ImgLoader(thumbUrl, _width - 2 * SPACE, _artist.y - 2 * SPACE, -1);
			_thumb.x = _width / 2 - _thumb.width / 2;
			_thumb.y = SPACE;
			addChild(_thumb);
			
			
			addEventListener(MouseEvent.ROLL_OVER, rollOverListener);			addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//selecting and deselecting
		public function select() : void {
			removeEventListener(MouseEvent.ROLL_OVER, rollOverListener);
			removeEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			
			_bg.alpha = _bgAlpha;
			ColorUtils.setColor(_bg, _bgSelectedColor);
			
			_artist.setColor(_textSelectedColor);			_album.setColor(_textSelectedColor);
		}

		public function deselect() : void {
			addEventListener(MouseEvent.ROLL_OVER, rollOverListener);
			addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			
			if (this.root == null) {
				rollOutListener(null);
				return;
			}
			
			if (this.hitTestPoint(this.root.stage.mouseX, this.root.stage.mouseY) == true) {
				rollOverListener(null);
			} else {
				rollOutListener(null);
			}
		}

		
		//set width height
		public function setWidthHeight(width : Number, height : Number) : void {
			_width = width;			_height = height;
			
			if (_bg != null) {
				_bg.width = _width;
				_bg.height = _height;
			}
			
			if (_album != null) {
				_album.setWidth(_width);
				_album.y = _height - SPACE - _album.height;
			}
			
			if (_artist != null) {
				_artist.setWidth(_width);
				_artist.y = _album.y - SPACE - _artist.height;
			}
			
			if (_thumb != null) {
				_thumb.setWidthHeight(_width - 2 * SPACE, _artist.y - 2 * SPACE);
				_thumb.x = _width / 2 - _thumb.width / 2;
				_thumb.y = SPACE;
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//roll over and roll out
		private function rollOverListener(e : MouseEvent) : void {
			_bg.alpha = _bgAlpha;
			ColorUtils.setColor(_bg, _bgHoverColor);
			
			_artist.setColor(_textHoverColor);			_album.setColor(_textHoverColor);
		}

		private function rollOutListener(e : MouseEvent) : void {
			_bg.alpha = 0;
			
			_artist.setColor(_textColor);			_album.setColor(_textColor);
		}
	}
}
