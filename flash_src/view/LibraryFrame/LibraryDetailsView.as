package view.LibraryFrame {
	import model.Playlist;
	import model.Settings;

	import utils.FadeFrame;
	import utils.ImgLoader;
	import utils.Library;
	import utils.UIElements.ScrollTextField;
	import utils.UIElements.Btn;

	import view.VideoFrame.ControlsView;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author Family
	 */
	public class LibraryDetailsView extends FadeFrame {
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private static const INFO_WIDTH : Number = 0.5; //info width scale		private static const LIST_WIDTH : Number = 0.5; //list width scale
		private static const SPACE : Number = 20; //space
		
		private static const SCROLL_DIR_UP : String = "dirUp";
		private static const SCROLL_DIR_DOWN : String = "dirDown";
		private static const SCROLL_SPEED : Number = 5;

		private static const CTRLS_SIZE : Number = 26; //btns size

		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller; //controller instance

		private var _width : Number; //width		private var _widthInfo : Number; //info part width		private var _widthList : Number; //list part width		private var _height : Number; //height

		private var _hoverColor : uint; //hover color
		private var _selectedColor : uint; //selected color

		private var _textFont : String; //text font
		private var _textSize : uint; //font size
		private var _textColor : uint; //text color
		private var _textHoverColor : uint; //text hover color
		private var _textSelectedColor : uint; //text selected color

		
		private var _albumNum : int; //current album num
		private var _albumThumb : ImgLoader; //album thumb
		private var _artist : ScrollTextField; //artist
		private var _album : ScrollTextField; //album
		//private var _description : TextArea; //description

		private var _infoListDiv : Sprite; //info - list div

		private var _btnUp : Btn; //btn up
		private var _btnDown : Btn; //btn down

		private var _listSprite : Sprite; //list sprite		private var _listMask : Sprite; //list mask
		private var _items : Array; //list items
		
		private var _scrollDir : String;

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		
		public function LibraryDetailsView(width : Number) : void {
			_controller = Controller.getInstance();
			
			_width = width;
			_widthInfo = _width * INFO_WIDTH;			_widthList = _width * LIST_WIDTH;
			_height = _controller.settings.appHeight;
			
			_hoverColor = _controller.settings.itemBgHoverColor;
			_selectedColor = _controller.settings.itemBgSelectedColor;
			
			_textFont = Settings.TEXT_FONT;
			_textSize = _controller.settings.libDetailsTextSize;
			_textColor = _controller.settings.textColor;
			_textHoverColor = _controller.settings.textHoverColor;
			_textSelectedColor = _controller.settings.textSelectedColor;
			
			
			_albumNum = -1;
			
			//album thumb
			_albumThumb = new ImgLoader("", _widthInfo - 2 * SPACE, _height / 2 - SPACE);
			_albumThumb.x = _widthInfo / 2 - _albumThumb.width / 2;			_albumThumb.y = SPACE;
			addChild(_albumThumb);
			
			//artist
			_artist = new ScrollTextField(_widthInfo,"", _textFont, _textSize, _textColor);
			addChild(_artist);
			
			//album
			_album = new ScrollTextField(_widthInfo,"", _textFont, _textSize, _textColor);
			addChild(_album);
			
			//description
			/*_description = new TextArea();
			_description.width = _widthInfo - 2 * SPACE;
			_description.height = _height - _album.y - _album.height - 2 * SPACE;
			_description.x = SPACE;			_description.y = _album.y + _album.height;
			addChild(_description);*/
			
			//div
			_infoListDiv = Library.createSprite("vDiv");
			_infoListDiv.height = _height;
			_infoListDiv.x = _widthInfo;
			addChild(_infoListDiv);
			
			
			//list
			//up and down arrows and divs
			var ctrlsMainColor : int = _controller.settings.ctrlsMainColor;
			var ctrlsMainHoverColor : int = _controller.settings.ctrlsMainHoverColor;
			var ctrlsMainAlpha : Number = _controller.settings.ctrlsMainAlpha;
			
			_btnUp = new Btn(_widthList, CTRLS_SIZE, "btnArrowUpSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha,"", false, false, false, true);
			_btnUp.x = _widthInfo;
			addChild(_btnUp);
			
			_btnDown = new Btn(_widthList, CTRLS_SIZE, "btnArrowDownSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, "", false, false, true);
			_btnDown.x = _widthInfo;			_btnDown.y = _height - _btnDown.height;
			addChild(_btnDown);
			
			//list sprite and its mask
			_listSprite = new Sprite();
			_listSprite.x = _widthInfo;			_listSprite.y = _btnUp.height;
			
			addChild(_listSprite);
			
			_listMask = new Sprite();
			_listMask.graphics.beginFill(0xFFFFFF);
			_listMask.graphics.drawRect(0, 0, _widthList, _btnDown.y - _btnUp.height);
			_listMask.graphics.endFill();
			_listMask.x = _widthInfo;			_listMask.y = _btnUp.height;
			addChild(_listMask);
			
			_listSprite.mask = _listMask;
			
			_items = new Array();
			
			_btnDown.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener);			_btnDown.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
			_btnUp.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener);
			_btnUp.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
			
			_controller.playlist.addEventListener(Playlist.ALBUM_CHANGED, trackChangedListener);			_controller.playlist.addEventListener(Playlist.TRACK_CHANGED, trackChangedListener);
		}


		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		public function load(num : uint) : void {
			if ((num == _albumNum) || (num < 0) || (num > _controller.playlist.albums.length())) {
				return;
			}
			
			var i : uint;
			for (i = 0; i < _items.length; i++) {
				_items[i].removeEventListener(MouseEvent.CLICK, clickListener);
				_listSprite.removeChild(_items[i]);
			}
			
			_listSprite.y = _listMask.y;
			_items.length = 0;
			
			_albumNum = num;
			
			var albumXml : XML = _controller.playlist.albums[_albumNum];			var tracks : XMLList = albumXml.children();
			var thumbUrl : String = albumXml.@thumb;
			var artist : String = albumXml.@artist;			var album : String = albumXml.@title;			var year : String = albumXml.@year;			var description : String = albumXml.@description;
			
			_albumThumb.load(thumbUrl);
			
			
			_artist.text = artist;
			_artist.setColor(_textColor);
			_artist.y = _albumThumb.y + _albumThumb.height + SPACE;
			
			_album.text = year == "" ? album : album + " (" + year + ")";
			_album.setColor(_textColor);
			_album.y = _artist.y + _artist.height;
			
			/*_description.text = description;
			//_description.setColor(_textColor);
			_description.y = _album.y + _album.height;*/
			
			for (i = 0;i < tracks.length();i++) {
				var song : String = tracks[i];
				var trackId : String = tracks[i].@trackId;
				
				_items[i] = new LibDetailsItem(trackId != "" ? trackId + " - " + song : song, _widthList);
				_items[i].y = i == 0 ? 0 : _items[i - 1].y + _items[i - 1].height;
				
				_listSprite.addChild(_items[i]);
				
				if ((_albumNum == _controller.playlist.curAlbumNum) && (i == _controller.playlist.curTrackNum)) {
					_items[i].select();
				}
				
				_items[i].addEventListener(MouseEvent.CLICK, clickListener);				_items[i].addEventListener(MouseEvent.DOUBLE_CLICK, doubleClickListener);
			}
		}
		
		
		//setting width and height
		public function setWidthHeight(width : Number, height : Number) : void {
			_width = width;
			_widthInfo = _width * INFO_WIDTH;
			_widthList = _width * LIST_WIDTH;			_height = height;
			
			
			if (_albumThumb != null) {
				_albumThumb.setWidthHeight(_widthInfo - 2 * SPACE, _height / 2 - SPACE);
				_albumThumb.x = _widthInfo / 2 - _albumThumb.width / 2;
			}
			
			if (_artist != null) {
				_artist.setWidth(_widthInfo);
				_artist.y = _albumThumb.y + _albumThumb.height + SPACE;
			}
			
			if (_album != null) {
				_album.setWidth(_widthInfo);
				_album.y = _artist.y + _artist.height;
			}
			
			if (_infoListDiv != null) {
				_infoListDiv.height = _height;
				_infoListDiv.x = _widthInfo;
			}
			
			if (_btnUp != null) {
				_btnUp.setWidth(_widthList);
				_btnUp.x = _widthInfo;
			}
			
			if (_btnDown != null) {
				_btnDown.setWidth(_widthList);
				_btnDown.x = _widthInfo;
				_btnDown.y = _height - _btnDown.height;
			}
			
			if (_listSprite != null) {
				_listSprite.x = _widthInfo;
				_listSprite.y = _btnUp.height;
			}
			
			if (_listMask != null) {
				_listMask.width = _widthList;				_listMask.height = _btnDown.y - _btnUp.height;
				_listMask.x = _widthInfo;
				_listMask.y = _btnUp.height;
			}
			
			var i : int;
			for (i = 0;i < _items.length;i++) {
				LibDetailsItem(_items[i]).setWidth(_widthList);
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//click listener
		private function clickListener(e : MouseEvent) : void {
			var i : uint;
			for (i = 0;i < _items.length;i++) {
				if (e.target == _items[i]) {
					if (_albumNum != _controller.playlist.curAlbumNum) {
						_controller.playlist.curAlbumNum = _albumNum;
					}
					
					_controller.playlist.curTrackNum = i;
				}
			}
		}
		
		
		//double click
		private function doubleClickListener(e : MouseEvent) : void {
			dispatchEvent(new Event(ControlsView.LIB_OFF, true));
		}


		//mouse down and up
		private function mouseDownListener(e : MouseEvent) : void {
			switch (e.currentTarget) {
				case _btnUp:
					_scrollDir = SCROLL_DIR_DOWN;
					addEventListener(Event.ENTER_FRAME, enterFrameListener);
					break;
				case _btnDown:
					_scrollDir = SCROLL_DIR_UP;
					addEventListener(Event.ENTER_FRAME, enterFrameListener);
					break;
			}
		}

		private function mouseUpListener(e : MouseEvent) : void {
			removeEventListener(Event.ENTER_FRAME, enterFrameListener);
		}

		//enter frame listener : manageing scroll
		private function enterFrameListener(e : Event) : void {
			if (_listSprite.height < _listMask.height) {
				return;
			}
			
			if (_scrollDir == SCROLL_DIR_UP) {
				if (_listSprite.y - SCROLL_SPEED + _listSprite.height >= _listMask.y + _listMask.height) {
					_listSprite.y -= SCROLL_SPEED;
				} else {
					_listSprite.y = _listMask.y + _listMask.height - _listSprite.height;
				}
			} else if (_scrollDir == SCROLL_DIR_DOWN) {
				if (_listSprite.y + SCROLL_SPEED <= _listMask.y) {
					_listSprite.y += SCROLL_SPEED;
				} else {
					_listSprite.y = _listMask.y;
				}
			}
		}
		
		
		//track changed
		private function trackChangedListener(e : Event) {
			var i : uint;
			
			for (i = 0;i < _items.length;i++) {
				if (_items[i] != null) {
					_items[i].deselect();
				}
			}
			
			if (_albumNum != _controller.playlist.curAlbumNum) {
				return;
			}
			
			for (i = 0;i < _items.length;i++) {
				if (i == _controller.playlist.curTrackNum) {
					if (_items[i] != null) {
						_items[i].select();
					}
				}
			}
		}
	}
}
