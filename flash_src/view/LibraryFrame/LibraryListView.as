package view.LibraryFrame {
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.None;

	import model.Playlist;
	import model.Settings;

	import utils.FadeFrame;
	import utils.Library;

	import view.VideoFrame.ControlsView;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	/**
	 * @author Family
	 */
	public class LibraryListView extends FadeFrame {
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller; //controller instance

		private var _width : Number; //width		private var _height : Number; //height
		private var _cols : uint; //cols
		private var _rows : uint; //rows

		private var _hDivs : Array; //horizontal divs		private var _vDivs : Array; //vertical divs

		private var _selectedAlbumNum : uint; //selectedAlbumNum

		private var _listSprite : Sprite; //list sprite
		private var _listItems : Array; //list mask
		private var _curPage : uint; //current page
		private var _tween : Tween; //next page transition tween

		private var _timer : Timer; //timer for click / doubleclick

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////

		public function LibraryListView(width : Number) : void {
			_controller = Controller.getInstance();
			
			_width = width;			_height = _controller.settings.appHeight;			_cols = _controller.settings.libCols;			_rows = _controller.settings.libRows;
			
			
			//divs
			var i : uint;
			_hDivs = new Array();			_vDivs = new Array();
			for (i = 0;i < _cols - 1;i++) {
				_vDivs[i] = Library.createSprite("vDiv");
				_vDivs[i].height = _height;
				_vDivs[i].x = (i + 1) * _width / _cols;
				addChild(_vDivs[i]);
			}
			for (i = 0;i < _rows - 1;i++) {
				_hDivs[i] = Library.createSprite("hDiv");
				_hDivs[i].width = _width;
				_hDivs[i].y = (i + 1) * _height / _rows;
				addChild(_hDivs[i]);
			}
			
			
			//list sprite
			_listSprite = new Sprite();
			addChild(_listSprite);
			
			
			//next page transition tween
			_tween = new Tween(_listSprite, "alpha", None.easeNone, 0, 1, 0.25, true);
			_tween.stop();
			
			
			_listItems = new Array();
			
			_curPage = getPage(_controller.playlist.curAlbumNum);
			loadPage(_curPage);
			
			
			//listeners
			_controller.playlist.addEventListener(Playlist.ALBUM_CHANGED, albumChangedListener);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//prev and next page
		public function prevPage() : void {
			if (_curPage == 0) {
				return;
			}
			
			_curPage--;
			loadPage(_curPage);
		}

		public function nextPage() : void {
			if ((_curPage + 1) * _cols * _rows >= _controller.playlist.albums.length()) {
				return;
			}
			
			_curPage++;
			loadPage(_curPage);
		}

		//setting width and height
		public function setWidthHeight(width : Number, height : Number) : void {
			_width = width;			_height = height;
			
			var i : uint;
			var j : uint;
			var k : uint;
			
			if (_vDivs != null) {
				for (i = 0;i < _vDivs.length;i++) {
					if (_vDivs[i] == null) {
						continue;
					}
					_vDivs[i].height = _height;
					_vDivs[i].x = (i + 1) * _width / _cols;
				}
			}
			
			if (_hDivs != null) {
				for (i = 0;i < _hDivs.length;i++) {
					if (_hDivs[i] == null) {
						continue;
					}
					_hDivs[i].width = _width;
					_hDivs[i].y = (i + 1) * _height / _rows;
				}
			}
			
			if (_listItems != null) {
				for (i = 0;i < _rows;i++) {
					for (j = 0;j < _cols;j++) {
						if (_listItems[k] != null) {
							_listItems[k].x = j * (_width / _cols);
							_listItems[k].y = i * (_height / _rows);
							LibListItem(_listItems[k]).setWidthHeight(_width / _cols, _height / _rows);
						}
						k++;
					}
				}
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Getters & Setters                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		
		// selected album
		public function get selectedAlbumNum() : uint {
			return _selectedAlbumNum;
		}

		public function set selectedAlbumNum(num : uint) : void {
			if ((num < 0) || (num >= _controller.playlist.albums.length())) {
				return;
			}
			
			_selectedAlbumNum = num;
			loadPage(getPage(_selectedAlbumNum));
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Private Methods                                                                    //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//getting page from album num
		private function getPage(albumNum : uint) : uint {
			return Math.floor(albumNum / (_cols * _rows));
		}

		//loading page
		private function loadPage(pageNum : uint) : void {
			_curPage = pageNum;
			
			_tween.addEventListener(TweenEvent.MOTION_FINISH, tweenFinishedListener);
			_tween.continueTo(0, 0.25);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//tween finished
		private function tweenFinishedListener(e : Event) : void {
			_tween.removeEventListener(TweenEvent.MOTION_FINISH, tweenFinishedListener);
			
			var i : uint;
			var j : uint;
			var k : uint;
			
			var albumsList : XMLList = _controller.playlist.albums;
			
			var albumNum : uint = _curPage * _cols * _rows;
			var album : LibListItem;
			
			var thumb : String;
			var artist : String;
			var title : String;
			var year : String;
			
			//clearing last page
			for (i = 0;i < _listItems.length;i++) {
				if (_listSprite.hasEventListener(MouseEvent.CLICK)) {
					_listSprite.removeEventListener(MouseEvent.CLICK, clickListener);
				}
				if (_listSprite.hasEventListener(MouseEvent.DOUBLE_CLICK)) {
					_listSprite.removeEventListener(MouseEvent.DOUBLE_CLICK, doubleClickListener);
				}
				if ((_listItems[i] != null) && (_listItems[i].parent != null)) {
					_listItems[i].parent.removeChild(_listItems[i]);
				}
			}
			
			
			//loading new
			for (i = 0;i < _rows;i++) {
				for (j = 0;j < _cols;j++) {
					if (albumNum >= albumsList.length()) {
						_tween.continueTo(1, 0.25);
						return;
					}
					
					thumb = albumsList[albumNum].@thumb;
					artist = albumsList[albumNum].@artist;
					title = albumsList[albumNum].@title;
					year = albumsList[albumNum].@year;
					
					album = new LibListItem(_width / _cols, _height / _rows, thumb, artist, title, year);
					album.x = j * (_width / _cols);
					album.y = i * (_height / _rows);
					_listSprite.addChild(album);
					if (albumNum == _controller.playlist.curAlbumNum) {
						album.select();
					}
					
					album.doubleClickEnabled = true;
					album.addEventListener(MouseEvent.CLICK, clickListener);
					album.addEventListener(MouseEvent.DOUBLE_CLICK, doubleClickListener);
					
					_listItems[k++] = album;
					
					albumNum++;
				}
			}
			
			_tween.continueTo(1, 0.25);
		}

		//click
		private function clickListener(e : MouseEvent) : void {
			var i : uint;
			for (i = 0;i < _listItems.length;i++) {
				if (e.target == _listItems[i]) {
					_selectedAlbumNum = _curPage * _cols * _rows + i;
				}
			}
			
			_timer = new Timer(250, 1);
			_timer.addEventListener(TimerEvent.TIMER_COMPLETE, timerCompleteListener);
			_timer.start();
		}

		//double click
		private function doubleClickListener(e : MouseEvent) : void {
			_timer.stop();
			_timer.removeEventListener(TimerEvent.TIMER_COMPLETE, timerCompleteListener);
			_timer = null;
                
			_controller.playlist.curAlbumNum = _selectedAlbumNum;
			dispatchEvent(new Event(ControlsView.LIB_OFF, true));
		}

		//double click timer complete
		private function timerCompleteListener(e : TimerEvent) : void {
			fadeOut();
		}

		
		//album changed
		private function albumChangedListener(e : Event) : void {
			var i : uint;
			
			var curAlbumNum : uint = _controller.playlist.curAlbumNum;
			for (i = 0;i < _listItems.length;i++) {
				_listItems[i].deselect();
				
				if ((_curPage * _cols * _rows + i) == curAlbumNum) {
					_listItems[i].select();
				}
			}
		}
	}
}
