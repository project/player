package view.LibraryFrame {
	import flash.events.MouseEvent;

	import utils.FadeFrame;
	import utils.Library;
	import utils.RotationFrame;
	import utils.UIElements.Btn;

	import view.VideoFrame.ControlsView;

	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * @author Family
	 */
	public class LibraryFrameView extends RotationFrame {
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private static const CTRLS_SIZE : uint = 26;

		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller; //controller instance

		private var _width : Number; //width		private var _height : Number; //height

		private var _bgColor : uint; //bg color
		private var _bgAlpha : Number; //bg alpha

		
		private var _bg : Sprite; //bg

		private var _listView : LibraryListView; //list of albums
		private var _detailsView : LibraryDetailsView; //details of album

		private var _leftBtn : Btn; //left btn		private var _rightBtn : Btn; //right btn		private var _backBtn : Btn; //back to list view btn
		private var _libOffBtn : Btn; //lib off btn
		private var _fullScreenBtn : Btn; //full screen btn
		
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////

		public function LibraryFrameView() : void {
			super();
			
			_controller = Controller.getInstance();
			
			_width = _controller.settings.appWidth;			_height = _controller.settings.appHeight;
			
			_bgColor = _controller.settings.framesBgColor;
			_bgAlpha = _controller.settings.framesBgAlpha;
			
			//creating bg
			_bg = new Sprite();
			_bg.graphics.beginFill(_bgColor);
			_bg.graphics.drawRect(0, 0, _width, _height);
			_bg.graphics.endFill();
			_bg.alpha = _bgAlpha;
			//_sprite.addChild(_bg);
			
			//btns
			var ctrlsMainColor : int = _controller.settings.ctrlsMainColor;
			var ctrlsMainHoverColor : int = _controller.settings.ctrlsMainHoverColor;
			var ctrlsMainAlpha : Number = _controller.settings.ctrlsMainAlpha;
			
			var backBtnHint : String = _controller.settings.backBtnHint;			var libOffBtnHint : String = _controller.settings.libOffBtnHint;			var fullScreenBtnHint : String = _controller.settings.fullScreenBtnHint;
			
			_leftBtn = new Btn(CTRLS_SIZE, _height, "btnLeftSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, "", false, true);
			_sprite.addChild(_leftBtn);
			
			_rightBtn = new Btn(CTRLS_SIZE, _height, "btnRightSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, "", true);
			_rightBtn.x = _width - _rightBtn.width;
			_sprite.addChild(_rightBtn);
			
			_fullScreenBtn = new Btn(CTRLS_SIZE, CTRLS_SIZE, "btnFullScreenSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, fullScreenBtnHint, false, false, true);
			_fullScreenBtn.x = _width - _fullScreenBtn.width;
			_fullScreenBtn.y = _height - _fullScreenBtn.height;
			_fullScreenBtn.curLabel = _controller.fullScreen == true ? ControlsView.FULL_SCREEN_OFF : ControlsView.FULL_SCREEN_ON;
			_controller.addEventListener(Controller.SWITCH_FULL_SCREEN, fullScreenSwitchedListener);
			_sprite.addChild(_fullScreenBtn);
			
			_libOffBtn = new Btn(CTRLS_SIZE, CTRLS_SIZE, "btnLibOffSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, libOffBtnHint, false, false, true);
			_libOffBtn.x = _width - _libOffBtn.width;
			_libOffBtn.y = _fullScreenBtn.y - _libOffBtn.height;
			_sprite.addChild(_libOffBtn);
			
			addEventListener(ControlsView.LIB_OFF, close);
			
			
			//listView
			_listView = new LibraryListView(_width - _leftBtn.width - _rightBtn.width);
			_listView.x = _leftBtn.width;
			_sprite.addChild(_listView);
			_listView.fadeIn();
			
			
			//detailsView
			_detailsView = new LibraryDetailsView(_width - _leftBtn.width - _rightBtn.width);
			_detailsView.x = _leftBtn.width;
			
			_backBtn = new Btn(CTRLS_SIZE, CTRLS_SIZE, "btnBackSkin", -1, -1, 1, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, backBtnHint, false, false, true);
			_backBtn.x = _width - _leftBtn.width - _rightBtn.width;
			_backBtn.y = _libOffBtn.y - _libOffBtn.height;
			_detailsView.addChild(_backBtn);
			_backBtn.addEventListener(MouseEvent.CLICK, clickListener);
			
			
			
			//listeners
			_listView.addEventListener(FadeFrame.FRAME_FADED_OUT, frameClosedListener);			_detailsView.addEventListener(FadeFrame.FRAME_FADED_OUT, frameClosedListener);
			_leftBtn.addEventListener(MouseEvent.CLICK, clickListener);			_rightBtn.addEventListener(MouseEvent.CLICK, clickListener);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Protected Methods                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//setting width and height
		protected override function setWidthHeight(width : Number, height : Number) : void {
			_width = width;			_height = height;

			if (_bg != null) {
				_bg.width = _width;
				_bg.height = _height;
			}
			
			if (_leftBtn != null) {
				_leftBtn.hght = _height;
			}
			
			if (_rightBtn != null) {
				_rightBtn.hght = _height;
				_rightBtn.x = _width - _rightBtn.width;
			}
			
			if (_fullScreenBtn != null) {
				_fullScreenBtn.x = _width - _fullScreenBtn.width;
				_fullScreenBtn.y = _height - _fullScreenBtn.height;
			}
			
			if (_libOffBtn != null) {
				_libOffBtn.x = _width - _libOffBtn.width;
				_libOffBtn.y = _fullScreenBtn.y - _libOffBtn.height;
			}
			
			if (_listView != null) {
				_listView.x = _leftBtn.width;
				_listView.setWidthHeight(_width - _leftBtn.width - _rightBtn.width, _height);
			}
			
			if (_detailsView != null) {
				_detailsView.x = _leftBtn.width;
				_detailsView.setWidthHeight(_width - _leftBtn.width - _rightBtn.width, _height);
			}
			
			if (_backBtn != null) {
				_backBtn.x = _width - _leftBtn.width - _rightBtn.width;
				_backBtn.y = _libOffBtn.y - _libOffBtn.height;
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private function frameClosedListener(e : Event) : void {
			if (e.target == _listView) {
				_sprite.removeChild(_listView);				_sprite.addChild(_detailsView);
				_detailsView.load(_listView.selectedAlbumNum);
				_detailsView.fadeIn();
			} else if (e.target == _detailsView) {
				_sprite.removeChild(_detailsView);
				_sprite.addChild(_listView);
				_listView.fadeIn();
			}
		}

		
		private function clickListener(e : Event) : void {
			switch (e.target) {
				case _leftBtn:
					if (_detailsView.parent == _sprite) {
						_listView.selectedAlbumNum--;
						_detailsView.load(_listView.selectedAlbumNum);
					} else {
						_listView.prevPage();
					}
					break;
				case _rightBtn:
					if (_detailsView.parent == _sprite) {
						_listView.selectedAlbumNum++;
						_detailsView.load(_listView.selectedAlbumNum);
					} else {
						_listView.nextPage();
					}
					break;
				case _backBtn:
					_detailsView.fadeOut();
					break;
			}
		}

		private function fullScreenSwitchedListener(e : Event) : void {
			_fullScreenBtn.curLabel = _controller.fullScreen == true ? ControlsView.FULL_SCREEN_OFF : ControlsView.FULL_SCREEN_ON;
		}
	}
}
