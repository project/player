package view.LibraryFrame {
	import model.Settings;

	import utils.UIElements.ScrollTextField;

	import flash.display.Sprite;
	import flash.events.MouseEvent;

	/**
	 * @author Family
	 */
	public class LibDetailsItem extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private static const SPACE : Number = 5;

		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller; //controller instance

		private var _bg : Sprite; //bg
		private var _txtFld : ScrollTextField; //track title

		private var _bgHoverColor : uint; //bg hover color
		private var _bgSelectedColor : uint; //bg selected color
		private var _bgAlpha : Number; //bg alpha

		private var _textFont : String; //text font
		private var _textSize : uint; //text size
		private var _textColor : uint; //text color
		private var _textHoverColor : uint; //text hover color
		private var _textSelectedColor : uint; //text selected color

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		
		public function LibDetailsItem(text : String, width : Number) : void {
			this.mouseChildren = false;
			this.doubleClickEnabled = true;
			
			_controller = Controller.getInstance();
			
			_bgHoverColor = _controller.settings.itemBgHoverColor;
			_bgSelectedColor = _controller.settings.itemBgSelectedColor;
			_bgAlpha = _controller.settings.itemBgAlpha;
			_textFont = Settings.TEXT_FONT;
			_textSize = _controller.settings.libDetailsTextSize;
			_textColor = _controller.settings.textColor;
			_textHoverColor = _controller.settings.textHoverColor;
			_textSelectedColor = _controller.settings.textSelectedColor;
			
			
			//bg
			_bg = new Sprite;
			_bg.graphics.beginFill(_bgHoverColor);
			_bg.graphics.drawRect(0, 0, width, 2 * SPACE);
			_bg.graphics.endFill();
			_bg.alpha = 0;
			addChild(_bg);
			
			text = text == "" ? "Unknown" : text;
			_txtFld = new ScrollTextField(width - 2 * SPACE, text, _textFont, _textSize, _textColor, false, false, false, false, "left", ScrollTextField.ALIGN_LEFT);
			_txtFld.x = SPACE;
			_txtFld.y = SPACE;
			addChild(_txtFld);
			
			_bg.height = 2 * SPACE + _txtFld.height;
			
			
			//listeners
			addEventListener(MouseEvent.ROLL_OVER, rollOverListener);			addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//selecting and deselecting
		public function select():void {
			removeEventListener(MouseEvent.ROLL_OVER, rollOverListener);
			removeEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			
			_bg.alpha = _bgAlpha;
			ColorUtils.setColor(_bg, _bgSelectedColor);
			
			_txtFld.setColor(_textSelectedColor);
		}
		
		public function deselect():void {
			addEventListener(MouseEvent.ROLL_OVER, rollOverListener);
			addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			
			if (this.hitTestPoint(this.root.stage.mouseX, this.root.stage.mouseY) == true) {
				rollOverListener(null);
			} else {
				rollOutListener(null);
			}
		}
		
		//set width height
		public function setWidth(width : Number) : void {
			if (_bg != null) {
				_bg.width = width;
			}
			
			if (_txtFld != null) {
				_txtFld.setWidth(width - 2 * SPACE);
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//roll over and roll out
		private function rollOverListener(e : MouseEvent) : void {
			_bg.alpha = _bgAlpha;
			ColorUtils.setColor(_bg, _bgHoverColor);
			
			_txtFld.setColor(_textHoverColor);
		}

		private function rollOutListener(e : MouseEvent) : void {
			_bg.alpha = 0;
			
			_txtFld.setColor(_textColor);
		}
	}
}
