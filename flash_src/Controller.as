package {
	import model.Playlist;
	import model.Settings;

	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.EventDispatcher;

	/**
	 * @author Family
	 */
	public class Controller extends EventDispatcher {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const INIT_VIEW : String = "initView"; //dispatched, when settings and playlist inited
		public static const NO_MEDIA_FOUND : String = "noMediaFound"; //dispatched, when playlist is missing

		public static const SWITCH_TO_VIDEO_FRAME : String = "switchToVideoFrame"; //dispatched, when playlist frame is closed
		public static const SWITCH_TO_LIBRARY_FRAME : String = "switchToLibraryFrame"; //dispatched, when video frame is closed

		public static const SWITCH_FULL_SCREEN : String = "switchFullScreen"; //dispatched, when full screen switched on or off
		
		public static const SHOW_EMBED_POP_UP : String = "showEmbedPopUp"; //dispatched, when full screen switched on or off
		public static const HIDE_EMBED_POP_UP : String = "hideEmbedPopUp"; //dispatched, when full screen switched on or off

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const FREE_VERSION : Boolean = true;
		public static const WATERMARK_URL : String = "http://web-dorado.com/files/fromSVP.php";
//		public static const WATERMARK_URL : String = "http://web-dorado.com/files/fromSVPDrupal.php";

		public static const MODE : uint = MODE_FULL; //player mode. single track, one album or library

		public static const MODE_LIGHT : uint = 0; //single track mode
		public static const MODE_NORMAL : uint = 1; //one album mode
		public static const MODE_FULL : uint = 2; //library mode

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private static var INSTANCE : Controller; //instance of controller
		
		private var _stage : Stage;

		private var _settings : Settings; //settings
		private var _playlist : Playlist; //playlist

		private var _fullScreen : Boolean; //full screen state

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		public function Controller() : void {
		}

		//getting Controller instance
		public static function getInstance() : Controller {
			if (INSTANCE == null) {
				INSTANCE = new Controller();
			}
			return INSTANCE;
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		//init
		public function init(stage : Stage, settingsUrl : String, playListUrl : String, trackUrl : String, defaultAlbumId : String, defaultTrackId : String) : void {
			_stage = stage;
			
			_fullScreen = false;
			
			settingsUrl = settingsUrl == "" ? "settings.xml" : settingsUrl;
			_settings = new Settings(settingsUrl);
			
			_playlist = new Playlist(playListUrl, trackUrl, defaultAlbumId, defaultTrackId);
			
			_settings.addEventListener(Settings.SETTINGS_INITED, settingsInitedListener);
			_settings.init();
		}
		
		public function showEmbedPopUp() : void {
			dispatchEvent(new Event(SHOW_EMBED_POP_UP));
		}
		
		public function hideEmbedPopUp() : void {
			dispatchEvent(new Event(HIDE_EMBED_POP_UP));
		}


		////////////////////////////////////////////////////////////////////////////////////////
		// Getters & Setters                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		public function get stage() : Stage {
			return _stage;
		}
		
		//settings
		public function get settings() : Settings {
			return _settings;
		}

		//playlist
		public function get playlist() : Playlist {
			return _playlist;
		}
		
		//fullscreen
		public function get fullScreen() : Boolean {
			return _fullScreen;
		}
		public function set fullScreen(fullScreen : Boolean) : void {
			_fullScreen = fullScreen;
			dispatchEvent(new Event(SWITCH_FULL_SCREEN));
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//when settings inited, initing playlist
		private function settingsInitedListener(e : Event) : void {
			_playlist.addEventListener(Playlist.PLAYLIST_INITED, playlistInitedListener);
			_playlist.addEventListener(Playlist.NO_MEDIA_FOUND, playlistNotFound);
			_playlist.init();
		}

		//when playlist inited initing view
		private function playlistInitedListener(e : Event) : void {
			dispatchEvent(new Event(INIT_VIEW));
		}
		
		//when playlist inited initing view
		private function playlistNotFound(e : Event) : void {
			dispatchEvent(new Event(NO_MEDIA_FOUND));
		}
	}
}
