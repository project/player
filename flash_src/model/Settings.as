package model {
	import debug.Debug;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	/**
	 * @author Family
	 */
	public class Settings extends EventDispatcher {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const SETTINGS_INITED : String = "settingsInited"; //dispatched, when settings inited


		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const CTRLS_POS_T : uint = 1;
		public static const CTRLS_POS_B : uint = 2;
		public static const PLAYLIST_POS_L : uint = 1;
		public static const PLAYLIST_POS_R : uint = 2;
		public static const WATERMARK_POS_T_L : uint = 1; 
		public static const WATERMARK_POS_T_R : uint = 2; 
		public static const WATERMARK_POS_B_L : uint = 3; 
		public static const WATERMARK_POS_B_R : uint = 4; 

		public static const REPEAT_OFF : String = "repeatOff";
		public static const REPEAT_ONE : String = "repeatOne";
		public static const REPEAT_ALL : String = "repeatAll";

		public static const SHUFFLE_ON : String = "shuffleOn";
		public static const SHUFFLE_OFF : String = "shuffleOff";
		
		public static const TEXT_FONT : String = "Verdana";
		public static const TIME_TEXT_FONT_SIZE : uint = 10;
		
		public static const HINT_BG_COLOR : uint = 0x14171A;
		public static const HINT_TEXT_FONT_SIZE : uint = 10;
		public static const HINT_TEXT_FONT_COLOR : uint = 0x999999;
		
		public static const NO_MEDIA_FOUND_MSG : String = "No Media Found";
		public static const STREAM_NOT_FOUND_MSG : String = "Stream Not Found";
		public static const ALBUM_IS_EMPTY_MSG : String = "Album Is Empty";
		public static const ERROR_MSG_FONT_SIZE : uint = 14;
		public static const ERROR_MSG_FONT_COLOR : uint = 0xFFFFFF;
		
		public static const END_REPLAY_BTN_TEXT : String = "Replay";
		public static const END_NEXT_BTN_TEXT : String = "Next";
		
		

		//defaults
		private static const APP_WIDTH : uint = 640;
		private static const APP_HEIGHT : uint = 360;
		private static const PLAYLIST_WIDTH : uint = 200;
		

		private static const START_WITH_LIB : Boolean = false;

		private static const AUTO_PLAY : Boolean = false;
		private static const AUTO_NEXT : Boolean = true;
		private static const AUTO_NEXT_ALBUM : Boolean = true;
		private static const DEFAULT_VOL : Number = 0.5;
		private static const DEFAULT_REPEAT : String = REPEAT_OFF;
		private static const DEFAULT_SHUFFLE : String = SHUFFLE_OFF;
		
		private static const AUTOHIDE_TIME : uint = 3;

		private static const CENTER_BTN_ALPHA : Number = 1;
		
		private static const LOADING_ANIM_TYPE : String = "1";

		private static const KEEP_ASPECT_RATIO : Boolean = true;

		private static const CLICK_ON_VID : Boolean = true;
		private static const SPACE_ON_VID : Boolean = true;
		private static const MOUSE_WHEEL : Boolean = true;

		private static const CTRLS_POS : uint = CTRLS_POS_B;
		private static const CTRLS_STACK : String = "playPrev,playPause,stop,playNext,vol,time,+,repeat,shuffle,hd,share,playlist,lib,fullScreen";
		private static const CTRLS_OVER_VIDEO : Boolean = true;
		private static const CTRLS_AUTO_HIDE : Boolean = true;

		private static const WATERMARK_URL : String = "";
		private static const WATERMARK_POS : uint = WATERMARK_POS_B_R;
		private static const WATERMARK_SIZE : Number = 80;
		private static const WATERMARK_SPACING : Number = 20;
		private static const WATERMARK_ALPHA : Number = 0.2;

		private static const PLAYLIST_POS : uint = PLAYLIST_POS_R;
		private static const PLAYLIST_OVER_VIDEO : Boolean = true;
		private static const PLAYLIST_AUTO_HIDE : Boolean = true;
		private static const PLAYLIST_TEXT_SIZE : uint = 12;

		private static const LIB_COLS : uint = 3;
		private static const LIB_ROWS : uint = 2;

		private static const LIB_LIST_TEXT_SIZE : uint = 12;
		private static const LIB_DETAILS_TEXT_SIZE : uint = 12;
		
		
		private static const PLAY_BTN_HINT : String = "play";
		private static const PAUSE_BTN_HINT : String = "pause";
		private static const PLAY_PAUSE_BTN_HINT : String = "toggle pause";
		private static const STOP_BTN_HINT : String = "stop";
		private static const PLAY_PREV_BTN_HINT : String = "play previous";
		private static const PLAY_NEXT_BTN_HINT : String = "play next";
		private static const VOL_BTN_HINT : String = "volume";
		private static const REPEAT_BTN_HINT : String = "repeat";
		private static const SHUFFLE_BTN_HINT : String = "shuffle";
		private static const HD_BTN_HINT : String = "HD";
		private static const SHARE_BTN_HINT : String = "Share";
		private static const PLAYLIST_BTN_HINT : String = "open/close playlist";
		private static const LIB_ON_BTN_HINT : String = "open library";
		private static const LIB_OFF_BTN_HINT : String = "close library";
		private static const FULL_SCREEN_BTN_HINT : String = "switch full screen";
		private static const BACK_BTN_HINT : String = "back to list";
		
		
		private static const APP_BG_COLOR : int = 0x212326;
		private static const VID_BG_COLOR : int = -1;

		private static const FRAMES_BG_COLOR : int = 0x363b40;
		private static const FRAMES_BG_ALPHA : Number = 0.75;

		private static const CTRLS_COLOR : int = 0xb3b3b3;
		private static const CTRLS_MAIN_HOVER_COLOR : int = 0xFFFFFF;
		private static const CTRLS_MAIN_ALPHA : Number = 1;
		private static const SLIDE_COLOR : int = 0x409fff;

		private static const ITEM_BG_HOVER_COLOR : uint = 0xCFCFCF;
		private static const ITEM_BG_SELECTED_COLOR : uint = 0xa6d3ff;
		private static const ITEM_BG_ALPHA : Number = 0.35;

		private static const TEXT_COLOR : uint = 0x999999;
		private static const TEXT_HOVER_COLOR : uint = 0x000000;
		private static const TEXT_SELECTED_COLOR : Number = 0x000000;

		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _settingsXml : XML;
		private var _settingsXmlLoader : URLLoader;
		private var _settingsXmlUrl : String;

		
		//general
		private var _appWidth : uint; //width
		private var _appHeight : uint; //height
		private var _playlistWidth : uint; //playlist width
		

		private var _startWithLib : Boolean; //show lib on start

		//settings
		private var _autoPlay : Boolean; //auto play video on start
		private var _autoNext : Boolean; //auto play next song in album
		private var _autoNextAlbum : Boolean; //auto play next album, when current ends
		private var _defaultVol : Number; //vol on start
		private var _defaultRepeat : String; //repeat on start
		private var _defaultShuffle : String; //shuffle on start
		
		private var _autohideTime : uint; //autohide time in full screen mode

		//video
		private var _centerBtnAlpha : Number; //center btns alpha
		
		private var _loadingAnimType : String; //loading anim type

		private var _keepAspectRatio : Boolean; //keep video aspect ratio

		private var _clickOnVid : Boolean; //toggle pause when click on vid
		private var _spaceOnVid : Boolean; //toggle pause when space pressed
		private var _mouseWheel : Boolean; //vol up down on mouse scroll
		
		//controls
		private var _ctrlsPos : uint; //controls pos. top or bootom
		private var _ctrlsStack : String; //controls names divided by ",". after "+" all controls will be on right
		private var _ctrlsOverVid : Boolean; //controls over video
		private var _ctrlsAutoHide : Boolean; //slide out controls when mouse roll out
		
		//watermark
		private var _watermarkUrl : String; //watermark image url
		private var _watermarkPos : uint; //watermark pos. top-left, top-right, bottom-left or bottom-right
		private var _watermarkSize : Number; //watermark width / height
		private var _watermarkSpacing : Number; //watermark distance from edges
		private var _watermarkAlpha : Number; //watermark alpha

		//playlist
		private var _playlistPos : uint; //playlist pos. right or left
		private var _playlistOverVid : Boolean; //playlist over video
		private var _playlistAutoHide : Boolean; //close playlist, when mouse roll out of vid
		private var _playlistTextSize : uint; //font size of playlist items
		
		//library
		private var _libCols : uint; //columns of lib
		private var _libRows : uint; //rows of lib

		private var _libListTextSize : uint; //font size of library list items
		private var _libDetailsTextSize : uint; //font size of library details items
		
		//hints
		private var _playBtnHint : String; //play btn hint
		private var _pauseBtnHint : String; //pause btn hint
		private var _playPauseBtnHint : String; //play / pause btn hint
		private var _stopBtnHint : String; //stop btn hint
		private var _playPrevBtnHint : String; //play prev btn hint
		private var _playNextBtnHint : String; //play next btn hint
		private var _volBtnHint : String; //vol btn hint
		private var _repeatBtnHint : String; //repeat btn hint
		private var _shuffleBtnHint : String; //shuffle btn hint
		private var _hdBtnHint : String; //HD btn hint
		private var _shareBtnHint : String; //HD btn hint
		private var _playlistBtnHint : String; //playlist btn hint
		private var _libOnBtnHint : String; //lib on btn hint
		private var _libOffBtnHint : String; //lib off btn hint
		private var _fullScreenBtnHint : String; //full screen btn hint
		private var _backBtnHint : String; //back btn hint
		
		private var _endReplayBtnText : String; //back btn hint
		private var _endNextBtnText : String; //back btn hint

		//colors
		private var _appBgColor : int; //color of app bg
		private var _vidBgColor : int; //color of video bg

		private var _framesBgColor : uint; //color of frames bg
		private var _framesBgAlpha : Number; //alpha of frames bg

		private var _ctrlsMainColor : int; //btn logos and slide head color
		private var _ctrlsMainHoverColor : int; //btn logos and slide head hover color
		private var _ctrlsMainAlpha : Number; //btn logos and slide head alpha
		private var _slideColor : int; //color of slide

		private var _itemBgHoverColor : uint; //playlist and lib items bg hover color
		private var _itemBgSelectedColor : uint; //playlist and lib items bg selected color
		private var _itemBgAlpha : Number; ////playlist and lib items bg alpha

		private var _textColor : uint; //text color
		private var _textHoverColor : uint; //text hover color
		private var _textSelectedColor : uint; //text selected color
		
		private var _embedUrl : String;

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////

		public function Settings(xmlUrl : String) : void {
			_settingsXmlUrl = xmlUrl;
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//initing settings
		public function init() : void {
			_settingsXmlLoader = new URLLoader();
			_settingsXmlLoader.addEventListener(Event.COMPLETE, parseSettingsXml);
			_settingsXmlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorListener);
			_settingsXmlLoader.load(new URLRequest(_settingsXmlUrl));
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Getters & Setters                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		
		public function get appWidth() : uint {
			return _appWidth;
		}
		
		public function get appHeight() : uint {
			return _appHeight;
		}
		
		public function get playlistWidth() : uint {
			return _playlistWidth;
		}
		
		public function get startWithLib() : Boolean {
			return _startWithLib;
		}
		
		public function get autoPlay() : Boolean {
			return _autoPlay;
		}
		
		public function get autoNext() : Boolean {
			return _autoNext;
		}
		
		public function get autoNextAlbum() : Boolean {
			return _autoNextAlbum;
		}
		
		public function get defaultVol() : Number {
			return _defaultVol;
		}
		
		public function get defaultRepeat() : String {
			return _defaultRepeat;
		}
		
		public function get defaultShuffle() : String {
			return _defaultShuffle;
		}
		
		public function get autohideTime() : uint {
			return _autohideTime;
		}
		
		public function get centerBtnAlpha() : Number {
			return _centerBtnAlpha;
		}
		
		public function get loadingAnimType() : String {
			return _loadingAnimType;
		}
		
		public function get keepAspectRatio() : Boolean {
			return _keepAspectRatio;
		}
		
		public function get clickOnVid() : Boolean {
			return _clickOnVid;
		}
		
		public function get spaceOnVid() : Boolean {
			return _spaceOnVid;
		}
		
		public function get mouseWheel() : Boolean {
			return _mouseWheel;
		}
		
		public function get ctrlsPos() : uint {
			return _ctrlsPos;
		}
		
		public function get ctrlsStack() : String {
			return _ctrlsStack;
		}
		
		public function get ctrlsOverVid() : Boolean {
			return _ctrlsOverVid;
		}
		
		public function get ctrlsAutoHide() : Boolean {
			return _ctrlsAutoHide;
		}
		
		public function get watermarkUrl() : String {
			return _watermarkUrl;
		}
		
		public function get watermarkPos() : uint {
			return _watermarkPos;
		}
		
		public function get watermarkSize() : Number {
			return _watermarkSize;
		}
		
		public function get watermarkSpacing() : Number {
			return _watermarkSpacing;
		}
		
		public function get watermarkAlpha() : Number {
			return _watermarkAlpha;
		}
		
		public function get playlistPos() : uint {
			return _playlistPos;
		}
		
		public function get playlistOverVid() : Boolean {
			return _playlistOverVid;
		}
		
		public function get playlistAutoHide() : Boolean {
			return _playlistAutoHide;
		}
		
		public function get playlistTextSize() : uint {
			return _playlistTextSize;
		}
		
		public function get libCols() : uint {
			return _libCols;
		}
		
		public function get libRows() : uint {
			return _libRows;
		}
		
		public function get libListTextSize() : uint {
			return _libListTextSize;
		}
		
		public function get libDetailsTextSize() : uint {
			return _libDetailsTextSize;
		}
		
		public function get playBtnHint() : String {
			return _playBtnHint;
		}
		
		public function get pauseBtnHint() : String {
			return _pauseBtnHint;
		}
		
		public function get playPauseBtnHint() : String {
			return _playPauseBtnHint;
		}
		
		public function get stopBtnHint() : String {
			return _stopBtnHint;
		}
		
		public function get playPrevBtnHint() : String {
			return _playPrevBtnHint;
		}
		
		public function get playNextBtnHint() : String {
			return _playNextBtnHint;
		}
		
		public function get volBtnHint() : String {
			return _volBtnHint;
		}
		
		public function get repeatBtnHint() : String {
			return _repeatBtnHint;
		}
		
		public function get shuffleBtnHint() : String {
			return _shuffleBtnHint;
		}
		
		public function get hdBtnHint() : String {
			return _hdBtnHint;
		}
		
		public function get shareBtnHint() : String {
			return _shareBtnHint;
		}
		
		public function get playlistBtnHint() : String {
			return _playlistBtnHint;
		}
		
		public function get libOnBtnHint() : String {
			return _libOnBtnHint;
		}
		
		public function get libOffBtnHint() : String {
			return _libOffBtnHint;
		}
		
		public function get fullScreenBtnHint() : String {
			return _fullScreenBtnHint;
		}
		
		public function get backBtnHint() : String {
			return _backBtnHint;
		}
		
		public function get endReplayBtnText() : String {
			return _endReplayBtnText;
		}
		
		public function get endNextBtnText() : String {
			return _endNextBtnText;
		}
		
		public function get appBgColor() : int {
			return _appBgColor;
		}
		
		public function get vidBgColor() : int {
			return _vidBgColor;
		}
		
		public function get framesBgColor() : uint {
			return _framesBgColor;
		}
		
		public function get framesBgAlpha() : Number {
			return _framesBgAlpha;
		}
		
		public function get ctrlsMainColor() : int {
			return _ctrlsMainColor;
		}
		
		public function get ctrlsMainHoverColor() : int {
			return _ctrlsMainHoverColor;
		}
		
		public function get ctrlsMainAlpha() : Number {
			return _ctrlsMainAlpha;
		}
		
		public function get slideColor() : int {
			return _slideColor;
		}
		
		public function get itemBgHoverColor() : uint {
			return _itemBgHoverColor;
		}
		
		public function get itemBgSelectedColor() : uint {
			return _itemBgSelectedColor;
		}
		
		public function get itemBgAlpha() : Number {
			return _itemBgAlpha;
		}
		
		public function get textColor() : uint {
			return _textColor;
		}
		
		public function get textHoverColor() : uint {
			return _textHoverColor;
		}
		
		public function get textSelectedColor() : uint {
			return _textSelectedColor;
		}
		
		public function get embedUrl() : String {
			return _embedUrl;
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////

		//parsing xml from xml file, if it doesn't exist setting defaults
		private function parseSettingsXml(e : Event = null) : void {
			_settingsXml = e != null ? XML(e.target.data) : new XML();
			
			
			_appWidth = (_settingsXml.appWidth != undefined) && (_settingsXml.appWidth != "") ? _settingsXml.appWidth : APP_WIDTH;
			_appHeight = (_settingsXml.appHeight != undefined) && (_settingsXml.appHeight != "") ? _settingsXml.appHeight : APP_HEIGHT;
			_playlistWidth = (_settingsXml.playlistWidth != undefined) && (_settingsXml.playlistWidth != "") ? _settingsXml.playlistWidth : PLAYLIST_WIDTH;
			_playlistWidth = _playlistWidth > _appWidth ? _appWidth : _playlistWidth;
		
			_startWithLib = (_settingsXml.startWithLib != undefined) && (_settingsXml.startWithLib != "") ? (_settingsXml.startWithLib == "true") : START_WITH_LIB;

			//settings
			_autoPlay = (_settingsXml.autoPlay != undefined) && (_settingsXml.autoPlay != "") ? (_settingsXml.autoPlay == "true") : AUTO_PLAY;
			_autoNext = (_settingsXml.autoNext != undefined) && (_settingsXml.autoNext != "") ? (_settingsXml.autoNext == "true") : AUTO_NEXT;
			_autoNextAlbum = (_settingsXml.autoNextAlbum != undefined) && (_settingsXml.autoNextAlbum != "") ? (_settingsXml.autoNextAlbum == "true") : AUTO_NEXT_ALBUM;
			_defaultVol = (_settingsXml.defaultVol != undefined) && (_settingsXml.defaultVol != "") ? _settingsXml.defaultVol : DEFAULT_VOL;
			_defaultVol = _defaultVol < 0 ? 0 : _defaultVol > 1 ? 1 : _defaultVol;
			_defaultRepeat = (_settingsXml.defaultRepeat != undefined) && (_settingsXml.defaultRepeat != "") ? _settingsXml.defaultRepeat : DEFAULT_REPEAT;
			_defaultShuffle = (_settingsXml.defaultShuffle != undefined) && (_settingsXml.defaultShuffle != "") ? _settingsXml.defaultShuffle : DEFAULT_SHUFFLE;
			
			_autohideTime = (_settingsXml.autohideTime != undefined) && (_settingsXml.autohideTime != "") ? _settingsXml.autohideTime : AUTOHIDE_TIME;

			//video
			_centerBtnAlpha = (_settingsXml.centerBtnAlpha != undefined) && (_settingsXml.centerBtnAlpha != "") ? _settingsXml.centerBtnAlpha : CENTER_BTN_ALPHA;
			
			_loadingAnimType = (_settingsXml.loadinAnimType != undefined) && (_settingsXml.loadinAnimType != "") ? _settingsXml.loadinAnimType : LOADING_ANIM_TYPE;
		
			_keepAspectRatio = (_settingsXml.keepAspectRatio != undefined) && (_settingsXml.keepAspectRatio != "") ? (_settingsXml.keepAspectRatio == "true") : KEEP_ASPECT_RATIO;

			_clickOnVid = (_settingsXml.clickOnVid != undefined) && (_settingsXml.clickOnVid != "") ? (_settingsXml.clickOnVid == "true") : CLICK_ON_VID;
			_spaceOnVid = (_settingsXml.spaceOnVid != undefined) && (_settingsXml.spaceOnVid != "") ? (_settingsXml.spaceOnVid == "true") : SPACE_ON_VID;
			_mouseWheel = (_settingsXml.mouseWheel != undefined) && (_settingsXml.mouseWheel != "") ? (_settingsXml.mouseWheel == "true") : MOUSE_WHEEL;
		
			//controls
			_ctrlsPos = (_settingsXml.ctrlsPos != undefined) && (_settingsXml.ctrlsPos != "") ? _settingsXml.ctrlsPos : CTRLS_POS;
			_ctrlsStack = (_settingsXml.ctrlsStack != undefined) && (_settingsXml.ctrlsStack != "") ? _settingsXml.ctrlsStack : CTRLS_STACK;
			_ctrlsOverVid = (_settingsXml.ctrlsOverVid != undefined) && (_settingsXml.ctrlsOverVid != "") ? (_settingsXml.ctrlsOverVid == "true") : CTRLS_OVER_VIDEO;
			_ctrlsAutoHide = (_settingsXml.ctrlsAutoHide != undefined) && (_settingsXml.ctrlsAutoHide != "") ? (_settingsXml.ctrlsAutoHide == "true") : CTRLS_AUTO_HIDE;
		
			//watermark
			_watermarkUrl = (_settingsXml.watermarkUrl != undefined) && (_settingsXml.watermarkUrl != "") ? _settingsXml.watermarkUrl : WATERMARK_URL;
			_watermarkPos = (_settingsXml.watermarkPos != undefined) && (_settingsXml.watermarkPos != "") ? _settingsXml.watermarkPos : WATERMARK_POS;
			_watermarkSize = (_settingsXml.watermarkSize != undefined) && (_settingsXml.watermarkSize != "") ? _settingsXml.watermarkSize : WATERMARK_SIZE;
			_watermarkSpacing = (_settingsXml.watermarkSpacing != undefined) && (_settingsXml.watermarkSpacing != "") ? _settingsXml.watermarkSpacing : WATERMARK_SPACING;
			_watermarkAlpha = (_settingsXml.watermarkAlpha != undefined) && (_settingsXml.watermarkAlpha != "") ? _settingsXml.watermarkAlpha : WATERMARK_ALPHA;

			//playlist
			_playlistPos = (_settingsXml.playlistPos != undefined) && (_settingsXml.playlistPos != "") ? _settingsXml.playlistPos : PLAYLIST_POS;
			_playlistOverVid = (_settingsXml.playlistOverVid != undefined) && (_settingsXml.playlistOverVid != "") ? (_settingsXml.playlistOverVid == "true") : PLAYLIST_OVER_VIDEO;
			_playlistAutoHide = (_settingsXml.playlistAutoHide != undefined) && (_settingsXml.playlistAutoHide != "") ? (_settingsXml.playlistAutoHide == "true") : PLAYLIST_AUTO_HIDE;
			_playlistTextSize = (_settingsXml.playlistTextSize != undefined) && (_settingsXml.playlistTextSize != "") ? _settingsXml.playlistTextSize : PLAYLIST_TEXT_SIZE;
		
			//library
			_libCols = (_settingsXml.libCols != undefined) && (_settingsXml.libCols != "") ? _settingsXml.libCols : LIB_COLS;
			_libRows = (_settingsXml.libRows != undefined) && (_settingsXml.libRows != "") ? _settingsXml.libRows : LIB_ROWS;
		
			_libListTextSize = (_settingsXml.libListTextSize != undefined) && (_settingsXml.libListTextSize != "") ? _settingsXml.libListTextSize : LIB_LIST_TEXT_SIZE;
			_libDetailsTextSize = (_settingsXml.libDetailsTextSize != undefined) && (_settingsXml.libDetailsTextSize != "") ? _settingsXml.libDetailsTextSize : LIB_DETAILS_TEXT_SIZE;
			
			//hints
			_playBtnHint = (_settingsXml.playBtnHint != undefined) && (_settingsXml.playBtnHint != "") ? _settingsXml.playBtnHint : PLAY_BTN_HINT;
			_pauseBtnHint = (_settingsXml.pauseBtnHint != undefined) && (_settingsXml.pauseBtnHint != "") ? _settingsXml.pauseBtnHint : PAUSE_BTN_HINT;
			_playPauseBtnHint = (_settingsXml.playPauseBtnHint != undefined) && (_settingsXml.playPauseBtnHint != "") ? _settingsXml.playPauseBtnHint : PLAY_PAUSE_BTN_HINT;
			_stopBtnHint = (_settingsXml.stopBtnHint != undefined) && (_settingsXml.stopBtnHint != "") ? _settingsXml.stopBtnHint : STOP_BTN_HINT;
			_playPrevBtnHint = (_settingsXml.playPrevBtnHint != undefined) && (_settingsXml.playPrevBtnHint != "") ? _settingsXml.playPrevBtnHint : PLAY_PREV_BTN_HINT;
			_playNextBtnHint = (_settingsXml.playNextBtnHint != undefined) && (_settingsXml.playNextBtnHint != "") ? _settingsXml.playNextBtnHint : PLAY_NEXT_BTN_HINT;
			_volBtnHint = (_settingsXml.volBtnHint != undefined) && (_settingsXml.volBtnHint != "") ? _settingsXml.volBtnHint : VOL_BTN_HINT;
			_repeatBtnHint = (_settingsXml.repeatBtnHint != undefined) && (_settingsXml.repeatBtnHint != "") ? _settingsXml.repeatBtnHint : REPEAT_BTN_HINT;
			_shuffleBtnHint = (_settingsXml.shuffleBtnHint != undefined) && (_settingsXml.shuffleBtnHint != "") ? _settingsXml.shuffleBtnHint : SHUFFLE_BTN_HINT;
			_hdBtnHint = (_settingsXml.hdBtnHint != undefined) && (_settingsXml.hdBtnHint != "") ? _settingsXml.hdBtnHint : HD_BTN_HINT;
			_shareBtnHint = (_settingsXml.shareBtnHint != undefined) && (_settingsXml.shareBtnHint != "") ? _settingsXml.shareBtnHint : SHARE_BTN_HINT;
			_playlistBtnHint = (_settingsXml.playlistBtnHint != undefined) && (_settingsXml.playlistBtnHint != "") ? _settingsXml.playlistBtnHint : PLAYLIST_BTN_HINT;
			_libOnBtnHint = (_settingsXml.libOnBtnHint != undefined) && (_settingsXml.libOnBtnHint != "") ? _settingsXml.libOnBtnHint : LIB_ON_BTN_HINT;
			_libOffBtnHint = (_settingsXml.libOffBtnHint != undefined) && (_settingsXml.libOffBtnHint != "") ? _settingsXml.libOffBtnHint : LIB_OFF_BTN_HINT;
			_fullScreenBtnHint = (_settingsXml.fullScreenBtnHint != undefined) && (_settingsXml.fullScreenBtnHint != "") ? _settingsXml.fullScreenBtnHint : FULL_SCREEN_BTN_HINT;
			_backBtnHint = (_settingsXml.backBtnHint != undefined) && (_settingsXml.backBtnHint != "") ? _settingsXml.backBtnHint : BACK_BTN_HINT;
			
			_endReplayBtnText = (_settingsXml.replayBtnText != undefined) && (_settingsXml.replayBtnText != "") ? _settingsXml.replayBtnText : END_REPLAY_BTN_TEXT;
			_endNextBtnText = (_settingsXml.nextBtnText != undefined) && (_settingsXml.nextBtnText != "") ? _settingsXml.nextBtnText : END_NEXT_BTN_TEXT;

			//colors
			_appBgColor = (_settingsXml.appBgColor != undefined) && (_settingsXml.appBgColor != "") ? _settingsXml.appBgColor : APP_BG_COLOR;
			_vidBgColor = (_settingsXml.vidBgColor != undefined) && (_settingsXml.vidBgColor != "") ? _settingsXml.vidBgColor : VID_BG_COLOR;
		
			_framesBgColor = (_settingsXml.framesBgColor != undefined) && (_settingsXml.framesBgColor != "") ? _settingsXml.framesBgColor : FRAMES_BG_COLOR;
			_framesBgAlpha = (_settingsXml.framesBgAlpha != undefined) && (_settingsXml.framesBgAlpha != "") ? _settingsXml.framesBgAlpha : FRAMES_BG_ALPHA;

			_ctrlsMainColor = (_settingsXml.ctrlsMainColor != undefined) && (_settingsXml.ctrlsMainColor != "") ? _settingsXml.ctrlsMainColor : CTRLS_COLOR;
			_ctrlsMainHoverColor = (_settingsXml.ctrlsMainHoverColor != undefined) && (_settingsXml.ctrlsMainHoverColor != "") ? _settingsXml.ctrlsMainHoverColor : CTRLS_MAIN_HOVER_COLOR;
			_ctrlsMainAlpha = (_settingsXml.ctrlsMainAlpha != undefined) && (_settingsXml.ctrlsMainAlpha != "") ? _settingsXml.ctrlsMainAlpha : CTRLS_MAIN_ALPHA;
			_slideColor = (_settingsXml.slideColor != undefined) && (_settingsXml.slideColor != "") ? _settingsXml.slideColor : SLIDE_COLOR;
		
			_itemBgHoverColor = (_settingsXml.itemBgHoverColor != undefined) && (_settingsXml.itemBgHoverColor != "") ? _settingsXml.itemBgHoverColor : ITEM_BG_HOVER_COLOR;
			_itemBgSelectedColor = (_settingsXml.itemBgSelectedColor != undefined) && (_settingsXml.itemBgSelectedColor != "") ? _settingsXml.itemBgSelectedColor : ITEM_BG_SELECTED_COLOR;
			_itemBgAlpha = (_settingsXml.itemBgAlpha != undefined) && (_settingsXml.itemBgAlpha != "") ? _settingsXml.itemBgAlpha : ITEM_BG_ALPHA;

			_textColor = (_settingsXml.textColor != undefined) && (_settingsXml.textColor != "") ? _settingsXml.textColor : TEXT_COLOR;
			_textHoverColor = (_settingsXml.textHoverColor != undefined) && (_settingsXml.textHoverColor != "") ? _settingsXml.textHoverColor : TEXT_HOVER_COLOR;
			_textSelectedColor = (_settingsXml.textSelectedColor != undefined) && (_settingsXml.textSelectedColor != "") ? _settingsXml.textSelectedColor : TEXT_SELECTED_COLOR;
			
			_embedUrl = (_settingsXml.embed != undefined) && (_settingsXml.embed != "") ? _settingsXml.embed : "";
			
			dispatchEvent(new Event(SETTINGS_INITED));
		}

		//if settings.xml is missing, setting default values
		private function ioErrorListener(e : IOErrorEvent) : void {
			parseSettingsXml();
		}
	}
}
