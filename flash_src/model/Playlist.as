package model {
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IOErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;

/**
 * @author Family
 */
public class Playlist extends EventDispatcher {
    ////////////////////////////////////////////////////////////////////////////////////////
    // Events                                                                             //
    ////////////////////////////////////////////////////////////////////////////////////////
    public static const PLAYLIST_INITED : String = "playlistIninted"; //dispatched, when playlist inited
    public static const NO_MEDIA_FOUND : String = "noMediaFound"; //dispatched, if xml file is not found, or it's empty

    public static const ALBUM_CHANGED : String = "albumChanged"; //dispatched, when album changed
    public static const TRACK_CHANGED : String = "trackChanged"; //dispatched, when track changed

    ////////////////////////////////////////////////////////////////////////////////////////
    // Variables                                                                          //
    ////////////////////////////////////////////////////////////////////////////////////////
    private var _controller : Controller; //controller instance

    private var _playlistXml : XML; //playlist xml
    private var _playlistXmlLoader : URLLoader; //playlist xml loader
    private var _playlistXmlUrl : String; //playlist xml url

    private var _fmsUrl : String; //flash media server url

    private var _trackUrl : String; //url of single track

    private var _albums : XMLList; //albums list
    private var _curAlbumNum : uint; //current album number
    private var _curTrackNum : uint; //current track num

    private var _defaultAlbumId : String; //current album number
    private var _defaultTrackId : String; //current track num

    private var _repeat : String; //repeat
    private var _shuffle : String; //shuffle

    private var _shuffleList : Array;

    private var _autoNext : Boolean; //auto play next track, when current is finished
    private var _autoNextAlbum : Boolean; //auto play next album, when current is finished


    ////////////////////////////////////////////////////////////////////////////////////////
    // Constructor                                                                        //
    ////////////////////////////////////////////////////////////////////////////////////////

    public function Playlist(playlistUrl : String, trackUrl : String, defaultAlbumId : String, defaultTrackId : String) : void {
        _controller = Controller.getInstance();

        _playlistXmlUrl = playlistUrl;
        _trackUrl = trackUrl;

        _defaultAlbumId = defaultAlbumId;
        _defaultTrackId = defaultTrackId;
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Public Methods                                                                     //
    ////////////////////////////////////////////////////////////////////////////////////////

    //initing playlist and default values
    public function init() : void {
        _repeat = _controller.settings.defaultRepeat;
        _shuffle = _controller.settings.defaultShuffle;

        if (Controller.MODE == Controller.MODE_LIGHT) {
            _autoNext = false;
        } else {
            _autoNext = _controller.settings.autoNext;
        }

        _autoNextAlbum = _controller.settings.autoNextAlbum;

        if (_playlistXmlUrl != "") {
            _playlistXmlLoader = new URLLoader();
            _playlistXmlLoader.addEventListener(Event.COMPLETE, parsePlaylistXml);
            _playlistXmlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorListener);
            _playlistXmlLoader.load(new URLRequest(_playlistXmlUrl));
        } else {
            ioErrorListener(null);
        }
    }


    //playing next track
    public function playPrevTrack() : void {
        if (_shuffle == Settings.SHUFFLE_ON) {
            //shuffle all albums
//                curAlbumNum = randAlbumNum();
            curTrackNum = getRandTrackNum();
        } else {
            curTrackNum--;
        }
    }

    //play previous track
    public function playNextTrack() : void {
        if (_shuffle == Settings.SHUFFLE_ON) {
            //shuffle all albums
//                curAlbumNum = randAlbumNum();
            curTrackNum = getRandTrackNum();
        } else {
            curTrackNum++;
        }
    }


    //when ended playing track
    public function trackEnd() {
        switch (_repeat) {
            case Settings.REPEAT_OFF:
                if (_autoNext == true) {
                    playNextTrack();
                }
                break;
            case Settings.REPEAT_ONE:
                dispatchEvent(new Event(TRACK_CHANGED));
                break;
            case Settings.REPEAT_ALL:
                playNextTrack();
                break;
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Getters & Setters                                                                  //
    ////////////////////////////////////////////////////////////////////////////////////////

    //getting albums list
    public function get fmsUrl() : String {
        return _fmsUrl;
    }

    //getting albums list
    public function get albums() : XMLList {
        return _albums;
    }

    //current album
    public function get curAlbum() : XML {
        return _albums[_curAlbumNum];
    }

    //current album num
    public function get curAlbumNum() : int {
        return _curAlbumNum;
    }

    public function set curAlbumNum(num : int) : void {
        if (num < 0) {
            return;
//				_curAlbumNum = albums.length() - 1;
        } else if (num > albums.length() - 1) {
            return;
//				_curAlbumNum = 0;
        } else {
            _curAlbumNum = num;
        }

        generateShuffleList();

        _curTrackNum = _shuffle == Settings.SHUFFLE_ON ? getRandTrackNum() : 0;
        dispatchEvent(new Event(ALBUM_CHANGED));
    }

    //current track
    public function get currentTrack() : XML {
        return curAlbum.children()[_curTrackNum];
    }

    //current track num
    public function get curTrackNum() : int {
        return _curTrackNum;
    }

    public function set curTrackNum(num : int) : void {
        if (num < 0) {
            if (_repeat == Settings.REPEAT_ALL) {
                _curTrackNum = curAlbum.children().length() - 1;
                dispatchEvent(new Event(TRACK_CHANGED));
            } else if ((_autoNextAlbum == true) && (_curAlbumNum > 0)) {
                curAlbumNum--;
                _curTrackNum = curAlbum.children().length() - 1;
                dispatchEvent(new Event(TRACK_CHANGED));
            }
        } else if (num > curAlbum.children().length() - 1) {
            if (_repeat == Settings.REPEAT_ALL) {
                _curTrackNum = 0;
                dispatchEvent(new Event(TRACK_CHANGED));
            } else if (_autoNextAlbum == true) {
                curAlbumNum++;
            }
        } else {
            _curTrackNum = num;
            dispatchEvent(new Event(TRACK_CHANGED));
        }
    }

    public function get curAlbumId() : String {
        if (curAlbum == null) {
            return "";
        }
        return curAlbum.@id;
    }

    public function get curTrackId() : String {
        if (currentTrack == null) {
            return "";
        }
        return currentTrack.@id;
    }


    //repeat
    public function set repeat(repeat : String) : void {
        if ((repeat == Settings.REPEAT_ALL) || (repeat == Settings.REPEAT_ONE) || (repeat == Settings.REPEAT_OFF)) {
            _repeat = repeat;
        }
    }

    //shuffle
    public function set shuffle(shuffle : String) : void {
        if ((shuffle == Settings.SHUFFLE_ON) || (shuffle == Settings.SHUFFLE_OFF)) {
            _shuffle = shuffle;
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Private methods                                                                    //
    ////////////////////////////////////////////////////////////////////////////////////////
    private function randAlbumNum() : int {
        if (_albums.length() < 2) {
            return 0;
        }
        var num : int;
        do
            num = Math.round(Math.random() * (_albums.length() - 1));
        while (num == curAlbumNum);
        return num;
    }

    private function generateShuffleList() : void {
        _shuffleList = null;
        if ((curAlbum == null) || (curAlbum.children().length() == 0)) {
            return;
        }

        var tempArray = new Array();
        for (var i : uint = 0; i < curAlbum.children().length(); i++) {
            tempArray.push(i);
        }
        _shuffleList = new Array();
        for (i = 0; i < curAlbum.children().length(); i++) {
            var rand : int = Math.round(Math.random() * (tempArray.length - 1));
            _shuffleList.push(tempArray.splice(rand, 1));
        }
    }

    private function getRandTrackNum() : int {
        if ((_shuffleList == null) || (curAlbum.children().length() == 0)) {
            return 0;
        }

        if (_shuffleList.length == 0) {
            generateShuffleList();
        }

        if (_shuffleList[0] == _curTrackNum) {
            return _shuffleList.pop();
        }
        return _shuffleList.shift();
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    // Listeners                                                                          //
    ////////////////////////////////////////////////////////////////////////////////////////
    private function parsePlaylistXml(e : Event) : void {
        _playlistXml = e == null ? new XML('<library><album><track url="' + _trackUrl + '"></track></album></library>') : XML(e.target.data);

        _fmsUrl = _playlistXml.@fmsUrl;

        if (Controller.MODE == Controller.MODE_NORMAL) {
            _playlistXml = new XML('<library>' + XML(_playlistXml.album[0]).toXMLString() + '</library>');
        } else if (Controller.MODE == Controller.MODE_LIGHT) {
            _playlistXml = new XML('<library><album>' + XML(_playlistXml.album[0].track[0]).toXMLString() + '</album></library>');
        }

        if (Controller.FREE_VERSION == true) {
            _albums = _playlistXml.albumFree;
        } else {
            _albums = _playlistXml.album;
        }

        //if xml is empty, dispatching error msg
        if (_albums.length() == 0) {
            dispatchEvent(new Event(NO_MEDIA_FOUND));
            return;
        }

        _curAlbumNum = 0;
        _curTrackNum = 0;
        if (_defaultAlbumId != "") {
            for (var i : uint = 0; i < _albums.length(); i++) {
                if (_albums[i].@id == _defaultAlbumId) {
                    _curAlbumNum = i;
                    break;
                }
            }

            if (_defaultTrackId != "") {
                var tracksList : XMLList = _albums[_curAlbumNum].track;
                for (i = 0; i < tracksList.length(); i++) {
                    if (tracksList[i].@id == _defaultTrackId) {
                        _curTrackNum = i;
                        break;
                    }
                }
            }
        }

        generateShuffleList();
        if (_shuffle == Settings.SHUFFLE_ON) {
            _curTrackNum = getRandTrackNum();
        }

        dispatchEvent(new Event(PLAYLIST_INITED));
    }

    //if playlist.xml is missing, dispatching error msg
    private function ioErrorListener(e : IOErrorEvent) : void {
        if (_trackUrl == "") {
            dispatchEvent(new Event(NO_MEDIA_FOUND));
        } else {
            parsePlaylistXml(null);
        }
    }
}
}
