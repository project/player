package utils {
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.ColorTransform;
	import flash.net.URLRequest;

	/**
	 * @author Family
	 */
	public class ImgLoader extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const IMG_LOADED : String = "imgLoaded"; //dispatched, when img loaded		public static const IMG_LOADING_FAILED : String = "imgLoadingFailed"; //dispatched, when img loading failed
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller; //controller instance

		private var _bg : Sprite; //bg
		private var _loadingAnim : Sprite; //loading animation
		private var _url : String; //image url
		private var _loader : Loader; //image loader

		private var _width : Number; //width		private var _height : Number; //height

		private var _keepAspectRatio : Boolean; //keep img's aspect ratio or not
		private var _resizeToImg : Boolean; //resize to img, when it's loaded

		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////

		public function ImgLoader(url : String, width : Number, height : Number, bgColor : int = -1, resizeToImg : Boolean = false, keepAspectRatio : Boolean = true) : void {
			_controller = Controller.getInstance();
			
			_url = url;
			_width = width;
			_height = height;
			
			//creating bg and coloring it, if color == -1, making it transparent
			_bg = new Sprite;
			_bg.graphics.beginFill(0xFFFFFF);
			_bg.graphics.drawRect(0, 0, _width, _height);
			_bg.graphics.endFill();
			if (bgColor == -1) {
				_bg.alpha = 0;
			} else {
				var ct : ColorTransform = _bg.transform.colorTransform;
				ct.color = bgColor;
				_bg.transform.colorTransform = ct;
			}
			addChild(_bg);
			
			//initing resize to img
			_resizeToImg = resizeToImg;
			
			//initing keepAspectRatio
			_keepAspectRatio = keepAspectRatio;
			
			
			//loading anim
			var loadingAnimType : String = _controller.settings.loadingAnimType;
			_loadingAnim = Library.createSprite("loadingAnim" + loadingAnimType);
			_loadingAnim.width > width / 3 ? _loadingAnim.width = width / 3 : _loadingAnim.width;			_loadingAnim.height > height / 3 ? _loadingAnim.height = height / 3 : _loadingAnim.height;
			_loadingAnim.scaleX < _loadingAnim.scaleY ? _loadingAnim.scaleY = _loadingAnim.scaleX : _loadingAnim.scaleX = _loadingAnim.scaleY;
			_loadingAnim.x = _width / 2;			_loadingAnim.y = _height / 2;
			
			load(_url);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//loading img
		public function load(url : String) : void {
			if (url == "") {
				return;
			}
			
			//clearing last loader
			if (_loader != null) {
				if (_loader.parent != null) {
					_loader.parent.removeChild(_loader);
				}
				if (_loader.contentLoaderInfo.hasEventListener(Event.COMPLETE)) {
					_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, loadedListener);
				}
			}
			
			if (url == "") {
				if ((_loadingAnim != null) && (_loadingAnim.parent != null)) {
					_loadingAnim.parent.removeChild(_loadingAnim);
				}
				
				if ((_bg != null) && (_bg.parent != null)) {
					_bg.parent.removeChild(_bg);
				}
				
				return;
			}
			
			_bg.width = _width;
			_bg.height = _height;
			_bg.x = 0;
			_bg.y = 0;
			
			_loadingAnim.x = _width / 2;
			_loadingAnim.y = _height / 2;
			
			addChild(_bg);
			addChild(_loadingAnim);
			
			
			//image loader
			_loader = new Loader();
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadedListener);			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, errorListener);
			_loader.load(new URLRequest(url));
		}

		//setting width and height
		public function setWidthHeight(width : Number, height : Number) {
			_width = width;			_height = height;
			
			if (_bg != null) {
				_bg.width = _width;				_bg.height = _height;
			}
			
			if (_loadingAnim != null) {
				_loadingAnim.x = _width / 2;
				_loadingAnim.y = _height / 2;
			}
			
			if (_loader != null) {
				_loader.width = _width;
				_loader.height = _height;
			
				if (_keepAspectRatio == true) {
					_loader.scaleX < _loader.scaleY ? _loader.scaleY = _loader.scaleX : _loader.scaleX = _loader.scaleY;
					_loader.x = _width / 2 - _loader.width / 2;
					_loader.y = _height / 2 - _loader.height / 2;
				}
			}
			
			if (_resizeToImg == true) {
				if ((_bg != null) && (_bg.parent != null)) {
					_bg.parent.removeChild(_bg);
				}
				
				_loader.x = 0;
				_loader.y = 0;
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//when img loaded, removing loading anim, setting with and height to img, and aspect ratio
		private function loadedListener(e : Event) : void {
			if ((_loadingAnim != null) && (_loadingAnim.parent != null)) {
				_loadingAnim.parent.removeChild(_loadingAnim);
			}
			
			_loader.width = _bg.width;			_loader.height = _bg.height;
			
			if (_keepAspectRatio == true) {
				_loader.scaleX < _loader.scaleY ? _loader.scaleY = _loader.scaleX : _loader.scaleX = _loader.scaleY;
				_loader.x = _bg.width / 2 - _loader.width / 2;				_loader.y = _bg.height / 2 - _loader.height / 2;
			}
			
			if (_resizeToImg == true) {
				if ((_bg != null) && (_bg.parent != null)) {
					_bg.parent.removeChild(_bg);
				}
				
				_loader.x = 0;				_loader.y = 0;
			}
			
			//smoothing img
			Bitmap(_loader.content).smoothing = true;
			addChild(_loader);
			
			dispatchEvent(new Event(IMG_LOADED));
		}

		//if loading failed
		private function errorListener(e : IOErrorEvent) : void {
			if ((_loadingAnim != null) && (_loadingAnim.parent != null)) {
				_loadingAnim.parent.removeChild(_loadingAnim);
			}
				
			if ((_bg != null) && (_bg.parent != null)) {
				_bg.parent.removeChild(_bg);
			}
			
			dispatchEvent(new Event(IMG_LOADING_FAILED));
		}
	}
}
