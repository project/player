package utils {
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.utils.getDefinitionByName;

	/**
	 * @author admin
	 */
	public class Library {
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		public function Lybrary():void {
		}

		////////////////////////////////////////////////////////////////////////////////////////
		// Public static methods                                                              //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//get class
		public static function getClass(className:String):Class {
			return Class(getDefinitionByName(className));
		}

		//creat sprite by class name
		public static function createSprite(className:String):Sprite {
			var classObject:Class = getClass(className) as Class;
			return Sprite(new classObject());
		}
		
		//creat sprite by class name
		public static function createMC(className:String):MovieClip {
			var classObject:Class = getClass(className) as Class;
			return MovieClip(new classObject());
		}
	}
}
