﻿package utils {
	import flash.text.TextField;
	import flash.text.TextFormat;

	/**
	 * @author 111
	 */
	public class Text {
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		public function Text() : void {
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public static methods                                                              //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//stylizing text field
		public static function stylizeTextField(target : TextField, font : String = "Times New Roman", size : Object = 12, color : Object = 0x000000,
												bold : Object = false, italic : Object = false, underline : Object = false,
												selectable : Boolean = false, autoSize : String = "left"):void {
			var format:TextFormat = new TextFormat;
			format.font = font;
			format.size = size;
			format.color = color;
			format.bold = bold;
			format.italic = italic;
			format.underline = underline;
			
			target.setTextFormat(format);
			
			target.selectable = selectable;
			target.autoSize = autoSize;
		}
		
		//convert seconds to string time
		public static function getTime(seconds:int):String {
			var sign:String = seconds < 0 ? "-" : "";
			seconds = Math.abs(seconds);
			
			var h:uint = seconds / 3600;
			var m:uint = (seconds - h * 3600) / 60;
			var s:uint = seconds - h*3600 - m*60;
			
			var time:String = h == 0 ? "" : String(h) + ":";
			time += m < 10 ? "0" + String(m) + ":" : String(m) + ":";			time += s < 10 ? "0" + String(s) : String(s);
			
			return sign + time;
		}
	}
}
