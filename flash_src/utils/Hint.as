package utils {
	import model.Settings;

	import utils.UIElements.ScrollTextField;

	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	/**
	 * @author Family
	 */
	public class Hint extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var SPACE : Number = 2;
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller; //controller instance
		
		private var _sprite : Sprite; //containing sprite
		
		private var _bg : Sprite; //bg		private var _bgColor : uint; //bg color		private var _bgBorderColor : uint; //bg border color
		private var _txtFld : ScrollTextField; //text
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		public function Hint() {
			_controller = Controller.getInstance();
			
			var appWidth : Number = _controller.settings.appWidth;
			
			_bgColor = Settings.HINT_BG_COLOR;
			_bgBorderColor = Settings.HINT_TEXT_FONT_COLOR;
			
			var font : String = Settings.TEXT_FONT;			var size : uint = Settings.HINT_TEXT_FONT_SIZE;			var color : uint = Settings.HINT_TEXT_FONT_COLOR;
			
			_sprite = new Sprite();
			
			//bg
			_bg = new Sprite();
			_sprite.addChild(_bg);
			
			_txtFld = new ScrollTextField(appWidth, "", font, size, color, false, false, false, false, "left", ScrollTextField.ALIGN_LEFT);
			_txtFld.x = SPACE;			_txtFld.y = SPACE;
			_sprite.addChild(_txtFld);
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//show and hide
		public function show(hintText : String, obj : DisplayObject, borderSprite : Sprite):void {
			if (hintText == "") {
				return;
			}
			
			var rect : Rectangle = borderSprite.getRect(this);
			
			var objCenterX : Number = (obj.getRect(this).left + obj.getRect(this).right) / 2;			var objTopY : Number = obj.getRect(this).top;			var objBottomY : Number = obj.getRect(this).bottom;
			
			_txtFld.text = hintText;
			
			_bg.graphics.clear();			_bg.graphics.lineStyle(0.2, _bgBorderColor);
			_bg.graphics.beginFill(_bgColor);
			_bg.graphics.drawRect(0, 0, _txtFld.width + 2 * SPACE, _txtFld.height + 2 * SPACE);
			_bg.graphics.endFill();
			
			_sprite.x = objCenterX - _sprite.width / 2;
			_sprite.x = _sprite.x + _sprite.width > rect.x + rect.width ? rect.x + rect.width - _sprite.width : _sprite.x;			_sprite.x = _sprite.x < rect.x ? rect.x : _sprite.x;
			
			if ((objTopY - rect.y > _sprite.height + SPACE) || (rect.y + rect.height - objBottomY < _sprite.height + SPACE)) {
				_sprite.y = objTopY - SPACE - _sprite.height;
			} else {
				_sprite.y = objBottomY + SPACE;
			}
			
			addChild(_sprite);
		}
		
		public function hide():void {
			if ((_sprite != null) && (_sprite.parent != null))
			_sprite.parent.removeChild(_sprite);
		}
	}
}
