package utils {
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Strong;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;

	/**
	 * @author Family
	 */
	public class RotationFrame extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const FRAME_OPENED : String = "frameOpened"; //dispatched, when frame is opened		public static const FRAME_CLOSED : String = "frameClosed"; //dispatched, when frame is closed
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _controller : Controller; //controller instance

		private var _width : Number;
		private var _height : Number;
		private var _fullScreenWidth : Number;
		private var _fullScreenHeight : Number;

		protected var _sprite : Sprite; //sprite

		private var _bg : Sprite; //frame bg

		private var _openTween : Tween; //open tween		private var _closeTween : Tween; //close tween
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////

		public function RotationFrame() : void {
			_controller = Controller.getInstance(); //getting controller instance

			_width = _controller.settings.appWidth;			_height = _controller.settings.appHeight;
			
			_fullScreenWidth = Capabilities.screenResolutionX;			_fullScreenHeight = Capabilities.screenResolutionY;
			
			//creating sprite, and aligning it to stage top left
			_sprite = new Sprite;

			addChild(_sprite);			
			
			//creating bg
			_bg = new Sprite;
			_bg.graphics.beginFill(_controller.settings.appBgColor);
			_bg.graphics.drawRect(0, 0, _width, _height);
			_bg.graphics.endFill();
			_sprite.addChild(_bg);
			
			
			//open tween
			_openTween = new Tween(this, "rotationY", Strong.easeOut, -90, 0, 1, true);
			_openTween.stop();
			_openTween.addEventListener(TweenEvent.MOTION_FINISH, tweenFinishedListener);
			
			//close tween
			_closeTween = new Tween(this, "rotationY", Strong.easeIn, 0, 90, 1, true);
			_closeTween.stop();
			_closeTween.addEventListener(TweenEvent.MOTION_FINISH, tweenFinishedListener);
			
			
			//listeners
			_controller.addEventListener(Controller.SWITCH_FULL_SCREEN, arrange);
			
			arrange(null);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//opening frame, and making it visible
		public function open() : void {
			this.visible = true;
			_openTween.start();
		}

		//closing frame, and disableing mouse
		public function close(e : Event = null) : void {
			this.mouseEnabled = false;
			this.mouseChildren = false;
			_closeTween.start();
		}
		
		//getting border rect
		public function getBorderSprite() : Sprite {
			return _bg;
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Protected Methods                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//setting width and height
		protected function setWidthHeight(width : Number, height : Number) : void {
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//when frame opened, enabling mouse. when frame closed, makeing frame unvisible
		private function tweenFinishedListener(e : TweenEvent) : void {
			switch (e.target) {
				case _openTween:
					this.mouseEnabled = true;
					this.mouseChildren = true;
					dispatchEvent(new Event(FRAME_OPENED));
					break;
				case _closeTween:
					this.visible = false;
					dispatchEvent(new Event(FRAME_CLOSED));
					break;
			}
		}

		
		//arrangeing frame
		protected function arrange(e : Event) : void {
			var fullScreen : Boolean = _controller.fullScreen;
			
			if (fullScreen == false) {
				this.x = _width / 2;
				this.y = _height / 2;
				
				_sprite.scaleX = 1;				_sprite.scaleY = 1;
				_sprite.x = -this.x;
				_sprite.y = -this.y;
			
				_bg.width = _width;
				_bg.height = _height;
				
				setWidthHeight(_width, _height);
			} else {
				var scaleX : Number = _fullScreenWidth / _width;				var scaleY : Number = _fullScreenHeight / _height;
				var scale : Number = scaleX < scaleY ? scaleX : scaleY;
				
				_sprite.scaleX = scale;
				_sprite.scaleY = scale;
				
				var fsWidth : Number = _width; 				var fsHeight : Number = _height; 
				scaleX < scaleY ? fsHeight = _fullScreenHeight / scale : fsWidth = _fullScreenWidth / scale;
				
				_bg.width = fsWidth;				_bg.height = fsHeight;
				
				this.x = _width / 2;
				this.y = _height / 2;
				
				_sprite.x = -this.x; 
				_sprite.y = -this.y;
				
				setWidthHeight(fsWidth, fsHeight);
			}
		}
	}
}
