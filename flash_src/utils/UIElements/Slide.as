package utils.UIElements {
	import utils.Library;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author Family
	 */
	public class Slide extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const ACTIVE_LEVEL_CHANGED : String = "activeLevelChanged";		public static const DRAG_STARTED : String = "dragStarted";		public static const DRAG_ENDED : String = "dragEnded";

		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private static const INACTIVE_ALPHA : Number = 0.2;

		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		protected var _width : Number; //slide width
		protected var _height : Number; //slide height

		protected var _skin : Sprite; //slide skin

		protected var _bg : Sprite; //slide bg
		protected var _bgColor : int; //color overlay for slide bg's child called "color"		protected var _bgHover : Sprite; //slide bg hover
		protected var _bgHoverColor : int; //color overlay for slide bg hovers's child called "color"
		protected var _bgAlpha : Number; //bg alpha

		protected var _slideInactive : Sprite; //slide inactive line		protected var _slideActive : Sprite; //slide active line
		protected var _slideColor : int; //color overlay for slide's active line "color" child, inactive line will have the same color, but less alpha
		protected var _head : Sprite; //slide head		protected var _headColor : int; //slide head color		protected var _headHover : Sprite; //slide head's hover
		protected var _headHoverColor : int; //color for slide's head hover child "color"		protected var _slideAlpha : Number; //slide and head Alpha
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//initing skin, colors and alphas

		public function Slide(width : Number, height : Number, skin : String, bgColor : int, bgHoverColor : int, bgAlpha : Number,
							slideColor : int, slideHeadColor : int,	slideHeadHoverColor : int, slideAlpha : Number) : void {
			
			_width = width;
			_height = height;
				
			//colors and alphas	
			_bgColor = bgColor;			_bgHoverColor = bgHoverColor;
			_bgAlpha = bgAlpha;
			
			_slideColor = slideColor;
			_headColor = slideHeadColor;			_headHoverColor = slideHeadHoverColor;
			_slideAlpha = slideAlpha;
			
			
			//skin
			_skin = Library.createSprite(skin);
			addChild(_skin);
			
			
			//bg and slide's width and height
			_bg = Sprite(_skin.getChildByName("bg"));
			if (_bg != null) {
				_bg.width = _width;
				_bg.height = height;
				setColor(_bg, _bgColor);
				_bg.alpha = _bgAlpha;
			} else {
				_bg = new Sprite;
				_bg.graphics.beginFill(0xFFFFFF);
				_bg.graphics.drawRect(0, 0, _width, height);
				_bg.graphics.endFill();
				_bg.alpha = 0;
				addChild(_bg);
			}
			
			//bgHover
			_bgHover = Sprite(_bg.getChildByName("hover"));
			if (_bgHover != null) {
				setColor(_bgHover, _bgHoverColor);
				_bgHover.alpha = 0;
			}
			
			
			//slideInactive
			_slideInactive = Sprite(_skin.getChildByName("inactive"));
			if (_slideInactive != null) {
				_slideInactive.width = 0;
				_slideInactive.height = _slideInactive.height > height ? height : _slideInactive.height;
				_slideInactive.x = 0;
				_slideInactive.y = height / 2 - _slideInactive.height / 2;
				setColor(_slideInactive, _slideColor);
				_slideInactive.alpha = INACTIVE_ALPHA * _slideAlpha; 
			}
			
			
			//slideActive
			_slideActive = Sprite(_skin.getChildByName("active"));
			if (_slideActive != null) {
				_slideActive.width = 0;
				_slideActive.height = _slideActive.height > height ? height : _slideActive.height;
				_slideActive.x = 0;
				_slideActive.y = height / 2 - _slideActive.height / 2;
				setColor(_slideActive, _slideColor);
				_slideActive.alpha = _slideAlpha;
			}
			
			
			//head
			_head = Sprite(_skin.getChildByName("head"));
			if (_head != null) {
				_head.x = 0;
				_head.y = height / 2 - _head.height / 2;
				setColor(_head, _headColor);
				_head.alpha = _slideAlpha;
			}
			
			
			//headHover
			_headHover = Sprite(_head.getChildByName("hover"));
			if (_headHover != null) {
				setColor(_headHover, _headHoverColor);
				_headHover.alpha = 0;
			}
			
			addEventListener(MouseEvent.ROLL_OVER, rollOverListener);
			addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			
			addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener);
		}

		
		public function setWidth(width : Number) : void {
			var active : Number = activeLevel;			var inactive : Number = inactiveLevel;
			
			_width = width;
			_bg.width = _width;
			
			activeLevel = active;
			inactiveLevel = inactive;
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Getters & Setters                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//width and height
		public function get wdth() : uint {
			return _width * this.scaleX;
		}

		public function get hght() : uint {
			return _height * this.scaleY;
		}

		
		//active and inactive levels
		public function get inactiveLevel() : Number {
			return (_slideInactive.width - _head.width / 2 - _bg.x) / (_bg.width - _head.width);
		}

		public function set inactiveLevel(level : Number) : void {
			level *= _bg.width - _head.width;
			level += _head.width / 2;
			
			if (_slideInactive != null) {
				_slideInactive.width = level == _bg.width - _head.width / 2 ? _bg.width : level;
				_slideInactive.x = _bg.x;
			}
		}

		public function get activeLevel() : Number {
			return (_head.x - _bg.x) / (_bg.width - _head.width);
		}

		public function set activeLevel(level : Number) : void {
			level *= _bg.width - _head.width;
			level += _head.width / 2;
			
			if (_slideActive != null) {
				_slideActive.width = level;
				_slideActive.x = _bg.x;
			}
			
			_head.x = _bg.x + level - _head.width / 2;
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Protected Methods                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////

		//setting Color to slide elements child named "color"
		protected function setColor(sprt : Sprite, color : int) : void {
			if (color == -1) {
				return;
			}
			
			var colorField : Sprite = Sprite(sprt.getChildByName("color"));
			ColorUtils.setColor(colorField, color);
		}

		//hoer on
		private function hoverOn() : void {
			if (_bgHover != null) {
				_bgHover.alpha = 1;
			} else if (_bg != null) {
				setColor(_bg, _bgHoverColor);
			}
			
			
			if (_headHover != null) {
				_headHover.alpha = 1;
			} else if (_head != null) {
				setColor(_head, _headHoverColor);
			}
		}

		//hover off
		private function hoverOff() : void {
			if (_bgHover != null) {
				_bgHover.alpha = 0;
			} else if (_bg != null) {
				setColor(_bg, _bgColor);
			}
			
			
			if (_headHover != null) {
				_headHover.alpha = 0;
			} else if (_head != null) {
				setColor(_head, _headColor);
			}
		}

		//refreshing colors
		protected function refresh() : void {
			if (this.hitTestPoint(this.root.stage.mouseX, this.root.stage.mouseY) == true) {
				hoverOn();
			} else {
				hoverOff();
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//on roll over
		private function rollOverListener(e : MouseEvent) : void {
			hoverOn();
		}

		//on roll out
		private function rollOutListener(e : MouseEvent) : void {
			hoverOff();
		}

		//on mouse down
		private function mouseDownListener(e : MouseEvent) : void {
			removeEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			
			dispatchEvent(new Event(DRAG_STARTED, true));
			
			mouseMoveListener();
			
			this.root.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener);
			this.root.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
		}

		//on mouse up
		private function mouseUpListener(e : MouseEvent) : void {
			dispatchEvent(new Event(DRAG_ENDED, true));
			
			this.root.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
			this.root.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveListener);
			
			addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			
			refresh();			
		}

		//on mouse move
		private function mouseMoveListener(e : MouseEvent = null) : void {
			var mx : Number = _bg.mouseX * _bg.scaleX;
			if (mx <= (_head.width / 2)) {
				mx = 0;
			} else if (mx > _bg.width - _head.width / 2) {
				mx = 1;
			} else {
				mx = (mx - _head.width / 2) / (_bg.width - _head.width);
			}
			
			activeLevel = mx;
			
			dispatchEvent(new Event(ACTIVE_LEVEL_CHANGED, true));
		}
	}
}
