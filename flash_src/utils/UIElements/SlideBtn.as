package utils.UIElements {
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Strong;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author Family
	 */
	public class SlideBtn extends Btn {
		//events
		public static const LEVEL_CHANGED : String = "levelChanged";		public static const CUR_WIDTH_CHANGED : String = "curWidthChanged";

		//vars
		private var _slide : Slide;
		private var _slideMask : Sprite;
		private var _level : Number; //keeping last level to resume to it, when clicked on btn and level = 0

		private var _tween : Tween; // tween for open and close
		public function SlideBtn(btnWidth : uint, slideWidth : uint, height : uint, btnSkin : String, slideSkin : String, slideColor : int,
						bgColor : int, bgHoverColor : int, bgAlpha : Number,ctrlsMainColor : int, ctrlsMainHoverColor : int, ctrlsMainAlpha : Number,
						hintText : String = "", divOnLeft : Boolean = false, divOnRoght : Boolean = false, divOnUp : Boolean = false, divOnDown : Boolean = false) : void {
			
			super(btnWidth, height, btnSkin, bgColor, bgHoverColor, bgAlpha, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha, hintText, divOnLeft, divOnRoght, divOnUp, divOnDown);
			
			_slide = new Slide(slideWidth, height, slideSkin, -1, -1, 0, slideColor, ctrlsMainColor, ctrlsMainHoverColor, ctrlsMainAlpha);
			_slide.inactiveLevel = 1;
			_slide.x = width - _slide.wdth;
			
			_slideMask = new Sprite;
			_slideMask.graphics.beginFill(0xFFFFFF);
			_slideMask.graphics.drawRect(0, 0, slideWidth, height);
			_slideMask.graphics.endFill();
			_slideMask.x = btnWidth;
			addChild(_slideMask);
			
			_slide.mask = _slideMask;
			
			_tween = new Tween(_slide, "x", Strong.easeOut, btnWidth - _slide.wdth, btnWidth, 0.1, true);			_tween.stop();
			
			addChild(_slide);
			
			_slide.addEventListener(Slide.ACTIVE_LEVEL_CHANGED, levelChangedlistener);			_slide.addEventListener(Slide.DRAG_STARTED, dragStartedlistener);			_slide.addEventListener(Slide.DRAG_ENDED, dragEndedlistener);
			
			_tween.addEventListener(TweenEvent.MOTION_CHANGE, motionChangelistener);
		}
		
		
		//open and close
		private function open():void {
			_tween.func = Strong.easeIn;
			_tween.continueTo(_width, 0.1);
		}
		
		private function close():void {
			_tween.func = Strong.easeIn;
			_tween.continueTo(_width - _slide.wdth, 0.9);
		}
		
		
		//listeners
		//on roll over
		protected override function rollOverListener(e : MouseEvent) : void {
			super.rollOverListener(e);
			open();			
		}

		//on roll out
		protected override function rollOutListener(e : MouseEvent) : void {
			super.rollOutListener(e);
			close();			
		}

		//on click on btn
		protected override function clickListener(e : MouseEvent) : void {
			if (_bg.hitTestPoint(this.stage.mouseX, this.stage.mouseY) == false) {
				return;
			}
			
			if (_logo.currentFrame == 1) {
				level = _level;
				_level = 0;
			} else {
				_level = level;
				level = 0;
			}
			
			levelChangedlistener();
		}

		//on slide level changed
		private function levelChangedlistener(e : Event = null) : void {
			level = _slide.activeLevel;
			
			dispatchEvent(new Event(LEVEL_CHANGED, true));
		}

		//when starting to drag head
		private function dragStartedlistener(e : Event) : void {
			removeEventListener(MouseEvent.ROLL_OUT, rollOutListener);
		}

		//when drag ended
		private function dragEndedlistener(e : Event) : void {
			addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			
			if (hitTestPoint(this.root.stage.mouseX, this.root.stage.mouseY) == false) {
				rollOutListener(null);
			}
		}
		

		//setters and getters
		//current and full width
		public function get curWidth() : uint {
			return (_slide.wdth + _slide.x + 2) * this.scaleX;
		}

		public function get fullWidth() : uint {
			return (_width + _slide.wdth + 2) * this.scaleX;
		}

		
		//level
		public function get level() : Number {
			return _slide.activeLevel;
		}

		public function set level(level : Number) : void {
			_slide.activeLevel = level;
			_logo.gotoAndStop(_slide.activeLevel == 0 ? 1 : (2 + int((_logo.totalFrames - 1) * _slide.activeLevel)));
			refresh();
		}

		private function motionChangelistener(e : TweenEvent) : void {
			dispatchEvent(new Event(CUR_WIDTH_CHANGED));
		}
	}
}
