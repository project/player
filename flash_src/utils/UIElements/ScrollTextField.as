package utils.UIElements {
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;

	/**
	 * @author Family
	 */
	public class ScrollTextField extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Constants                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const ALIGN_LEFT : String = "alignLeft";		public static const ALIGN_CENTER : String = "alignCenter";		public static const ALIGN_RIGHT : String = "alignRight";		
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _txtFld : TextField; //textfield

		private var _width : Number; //width		private var _text : String; //text		private var _font : String; //font		private var _size : Object; //font size		private var _color : Object; //font color		private var _bold : Object; //bold		private var _italic : Object; //italic		private var _underline : Object; //underline		private var _selectable : Boolean; //selectable		private var _autoSize : String; //autosize		private var _align : String; //align

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		public function ScrollTextField(width : Number, text : String = "", font : String = "Times New Roman", size : Object = 12,
											color : Object = 0x000000, bold : Object = false, italic : Object = false, underline : Object = false,
											selectable : Boolean = false, autoSize : String = "left", align : String = ALIGN_CENTER) : void {
			
			_width = width;
			_text = text;
			_font = font;
			_size = size;
			_color = color;
			_bold = bold;
			_italic = italic;
			_underline = underline;
			_selectable = selectable;
			_autoSize = autoSize;
			_align = align;
			
			
			//text field
			_txtFld = new TextField();
			addChild(_txtFld);
			
			this.text = _text;
		}


		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//setting new width
		public function setWidth(width : Number) : void {
			_width = width;
			
			text = _text;
		}
		
		//setting text, if text is larger than width, slicing text and adding "..."
		public function set text(txt : String) : void {
			_text = txt;
			_txtFld.text = _text;
			
			var format : TextFormat = new TextFormat;
			format.font = _font;
			format.size = _size;
			format.color = _color;
			format.bold = _bold;
			format.italic = _italic;
			format.underline = _underline;
			
			_txtFld.setTextFormat(format);
			
			_txtFld.selectable = _selectable;
			_txtFld.autoSize = _autoSize;
			
			if (_txtFld.width <= _width) {
				_txtFld.x = _align == ALIGN_LEFT ? 0 : _align == ALIGN_CENTER ? _width / 2 - _txtFld.width / 2 : _width - _txtFld.width;
			} else {
				_txtFld.appendText("...");
				while((_txtFld.width > _width) && (_txtFld.length > 4)) {
					_txtFld.text = _txtFld.text.substr(0, _txtFld.text.length - 5) + "...";
					
					_txtFld.setTextFormat(format);
					_txtFld.selectable = _selectable;
					_txtFld.autoSize = _autoSize;
				}
				
				_txtFld.text = _txtFld.text.substr(0, _txtFld.text.length - 4) + "...";
				
				_txtFld.setTextFormat(format);
				_txtFld.selectable = _selectable;
				_txtFld.autoSize = _autoSize;
				
				_txtFld.x = 0;
			}
		}
		
		//setting new color
		public function setColor(color : Object) : void {
			_color = color;
			text = _text;
		}
	}
}
