package utils.UIElements {
	import utils.Library;

	import view.View;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author Family
	 */
	public class Btn extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		protected var _width : Number; //btn width
		protected var _height : Number; //btn height

		protected var _skin : Sprite; //btn skin

		protected var _bg : Sprite; //btn bg
		protected var _bgColor : int; //color overlay for btn bg's child called "color"
		protected var _bgHover : Sprite; //btn bg hover		protected var _bgHoverColor : int; //color overlay for btn bg hovers's child called "color"		protected var _bgAlpha : Number; //bg alpha

		protected var _logo : MovieClip; //logo of the btn		protected var _logoColor : int; //color overlay for logo's "color" child		protected var _logoHover : MovieClip; //logo's hover		protected var _logoHoverColor : int; //color for logo's hover child "color"		protected var _logoAlpha : Number; //logo alpha

		protected var _divLeft : Sprite; //left div		protected var _divRight : Sprite; //right div		protected var _divUp : Sprite; //up div		protected var _divDown : Sprite; //down div

		protected var _hintText : String; //hint text
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//initing skin, colors and alphas

		public function Btn(width : Number, height : Number, skin : String, bgColor : int, bgHoverColor : int, bgAlpha : Number,
							logoColor : int, logoHoverColor : int, logoAlpha : Number, hintText : String = "",
							divOnLeft : Boolean = false, divOnRight : Boolean = false, divOnUp : Boolean = false, divOnDown : Boolean = false) : void {
								
			this.mouseChildren = false;
			
			//initing width and height
			_width = width;
			_height = height;
			
			//initing colors and alphas				
			_bgColor = bgColor;
			_bgHoverColor = bgHoverColor;
			_bgAlpha = bgAlpha;
			
			_logoColor = logoColor;
			_logoHoverColor = logoHoverColor;
			_logoAlpha = logoAlpha;
			
			_hintText = hintText;
			
			
			//initing skin
			_skin = Library.createSprite(skin);
			addChild(_skin);
			
			
			//initing bg, if it doesn't exist, creating transparent one
			_bg = Sprite(_skin.getChildByName("bg"));
			if (_bg != null) {
				_bg.width = width;
				_bg.height = height;
				setColor(_bg, _bgColor);
				_bg.alpha = _bgAlpha;
			} else {
				_bg = new Sprite;
				_bg.graphics.beginFill(0xFFFFFF);
				_bg.graphics.drawRect(0, 0, width, height);
				_bg.graphics.endFill();
				_bg.alpha = 0;
				addChild(_bg);
			}
			
			
			//creating divs
			if (divOnLeft == true) {
				_divLeft = Library.createSprite("vDiv");
				_divLeft.height = _height;
				addChild(_divLeft);
			}
			
			if (divOnRight == true) {
				_divRight = Library.createSprite("vDiv");
				_divRight.height = _height;
				addChild(_divRight);
			}
			
			if (divOnUp == true) {
				_divUp = Library.createSprite("hDiv");
				_divUp.width = _width;
				addChild(_divUp);
			}
			
			if (divOnDown == true) {
				_divDown = Library.createSprite("hDiv");
				_divDown.width = _width;
				addChild(_divDown);
			}
			
			
			_bg.x = _divLeft == null ? 0 : _divLeft.width;			_bg.y = _divUp == null ? 0 : _divUp.height;
			
			if (_divLeft != null) {
				_divLeft.y = _bg.y;			}
			if (_divRight != null) {
				_divRight.x = _bg.x + _bg.width;
				_divRight.y = _bg.y;			}
			if (_divDown != null) {
				_divDown.x = _bg.x;				_divDown.y = _bg.y + _bg.height;
			}
			
			if (_divUp != null) {
				_divUp.x = _bg.x;
			}
			
			
			//bghover
			_bgHover = _bg != null ? Sprite(_bg.getChildByName("hover")) : null;
			if (_bgHover != null) {
				setColor(_bgHover, _bgHoverColor);
				_bgHover.alpha = 0;
			}
			
			
			//logo
			_logo = MovieClip(_skin.getChildByName("logo"));
			if (_logo != null) {
				_logo.gotoAndStop(1);
				_logo.x = _bg.x + _bg.width / 2 - _logo.width / 2;
				_logo.y = _bg.y + _bg.height / 2 - _logo.height / 2;
				setColor(_logo, _logoColor);
				_logo.alpha = _logoAlpha;
			}
			
			
			//bghover
			_logoHover = _logo != null ? MovieClip(_logo.getChildByName("hover")) : null;
			if (_logoHover != null) {
				setColor(_logoHover, _logoHoverColor);
				_logoHover.alpha = 0;
			}
			
			
			addEventListener(MouseEvent.ROLL_OVER, rollOverListener);			addEventListener(MouseEvent.ROLL_OUT, rollOutListener);
			addEventListener(MouseEvent.CLICK, clickListener);
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//set width and height
		public function setWidth(width : Number) : void {
			_width = width;
			
			if (_bg != null) {
				_bg.width = _width;
			}
			
			if (_divUp != null) {
				_divUp.width = _width;			}
			
			if (_divDown != null) {
				_divDown.width = _width;
			}
			
			if (_divRight != null) {
				_divRight.x = _bg.x + _bg.width;
			}
			
			if (_logo != null) {
				_logo.x = _bg.x + _bg.width / 2 - _logo.width / 2;			}
		}

		public function set hght(height : Number) : void {
			_height = height;
			
			if (_bg != null) {
				_bg.height = _height;
			}
			
			if (_divLeft != null) {
				_divLeft.height = _height;
			}
			
			if (_divRight != null) {
				_divRight.height = _height;
			}
			
			if (_divDown != null) {
				_divDown.y = _bg.y + _bg.height;
			}
			
			if (_logo != null) {
				_logo.y = _bg.y + _bg.height / 2 - _logo.height / 2;
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Getters & Setters                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//state
		public function get curLabel() : String {
			return _logo.currentLabel;
		}

		public function set curLabel(label : String) : void {
			hoverOff();
			_logo.gotoAndStop(label);
			refresh();
		}
		
		//hint
		public function get hintText() : String {
			return _hintText;
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		// Protected Methods                                                                  //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//setting Color to btn elements child named "color"
		protected function setColor(sprt : Sprite, color : int) : void {
			if (color == -1) {
				return;
			}
			
			var colorField : Sprite = Sprite(sprt.getChildByName("color"));
			ColorUtils.setColor(colorField, color);
		}

		
		//setting hover on
		protected function hoverOn() : void {
			if (_bgHover != null) {
				_bgHover.alpha = 1;
			} else if (_bg != null) {
				setColor(_bg, _bgHoverColor);
			}
			
			
			if (_logoHover != null) {
				_logoHover.alpha = 1;
			} else if (_logo != null) {
				setColor(_logo, _logoHoverColor);
			}
		}

		//setting hover off
		protected function hoverOff() : void {
			if (_bgHover != null) {
				_bgHover.alpha = 0;
			} else if (_bg != null) {
				setColor(_bg, _bgColor);
			}
			
			
			if (_logoHover != null) {
				_logoHover.alpha = 0;
			} else if (_logo != null) {
				setColor(_logo, _logoColor);
			}
		}

		//refreshing colors
		protected function refresh() : void {
			if (this.root == null) {
				hoverOff();
				return;
			}
			
			if (this.hitTestPoint(this.root.stage.mouseX, this.root.stage.mouseY) == true) {
				hoverOn();
			} else {
				hoverOff();
			}
		}

		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//on roll over
		protected function rollOverListener(e : MouseEvent) : void {
			hoverOn();
			
			dispatchEvent(new Event(View.SHOW_HINT, true));
		}

		//on roll out
		protected function rollOutListener(e : MouseEvent) : void {
			hoverOff();
			
			dispatchEvent(new Event(View.HIDE_HINT, true));
		}

		
		//clickListener
		protected function clickListener(e : MouseEvent) : void {
			var curLabel : String = _logo.currentLabel; //saveing current label , to dispatch it then
			
			//setting current logo to hover off
			hoverOff();
			
			//going to next logo
			_logo.gotoAndStop(_logo.currentFrame == _logo.totalFrames ? 1 : _logo.currentFrame + 1);
			refresh();
			
			
			//dispatching event, with type of current label
			if (curLabel != null) {
				dispatchEvent(new Event(curLabel, true));
			}
		}
	}
}
