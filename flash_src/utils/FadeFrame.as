package utils {
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.None;

	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * @author Family
	 */
	public class FadeFrame extends Sprite {
		////////////////////////////////////////////////////////////////////////////////////////
		// Events                                                                             //
		////////////////////////////////////////////////////////////////////////////////////////
		public static const FRAME_FADED_OUT : String = "frameClosed"; //dispatched, when frame faded out
		public static const FRAME_FADED_IN : String = "frameOpened"; //dispatched, when frame faded in
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Variables                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		private var _tween : Tween; //fade in and out tween
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Constructor                                                                        //
		////////////////////////////////////////////////////////////////////////////////////////
		public function FadeFrame():void {
			_tween = new Tween(this, "alpha", None.easeNone, 0, 1, 0.25, true);
			_tween.stop();
			_tween.addEventListener(TweenEvent.MOTION_FINISH, tweenFinishedListener);
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Public Methods                                                                     //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//fade in
		public function fadeIn():void {
			_tween.continueTo(1, 0.25);
		}
		
		//fade out
		public function fadeOut():void {
			_tween.continueTo(0, 0.25);
		}
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		// Listeners                                                                          //
		////////////////////////////////////////////////////////////////////////////////////////
		
		//when frame fade in or out 
		private function tweenFinishedListener(e:TweenEvent):void {
			if (_tween.finish == 0) {
				dispatchEvent(new Event(FRAME_FADED_OUT));
			} else if (_tween.finish == 1) {
				dispatchEvent(new Event(FRAME_FADED_IN));
			}
		}
	}
}
