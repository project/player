<?php
/**
 * @file
 * Spider Video Player module videos.
 */

/**
 * Menu loader callback. Load videos.
 */
function spider_video_player_videos() {
  global $user;
  $form = array();
  $free_version = '
    <a href="http://web-dorado.com/files/fromSVPDrupal.php" target="_blank" style="color:red; text-decoration:none; float:right;">
      <img src="' . base_path() . drupal_get_path('module', 'spider_video_player') . '/images/header.png" border="0" alt="www.web-dorado.com" width="215"><br />
    <div style="float:right;">' . t('Get the full version') . '&nbsp;&nbsp;&nbsp;&nbsp;</div>
    </a>';
  $form['fieldset_video_buttons'] = array(
    '#prefix' => $free_version,
    '#type' => 'fieldset',
  );
  $form['fieldset_video_buttons']['publish_video'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_video_player_videos_publish'),
    '#value' => t('Publish'),
  );
  $form['fieldset_video_buttons']['unpublish_video'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_video_player_videos_unpublish'),
    '#value' => t('Unpublish'),
  );
  $form['fieldset_video_buttons']['delete_video'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_video_player_videos_delete'),
    '#value' => t('Delete'),
    '#attributes' => array('onclick' => 'if (!confirm(Drupal.t("Do you want to delete selected videos?"))) {return false;}'),
  );
  $form['fieldset_video_buttons']['new_video'] = array(
    '#prefix' => l(t('New'), url('admin/settings/spider_video_player/videos/edit', array('absolute' => TRUE))),
  );
  // Search videos by name.
  $form['fieldset_search_videos_name'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search videos by name'),
    '#collapsible' => TRUE,
    '#collapsed' => ((variable_get('spider_video_player_search_videos_name', '') == '') ? TRUE : FALSE),
  );
  $form['fieldset_search_videos_name']['search_videos_name'] = array(
    '#type' => 'textfield',
    '#size' => 25,
    '#default_value' => variable_get('spider_video_player_search_videos_name', ''),
  );
  $form['fieldset_search_videos_name']['search_videos'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_video_player_search_videos_name'),
    '#value' => t('Go') . ' ',
  );
  $form['fieldset_search_videos_name']['reset_videos'] = array(
    '#type' => 'submit',
    '#value' => t('Reset') . ' ',
    '#submit' => array('spider_video_player_reset_videos_name'),
  );
  // Search videos by playlist.
  $form['fieldset_search_videos_by_playlist'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search videos by playlist'),
    '#collapsible' => TRUE,
    '#collapsed' => ((variable_get('spider_video_player_search_videos_by_playlist', '') == '') ? TRUE : FALSE),
  );
  $form['fieldset_search_videos_by_playlist']['search_videos_by_playlist_select'] = array(
    '#type' => 'select',
    '#options' => db_query("SELECT id,title FROM {spider_video_player_playlist} ORDER BY title")->fetchAllKeyed(),
    '#empty_option' => t('-Select Playlist-'),
    '#default_value' => variable_get('spider_video_player_search_videos_by_playlist', ''),
  );
  $form['fieldset_search_videos_by_playlist']['search_videos_by_playlist'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_video_player_search_videos_by_playlist'),
    '#value' => t('Go'),
  );
  $form['fieldset_search_videos_by_playlist']['reset_videos_by_playlist'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#submit' => array('spider_video_player_reset_videos_by_playlist'),
  );
  $header = array(
    'id' => array('data' => t('ID'), 'field' => 'n.id'),
    'title' => array('data' => t('Title'), 'field' => 'n.title'),
    'type' => array('data' => t('Type'), 'field' => 'n.type'),
    'url' => array('data' => t('Url'), 'field' => 'n.url'),
    'urlHD' => array('data' => t('UrlHD'), 'field' => 'n.urlHD'),
    'thumb' => array('data' => t('Thumb'), 'field' => 'n.thumb'),
    'published' => array('data' => t('Published'), 'field' => 'n.published'),
    'delete' => array('data' => t('Delete')),
  );
  $options = array();
  $query = db_select('spider_video_player_video', 'n')
    ->fields('n', array('id'))
    ->condition('n.title', '%' . db_like(variable_get('spider_video_player_search_videos_name', '')) . '%', 'LIKE')
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(20);
  if (variable_get('spider_video_player_search_videos_by_playlist', '') != '') {
    $videos = db_query("SELECT videos FROM {spider_video_player_playlist} WHERE id=:id", array(':id' => variable_get('spider_video_player_search_videos_by_playlist', '')))->fetchField();
    $videos_array = explode(',', $videos);
    $condition = db_or()->condition('n.id', 0, '=');
    foreach ($videos_array as $video) {
      if ($video != '') {
        $condition = $condition->condition('n.id', $video, '=');
      }
    }
    $query = $query->condition($condition);
  }
  // Allow users edit only own videos.
  if (($user->uid != 1) && (user_access('access Spider Video Player user videos administration'))) {
    $query = $query
      ->condition(
        db_or()
        ->condition('n.uid', $user->uid, '=')
        ->condition('n.uid', 1, '=')
      );
  }
  $videos_ids = $query->execute()->fetchCol();
  foreach ($videos_ids as $video_id) {
    $row = db_query("SELECT * FROM {spider_video_player_video} WHERE id=:id", array(':id' => $video_id))->fetchObject();
    if ($row->published) {
      $publish_unpublish_png = 'publish.png';
      $publish_unpublish_function = 'unpublish';
    }
    else {
      $publish_unpublish_png = 'unpublish.png';
      $publish_unpublish_function = 'publish';
    }
    $options[$video_id] = array(
      'id' => $video_id,
      'title' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $row->title,
          '#href' => url('admin/settings/spider_video_player/videos/edit', array('query' => array('video_id' => $video_id), 'absolute' => TRUE)),
        ),
      ),
      'type' => $row->type,
      'url' => (($row->url != '') ? $row->url : $row->urlHtml5),
      'urlHD' => (($row->urlHD != '') ? $row->urlHD : $row->urlHdHtml5),
      'thumb' => (($row->thumb != '') ? '<img width="50" title="' . $row->thumb . '" src="' . $row->thumb . '" />' : ''),
      'published' => l(t('<img src="' . base_path() . drupal_get_path('module', 'spider_video_player') . '/images/' . $publish_unpublish_png . '" />'), url('admin/settings/spider_video_player/videos/' . $publish_unpublish_function, array('query' => array('video_id' => $video_id), 'absolute' => TRUE)), array('html' => TRUE)),
      'delete' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('Delete'),
          '#href' => url('admin/settings/spider_video_player/videos/delete', array('query' => array('video_id' => $video_id), 'absolute' => TRUE)),
        ),
      ),
    );
  }
  $form['videos_table'] = array(
    '#type' => 'tableselect',
    '#js_select' => TRUE,
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No videos available.'),
    '#suffix' => theme('pager', array('tag' => array())),
  );
  return $form;
}

/**
 * Search videos by name.
 */
function spider_video_player_search_videos_name($form, &$form_state) {
  if ($form_state['values']['search_videos_name'] != '') {
    variable_set('spider_video_player_search_videos_name', $form_state['values']['search_videos_name']);
  }
  else {
    variable_set('spider_video_player_search_videos_name', '');
  }
}

/**
 * Reset videos.
 */
function spider_video_player_reset_videos_name($form, &$form_state) {
  variable_set('spider_video_player_search_videos_name', '');
}

/**
 * Search videos by playlist.
 */
function spider_video_player_search_videos_by_playlist($form, &$form_state) {
  if ($form_state['values']['search_videos_by_playlist_select'] != '') {
    variable_set('spider_video_player_search_videos_by_playlist', $form_state['values']['search_videos_by_playlist_select']);
  }
  else {
    variable_set('spider_video_player_search_videos_by_playlist', '');
  }
}

/**
 * Reset videos.
 */
function spider_video_player_reset_videos_by_playlist($form, &$form_state) {
  variable_set('spider_video_player_search_videos_by_playlist', '');
}

/**
 * Publish selected videos.
 */
function spider_video_player_videos_publish($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_video_player_video}")) {
    $video_ids_col = db_query("SELECT id FROM {spider_video_player_video}")->fetchCol();
    $flag = FALSE;
    foreach ($video_ids_col as $video_id) {
      if (isset($_POST['videos_table'][$video_id])) {
        $flag = TRUE;
        db_query("UPDATE {spider_video_player_video} SET published=:published WHERE id=:id", array(':published' => 1, ':id' => $video_id));
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one video.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Selected videos successfully published.'), 'status', FALSE);
    }
  }
}

/**
 * Publish video.
 */
function spider_video_player_video_publish() {
  if (isset($_GET['video_id'])) {
    $video_id = check_plain($_GET['video_id']);
    db_query("UPDATE {spider_video_player_video} SET published=:published WHERE id=:id", array(':published' => 1, ':id' => $video_id));
  }
  drupal_set_message(t('Video successfully published.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_video_player/videos', array('absolute' => TRUE)));
}

/**
 * Unpublish selected videos.
 */
function spider_video_player_videos_unpublish($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_video_player_video}")) {
    $video_ids_col = db_query("SELECT id FROM {spider_video_player_video}")->fetchCol();
    $flag = FALSE;
    foreach ($video_ids_col as $video_id) {
      if (isset($_POST['videos_table'][$video_id])) {
        $flag = TRUE;
        db_query("UPDATE {spider_video_player_video} SET published=:published WHERE id=:id", array(':published' => 0, ':id' => $video_id));
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one video.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Selected videos successfully unpublished.'), 'status', FALSE);
    }
  }
}

/**
 * Unpublish video.
 */
function spider_video_player_video_unpublish() {
  if (isset($_GET['video_id'])) {
    $video_id = check_plain($_GET['video_id']);
    db_query("UPDATE {spider_video_player_video} SET published=:published WHERE id=:id", array(':published' => 0, ':id' => $video_id));
  }
  drupal_set_message(t('Video successfully unpublished.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_video_player/videos', array('absolute' => TRUE)));
}

/**
 * Delete selected videos.
 */
function spider_video_player_videos_delete($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_video_player_video}")) {
    $video_ids_col = db_query("SELECT id FROM {spider_video_player_video}")->fetchCol();
    $flag = FALSE;
    foreach ($video_ids_col as $video_id) {
      if (isset($_POST['videos_table'][$video_id])) {
        $flag = TRUE;
        db_query("DELETE FROM {spider_video_player_video} WHERE id=:id", array(':id' => $video_id));
        $ids_col = db_query("SELECT id,videos FROM {spider_video_player_playlist}")->fetchAllKeyed();
        foreach ($ids_col as $key => $videos) {
          if (strpos($videos, $video_id . ',')) {
            db_query("UPDATE {spider_video_player_playlist} SET videos=:videos WHERE id=:id", array(':videos' => str_replace($video_id . ',', '', $videos), ':id' => $key));
          }
        }
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one video.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Selected videos successfully deleted.'), 'status', FALSE);
    }
  }
}

/**
 * Delete video.
 */
function spider_video_player_video_delete() {
  if (isset($_GET['video_id'])) {
    $video_id = check_plain($_GET['video_id']);
    db_query("DELETE FROM {spider_video_player_video} WHERE id=:id", array(':id' => $video_id));
    $ids_col = db_query("SELECT id,videos FROM {spider_video_player_playlist}")->fetchAllKeyed();
    foreach ($ids_col as $key => $videos) {
      if (strpos($videos, $video_id . ',')) {
        db_query("UPDATE {spider_video_player_playlist} SET videos=:videos WHERE id=:id", array(':videos' => str_replace($video_id . ',', '', $videos), ':id' => $key));
      }
    }
  }
  drupal_set_message(t('Video successfully deleted.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_video_player/videos', array('absolute' => TRUE)));
}

/**
 * Add or edit video.
 */
function spider_video_player_video_edit() {
  drupal_add_js(drupal_get_path('module', 'spider_video_player') . '/js/spider_video_player_change_video_type.js');
  drupal_add_js(drupal_get_path('module', 'spider_video_player') . '/js/spider_video_player_check_required.js');
  if (isset($_GET['video_id'])) {
    $video_id = check_plain($_GET['video_id']);
    $row = db_query("SELECT * FROM {spider_video_player_video} WHERE id=:id", array(':id' => $video_id))->fetchObject();
    $video_title = $row->title;
    $video_type = $row->type;
    $video_thumb = $row->thumb;
    $video_params = explode('#***#', $row->params);
    $video_published = $row->published;
  }
  else {
    $video_id = '';
    $video_title = '';
    $video_type = 'http';
    $video_http_url = '';
    $video_http_url_html5 = '';
    $video_http_url_hd = '';
    $video_http_url_hd_html5 = '';
    $video_youtube_url = '';
    $video_fms_url = '';
    $video_rtmp_url = '';
    $video_rtmp_url_hd = '';
    $video_thumb = '';
    $video_params = 'new';
    $video_published = 1;
  }
  switch ($video_type) {
    case 'http':
      drupal_add_js('spider_video_player_change_video_type("edit-video-type-http");', array('type' => 'inline', 'scope' => 'footer'));
      if (isset($_GET['video_id'])) {
        $video_http_url = $row->url;
        $video_http_url_html5 = $row->urlHtml5;
        $video_http_url_hd = $row->urlHD;
        $video_http_url_hd_html5 = $row->urlHdHtml5;
        $video_youtube_url = '';
        $video_fms_url = '';
        $video_rtmp_url = '';
        $video_rtmp_url_hd = '';
      }
      break;

    case 'youtube':
      drupal_add_js('spider_video_player_change_video_type("edit-video-type-youtube");', array('type' => 'inline', 'scope' => 'footer'));
      if (isset($_GET['video_id'])) {
        $video_http_url = '';
        $video_http_url_html5 = '';
        $video_http_url_hd = '';
        $video_http_url_hd_html5 = '';
        $video_youtube_url = $row->url;
        $video_fms_url = '';
        $video_rtmp_url = '';
        $video_rtmp_url_hd = '';
      }
      break;

    case 'rtmp':
      drupal_add_js('spider_video_player_change_video_type("edit-video-type-rtmp");', array('type' => 'inline', 'scope' => 'footer'));
      if (isset($_GET['video_id'])) {
        $video_http_url = '';
        $video_http_url_html5 = '';
        $video_http_url_hd = '';
        $video_http_url_hd_html5 = '';
        $video_youtube_url = '';
        $video_fms_url = $row->fmsUrl;
        $video_rtmp_url = $row->url;
        $video_rtmp_url_hd = $row->urlHD;
      }
      break;
  }
  $form = array();
  $form['video_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $video_title,
    '#size' => 35,
  );
  $form['video_type'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#default_value' => $video_type,
    '#attributes' => array('onmouseup' => 'spider_video_player_change_video_type(this.id);'),
    '#options' => array(
      'http' => t('http'),
      'youtube' => t('youtube'),
      'rtmp' => t('rtmp'),
    ),
    '#suffix' => '
      <div id="video_iframe" style="position:fixed; width:100%; height:100%; top:50px; left:0; display:none">
        <div id="div_black" style="position:fixed; top:0; left:0; width:100%; height:100%; background-color:#000000; opacity:0.80; z-index:10000000;"></div>
        <div id="div_content" style="position:relative; top:50px; width:570px; height:500px; margin:0 auto;z-index:10000001;background-color:#FFFFFF;">
          <iframe width="570" height="500" src="' . url('admin/settings/spider_video_player/video_upload', array('absolute' => TRUE)) . '"></iframe>
        </div>
        <div onclick=\'spider_video_player_file_upload("none")\' style=\'position:fixed; z-index:10000002; right:20%; top:100px; width:32px; height:32px; background-image:url("' . base_path() . drupal_get_path('module', 'spider_video_player') . '/images/close.png"); \'></div>
      </div>',
  );
  $form['video_type_hidden'] = array(
    '#type' => 'hidden',
    '#attributes' => array('id' => 'video_type_hidden'),
  );
  $form['video_http_url'] = array(
    '#prefix' => '
      <div id="video_http">
      <br />
      <strong>' . t('Url') . '</strong>
      <br />',
    '#type' => 'link',
    '#href' => '',
    '#title' => (($video_http_url != '') ? $video_http_url : t('Select Video')),
    '#attributes' => array('id' => 'url_http_link', 'onclick' => 'spider_video_player_set_video_type("url_http");spider_video_player_file_upload("");return false;'),
  );
  $form['url_http'] = array(
    '#type' => 'hidden',
    '#value' => $video_http_url,
    '#attributes' => array('id' => 'url_http'),
  );
  $form['video_http_url_remove'] = array(
    '#prefix' => '<br />',
    '#type' => 'link',
    '#href' => '',
    '#title' => t('Remove Video'),
    '#attributes' => array('onclick' => 'spider_video_player_remove_video("url_http"); return false;'),
  );
  $form['video_http_urlHtml5'] = array(
    '#prefix' => '
      <br />
      <strong>' . t('Url(HTML5)') . '</strong>
      <br />',
    '#type' => 'link',
    '#href' => '',
    '#title' => (($video_http_url_html5 != '') ? $video_http_url_html5 : t('Select Video')),
    '#attributes' => array('id' => 'urlHtml5_link', 'onclick' => 'spider_video_player_set_video_type("urlHtml5");spider_video_player_file_upload("");return false;'),
    '#description' => t('Allowed file extensions - MP4, WebM, Ogg'),
  );
  $form['urlHtml5'] = array(
    '#type' => 'hidden',
    '#value' => $video_http_url_html5,
    '#attributes' => array('id' => 'urlHtml5'),
  );
  $form['video_http_urlHtml5_remove'] = array(
    '#prefix' => '<br />',
    '#type' => 'link',
    '#href' => '',
    '#title' => t('Remove Video'),
    '#attributes' => array('onclick' => 'spider_video_player_remove_video("urlHtml5"); return false;'),
  );
  $form['video_http_urlHD'] = array(
    '#prefix' => '
      <br />
      <strong>' . t('UrlHD') . '</strong>
      <br />',
    '#type' => 'link',
    '#href' => '',
    '#title' => (($video_http_url_hd != '') ? $video_http_url_hd : t('Select Video')),
    '#attributes' => array('id' => 'urlHD_link', 'onclick' => 'spider_video_player_set_video_type("urlHD");spider_video_player_file_upload("");return false;'),
  );
  $form['urlHD'] = array(
    '#type' => 'hidden',
    '#value' => $video_http_url_hd,
    '#attributes' => array('id' => 'urlHD'),
  );
  $form['video_http_urlHD_remove'] = array(
    '#prefix' => '<br />',
    '#type' => 'link',
    '#href' => '',
    '#title' => t('Remove Video'),
    '#attributes' => array('onclick' => 'spider_video_player_remove_video("urlHD"); return false;'),
  );
  $form['video_http_urlHDHtml5'] = array(
    '#prefix' => '
      <br />
      <strong>' . t('UrlHD(HTML5)') . '</strong>
      <br />',
    '#type' => 'link',
    '#href' => '',
    '#title' => (($video_http_url_hd_html5 != '') ? $video_http_url_hd_html5 : t('Select Video')),
    '#attributes' => array('id' => 'urlHdHtml5_link', 'onclick' => 'spider_video_player_set_video_type("urlHdHtml5");spider_video_player_file_upload("");return false;'),
    '#description' => t('Allowed file extensions - MP4, WebM, Ogg'),
  );
  $form['urlHdHtml5'] = array(
    '#type' => 'hidden',
    '#value' => $video_http_url_hd_html5,
    '#attributes' => array('id' => 'urlHdHtml5'),
  );
  $form['video_http_urlHDHtml5_remove'] = array(
    '#prefix' => '<br />',
    '#type' => 'link',
    '#href' => '',
    '#title' => t('Remove Video'),
    '#attributes' => array('onclick' => 'spider_video_player_remove_video("urlHdHtml5"); return false;'),
    '#suffix' => '</div>',
  );
  $form['video_youtube_url'] = array(
    '#prefix' => '<div id="video_youtube">',
    '#type' => 'textfield',
    '#title' => t('Url'),
    '#default_value' => $video_youtube_url,
    '#size' => 35,
    '#suffix' => '</div>',
  );
  $form['video_fmsUrl'] = array(
    '#prefix' => '<div id="video_rtmp">',
    '#type' => 'textfield',
    '#title' => t('fmsUrl'),
    '#default_value' => $video_fms_url,
    '#size' => 35,
  );
  $form['video_rtmp_Url'] = array(
    '#type' => 'textfield',
    '#title' => t('Url'),
    '#default_value' => $video_rtmp_url,
    '#size' => 35,
  );
  $form['video_rtmp_urlHD'] = array(
    '#type' => 'textfield',
    '#title' => t('UrlHD'),
    '#default_value' => $video_rtmp_url_hd,
    '#size' => 35,
    '#suffix' => '</div>',
  );
  $form['video_radio_type_hidden'] = array(
    '#type' => 'hidden',
    '#value' => $video_type,
    '#attributes' => array('id' => 'video_radio_type_hidden'),
  );
  $form['video_thumb'] = array(
    '#prefix' => '
      <div id="image_iframe" style="position:fixed; width:100%; height:100%; top:50px; left:0; display:none">
        <div id="div_black" style="position:fixed; top:0; left:0; width:100%; height:100%; background-color:#000000; opacity:0.80; z-index:10000000;"></div>
        <div id="div_content" style="position:relative; top:50px; width:570px; height:500px; margin:0 auto;z-index:10000001;background-color:#FFFFFF;">
          <iframe width="570" height="500" src="' . url('admin/settings/spider_video_player/image_upload', array('absolute' => TRUE)) . '"></iframe>
        </div>
        <div onclick=\'spider_video_player_image_upload("none")\' style=\'position:fixed; z-index:10000002; right:20%; top:100px; width:32px; height:32px; background-image:url("' . base_path() . drupal_get_path('module', 'spider_video_player') . '/images/close.png"); \'></div>
      </div>
      <strong>' . t('Thumb') . '</strong>
      <br />',
    '#type' => 'link',
    '#href' => '',
    '#title' => t('Select Image'),
    '#attributes' => array('onclick' => 'spider_video_player_image_upload("");return false;'),
  );
  $form['thumb'] = array(
    '#type' => 'hidden',
    '#value' => $video_thumb,
    '#attributes' => array('id' => 'thumb'),
  );
  $form['video_thumb_remove'] = array(
    '#prefix' => '<br />',
    '#type' => 'link',
    '#href' => '',
    '#title' => t('Remove Image'),
    '#attributes' => array('onclick' => 'spider_video_player_remove_image(); return false;'),
    '#suffix' => '<br /><img id="imagebox" ' . (($video_thumb != '') ? 'height="150" src="' . $video_thumb . '"' : '') . '/><br />',
  );
  $required = '';
  if ($video_params == 'new') {
    $tag_ids = db_query("SELECT id FROM {spider_video_player_tag} WHERE published=:published ORDER BY ordering", array(':published' => 1))->fetchCol();
    foreach ($tag_ids as $tag_id) {
      $row = db_query("SELECT * FROM {spider_video_player_tag} WHERE id=:id", array(':id' => $tag_id))->fetchObject();
      $form['video_param_' . $tag_id] = array(
        '#type' => 'textfield',
        '#title' => $row->name,
        '#required' => (($row->required == 1) ? TRUE : FALSE),
        '#size' => 35,
      );
      if ($row->required == 1) {
        $required .= $tag_id . ',';
      }
    }
  }
  else {
    foreach ($video_params as $video_param) {
      if ($video_param != '') {
        $video_tag = explode('#===#', $video_param);
        if ($video_tag[0] != '') {
          $row = db_query("SELECT * FROM {spider_video_player_tag} WHERE published=:published AND id=:id", array(':published' => 1, ':id' => $video_tag[0]))->fetchObject();
          if ($row) {
            $form['video_param_' . $video_tag[0]] = array(
              '#type' => 'textfield',
              '#title' => $row->name,
              '#default_value' => $video_tag[1],
              '#required' => (($row->required == 1) ? TRUE : FALSE),
              '#size' => 35,
            );
            if ($row->required == 1) {
              $required .= $video_tag[0] . ',';
            }
          }
        }
      }
    }
  }
  $form['required_hidden'] = array(
    '#type' => 'hidden',
    '#value' => $required,
    '#attributes' => array('id' => 'required_hidden'),
  );
  $form['video_published'] = array(
    '#type' => 'radios',
    '#title' => t('Published'),
    '#default_value' => $video_published,
    '#options' => array('1' => t('Yes'), '0' => t('No')),
  );
  $form['video_save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('spider_video_player_video_save'),
    '#attributes' => array('onclick' => 'return spider_video_player_check_required("edit-video-title", "edit-video-param-", "required_hidden");'),
  );
  $form['video_apply'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#submit' => array('spider_video_player_video_apply'),
    '#attributes' => array('onclick' => 'return spider_video_player_check_required("edit-video-title", "edit-video-param-", "required_hidden");'),
  );
  $form['video_cancel'] = array(
    '#type' => 'link',
    '#href' => url('admin/settings/spider_video_player/videos/cancel', array('absolute' => TRUE)),
    '#title' => t('Cancel'),
    '#attributes' => array('class' => array('button')),
  );
  return $form;
}

/**
 * Save video.
 */
function spider_video_player_video_save($form, &$form_state) {
  global $user;
  $video_title = $form_state['values']['video_title'];
  $video_type = check_plain($_POST['video_radio_type_hidden']);
  switch ($video_type) {
    case 'http':
      $video_url = check_plain($_POST['url_http']);
      $video_url_hd = check_plain($_POST['urlHD']);
      $video_url_html5 = check_plain($_POST['urlHtml5']);
      $video_url_hd_html5 = check_plain($_POST['urlHdHtml5']);
      $video_fms_url = '';
      break;

    case 'youtube':
      $video_url = check_plain($_POST['video_youtube_url']);
      $video_url_hd = '';
      $video_url_html5 = '';
      $video_url_hd_html5 = '';
      $video_fms_url = '';
      break;

    case 'rtmp':
      $video_url = check_plain($_POST['video_rtmp_Url']);
      $video_url_hd = check_plain($_POST['video_rtmp_urlHD']);
      $video_url_html5 = '';
      $video_url_hd_html5 = '';
      $video_fms_url = check_plain($_POST['video_fmsUrl']);
      break;

    default:
      $video_url = '';
      $video_url_hd = '';
      $video_url_html5 = '';
      $video_url_hd_html5 = '';
      $video_fms_url = '';
      break;
  }
  $video_thumb = check_plain($_POST['thumb']);
  $tag_ids = db_query("SELECT id FROM {spider_video_player_tag} WHERE published=:published ORDER BY ordering", array(':published' => 1))->fetchCol();
  $video_params = '';
  foreach ($tag_ids as $tag_id) {
    $video_params .= $tag_id . '#===#' . $form_state['values']['video_param_' . $tag_id] . '#***#';
  }
  $video_published = $form_state['values']['video_published'];
  if (isset($_GET['video_id'])) {
    $video_id = check_plain($_GET['video_id']);
    db_query("UPDATE {spider_video_player_video} SET 
      uid=:uid,
      title=:title,
      type=:type,
      urlHtml5=:urlHtml5,
      urlHdHtml5=:urlHdHtml5,
      fmsUrl=:fmsUrl,
      url=:url,
      urlHD=:urlHD,
      params=:params,
      thumb=:thumb,
      published=:published WHERE id=:id", array(
      ':uid' => $user->uid,
      ':title' => $video_title,
      ':type' => $video_type,
      ':urlHtml5' => $video_url_html5,
      ':urlHdHtml5' => $video_url_hd_html5,
      ':fmsUrl' => $video_fms_url,
      ':url' => $video_url,
      ':urlHD' => $video_url_hd,
      ':params' => $video_params,
      ':thumb' => $video_thumb,
      ':published' => $video_published,
      ':id' => $video_id));
  }
  else {
    db_insert('spider_video_player_video')
      ->fields(array(
        'uid' => $user->uid,
        'title' => $video_title,
        'type' => $video_type,
        'urlHtml5' => $video_url_html5,
        'urlHdHtml5' => $video_url_hd_html5,
        'fmsUrl' => $video_fms_url,
        'url' => $video_url,
        'urlHD' => $video_url_hd,
        'params' => $video_params,
        'thumb' => $video_thumb,
        'published' => $video_published,
        ))
      ->execute();
  }
  drupal_set_message(t('Your video successfully saved.'), 'status', FALSE);
  $form_state['redirect'] = url('admin/settings/spider_video_player/videos', array('absolute' => TRUE));
}

/**
 * Apply video.
 */
function spider_video_player_video_apply($form, &$form_state) {
  global $user;
  $video_title = $form_state['values']['video_title'];
  $video_type = check_plain($_POST['video_radio_type_hidden']);
  switch ($video_type) {
    case 'http':
      $video_url = check_plain($_POST['url_http']);
      $video_url_hd = check_plain($_POST['urlHD']);
      $video_url_html5 = check_plain($_POST['urlHtml5']);
      $video_url_hd_html5 = check_plain($_POST['urlHdHtml5']);
      $video_fms_url = '';
      break;

    case 'youtube':
      $video_url = check_plain($_POST['video_youtube_url']);
      $video_url_hd = '';
      $video_url_html5 = '';
      $video_url_hd_html5 = '';
      $video_fms_url = '';
      break;

    case 'rtmp':
      $video_url = check_plain($_POST['video_rtmp_Url']);
      $video_url_hd = check_plain($_POST['video_rtmp_urlHD']);
      $video_url_html5 = '';
      $video_url_hd_html5 = '';
      $video_fms_url = check_plain($_POST['video_fmsUrl']);
      break;

    default:
      $video_url = '';
      $video_url_hd = '';
      $video_url_html5 = '';
      $video_url_hd_html5 = '';
      $video_fms_url = '';
      break;
  }
  $video_thumb = check_plain($_POST['thumb']);
  $tag_ids = db_query("SELECT id FROM {spider_video_player_tag} WHERE published=:published ORDER BY ordering", array(':published' => 1))->fetchCol();
  $video_params = '';
  foreach ($tag_ids as $tag_id) {
    $video_params .= $tag_id . '#===#' . $form_state['values']['video_param_' . $tag_id] . '#***#';
  }
  $video_published = $form_state['values']['video_published'];
  if (isset($_GET['video_id'])) {
    $video_id = check_plain($_GET['video_id']);
    db_query("UPDATE {spider_video_player_video} SET 
      uid=:uid,
      title=:title,
      type=:type,
      urlHtml5=:urlHtml5,
      urlHdHtml5=:urlHdHtml5,
      fmsUrl=:fmsUrl,
      url=:url,
      urlHD=:urlHD,
      params=:params,
      thumb=:thumb,
      published=:published WHERE id=:id", array(
      ':uid' => $user->uid,
      ':title' => $video_title,
      ':type' => $video_type,
      ':urlHtml5' => $video_url_html5,
      ':urlHdHtml5' => $video_url_hd_html5,
      ':fmsUrl' => $video_fms_url,
      ':url' => $video_url,
      ':urlHD' => $video_url_hd,
      ':params' => $video_params,
      ':thumb' => $video_thumb,
      ':published' => $video_published,
      ':id' => $video_id));
    drupal_set_message(t('Your video successfully updated.'), 'status', FALSE);
  }
  else {
    db_insert('spider_video_player_video')
      ->fields(array(
        'uid' => $user->uid,
        'title' => $video_title,
        'type' => $video_type,
        'urlHtml5' => $video_url_html5,
        'urlHdHtml5' => $video_url_hd_html5,
        'fmsUrl' => $video_fms_url,
        'url' => $video_url,
        'urlHD' => $video_url_hd,
        'params' => $video_params,
        'thumb' => $video_thumb,
        'published' => $video_published,
        ))
      ->execute();
    $video_id = db_query("SELECT MAX(id) FROM {spider_video_player_video}")->fetchField();
    drupal_set_message(t('Your video successfully saved.'), 'status', FALSE);
  }
  $form_state['redirect'] = url('admin/settings/spider_video_player/videos/edit', array('query' => array('video_id' => $video_id), 'absolute' => TRUE));
}

/**
 * Cancel video save.
 */
function spider_video_player_video_cancel() {
  drupal_goto(url('admin/settings/spider_video_player/videos', array('absolute' => TRUE)));
}
