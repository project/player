<?php
/**
 * @file
 * Flash Video Player view.
 */

/**
 * Flash Video Player view.
 */
function spider_video_player_flash_view($playlist, $theme, $nodeid) {
  if (isset($_GET['playlist'])) {
    $playlist = check_plain($_GET['playlist']);
  }
  if (isset($_GET['theme'])) {
    $theme = check_plain($_GET['theme']);
    // If shared from embed.
    $share_url = url('spider_video_player/flash', array('query' => array('theme' => $theme, 'playlist' => $playlist), 'absolute' => TRUE));
    $nodeid = 0;
  }
  else {
    $share_url = url(current_path(), array('query' => array('share' => '1'), 'absolute' => TRUE));
  }
  $theme_row = db_query("SELECT * FROM {spider_video_player_theme} WHERE id=:id", array(':id' => $theme))->fetchObject();
  if ($theme_row->appWidth != '') {
    $width = $theme_row->appWidth;
  }
  else {
    $width = '640';
  }
  if ($theme_row->appHeight != '') {
    $height = $theme_row->appHeight;
  }
  else {
    $height = '480';
  }
  if ($theme_row->show_trackid != '') {
    $show_trackid = $theme_row->show_trackid;
  }
  else {
    $show_trackid = 1;
  }
  if (isset($_GET['defaultAlbumId'])) {
    $album_id = check_plain($_GET['defaultAlbumId']);
  }
  else {
    $album_id = '';
  }
  if (isset($_GET['defaultTrackId'])) {
    $track_id = check_plain($_GET['defaultTrackId']);
  }
  else {
    $track_id = '';
  }
  $content =  '
    <div id="spider_video_player_flash_' . $nodeid . '">
    <script type="text/javascript" src="' . base_path() . drupal_get_path('module', 'spider_video_player') . '/js/spider_video_player_swfobject.js"></script>
    <div id="flashcontent' . $nodeid . '" style="width:' . $width . 'px; height:' . $height . 'px"></div>
    <script>
      function flashShare(type, b, c) {
        var t = document.title;
        switch (type) {
          case "fb":
            window.open("http://www.facebook.com/sharer.php?u=" + encodeURIComponent("' . $share_url . '&defaultAlbumId=" + b + "&defaultTrackId=" + c) + "&t=" + encodeURIComponent(t), "Facebook","menubar=1,resizable=1,width=350,height=250");
            break;
          case "g":
            window.open("http://plus.google.com/share?url=" + encodeURIComponent("' . $share_url . '&defaultAlbumId=" + b + "&defaultTrackId=" + c) + "&t=" + encodeURIComponent(t), "Google","menubar=1,resizable=1,width=350,height=250");
            break;
          case "tw":
            window.open("http://twitter.com/home/?status=" + encodeURIComponent("' . $share_url . '&defaultAlbumId=" + b + "&defaultTrackId=" + c), "Twitter","menubar=1,resizable=1,width=350,height=250");
            break;
        }
      }
      var so = new SWFObject("' . base_path() . drupal_get_path('module', 'spider_video_player') . '/videoPlayer.swf?wdrand=' . mt_rand() . '", "Player", "100%", "100%", "8", "#000000");
      so.addParam("FlashVars", "settingsUrl=' . str_replace("&", "@", str_replace("&amp;", "@", url('spider_video_player/settings_xml', array('query' => array('theme' => $theme, 'playlist' => $playlist), 'absolute' => TRUE)))) . '&playlistUrl=' . str_replace("&", "@", str_replace("&amp;", "@", url('spider_video_player/playlist_xml', array('query' => array('theme' => $theme, 'playlist' => $playlist), 'absolute' => TRUE)))) . '&defaultAlbumId=' . $album_id . '&defaultTrackId=' . $track_id . '");
      so.addParam("quality", "high");
      so.addParam("menu", "false");
      so.addParam("wmode", "transparent");
      so.addParam("loop", "false");
      so.addParam("allowfullscreen", "true");
      so.write("flashcontent' . $nodeid . '");
    </script>
    </div>';
  if (isset($_GET['playlist']) && isset($_GET['theme'])) {
    echo $content;
  }
  else {
    return $content;
  }
}

/**
 * Video player parameters.
 */
function spider_video_player_settings() {
  if (isset($_GET['theme'])) {
    $theme_id = check_plain($_GET['theme']);
  }
  else {
    $theme_id = 1;
  }
  if (isset($_GET['playlist'])) {
    $playlist_ids = check_plain($_GET['playlist']);
  }
  else {
    $playlist_ids = '';
  }
  $theme_row = db_query("SELECT * FROM {spider_video_player_theme} WHERE id=:id", array(':id' => $theme_id))->fetchObject();
  $ctrls = explode(',', $theme_row->ctrlsStack);
  $new = '';
  foreach ($ctrls as $key => $x) {
    $y = explode(":", $x);
    $ctrl = $y[0];
    $active = $y[1];
    if ($active == 1) {
      if ($new == '') {
        $new = $y[0];
      }
      else {
        $new = $new . ',' . $y[0];
      }
    }
  }
  echo '
    <settings>
      <appWidth>' . $theme_row->appWidth . '</appWidth>
      <appHeight>' . $theme_row->appHeight . '</appHeight>
      <playlistWidth>' . $theme_row->playlistWidth . '</playlistWidth>
      <startWithLib>' . spider_video_player_to_str($theme_row->startWithLib) . '</startWithLib>
      <autoPlay>' . spider_video_player_to_str($theme_row->autoPlay) . '</autoPlay>
      <autoNext>' . spider_video_player_to_str($theme_row->autoNext) . '</autoNext>
      <autoNextAlbum>' . spider_video_player_to_str($theme_row->autoNextAlbum) . '</autoNextAlbum>
      <defaultVol>' . (($theme_row->defaultVol + 0) / 100) . '</defaultVol>
      <defaultRepeat>' . $theme_row->defaultRepeat . '</defaultRepeat>
      <defaultShuffle>' . $theme_row->defaultShuffle . '</defaultShuffle>
      <autohideTime>' . $theme_row->autohideTime . '</autohideTime>
      <centerBtnAlpha>' . (($theme_row->centerBtnAlpha + 0) / 100) . '</centerBtnAlpha>
      <loadinAnimType>' . $theme_row->loadinAnimType . '</loadinAnimType>
      <keepAspectRatio>' . spider_video_player_to_str($theme_row->keepAspectRatio) . '</keepAspectRatio>
      <clickOnVid>' . spider_video_player_to_str($theme_row->clickOnVid) . '</clickOnVid>
      <spaceOnVid>' . spider_video_player_to_str($theme_row->spaceOnVid) . '</spaceOnVid>
      <mouseWheel>' . spider_video_player_to_str($theme_row->mouseWheel) . '</mouseWheel>
      <ctrlsPos>' . $theme_row->ctrlsPos . '</ctrlsPos>
      <ctrlsStack>' . $new . '</ctrlsStack>
      <ctrlsOverVid>' . spider_video_player_to_str($theme_row->ctrlsOverVid) . '</ctrlsOverVid>
      <ctrlsAutoHide>' . spider_video_player_to_str($theme_row->ctrlsSlideOut) . '</ctrlsAutoHide>
      <watermarkUrl>' . $theme_row->watermarkUrl . '</watermarkUrl>
      <watermarkPos>' . $theme_row->watermarkPos . '</watermarkPos>
      <watermarkSize>' . $theme_row->watermarkSize . '</watermarkSize>
      <watermarkSpacing>' . $theme_row->watermarkSpacing . '</watermarkSpacing>
      <watermarkAlpha>' . (($theme_row->watermarkAlpha + 0) / 100) . '</watermarkAlpha>
      <playlistPos>' . $theme_row->playlistPos . '</playlistPos>
      <playlistOverVid>' . spider_video_player_to_str($theme_row->playlistOverVid) . '</playlistOverVid>
      <playlistAutoHide>' . spider_video_player_to_str($theme_row->playlistAutoHide) . '</playlistAutoHide>
      <playlistTextSize>' . $theme_row->playlistTextSize . '</playlistTextSize>
      <libCols>' . $theme_row->libCols . '</libCols>
      <libRows>' . $theme_row->libRows . '</libRows>
      <libListTextSize>' . $theme_row->libListTextSize . '</libListTextSize>
      <libDetailsTextSize>' . $theme_row->libDetailsTextSize . '</libDetailsTextSize>
      <playBtnHint>' . t('play') . '</playBtnHint>
      <pauseBtnHint>' . t('pause') . '</pauseBtnHint>
      <playPauseBtnHint>' . t('toggle pause') . '</playPauseBtnHint>
      <stopBtnHint>' . t('stop') . '</stopBtnHint>
      <playPrevBtnHint>' . t('play previous') . '</playPrevBtnHint>
      <playNextBtnHint>' . t('play next') . '</playNextBtnHint>
      <volBtnHint>' . t('volume') . '</volBtnHint>
      <repeatBtnHint>' . t('repeat') . '</repeatBtnHint>
      <shuffleBtnHint>' . t('shuffle') . '</shuffleBtnHint>
      <hdBtnHint>' . t('HD') . '</hdBtnHint>
      <playlistBtnHint>' . t('open/close playlist') . '</playlistBtnHint>
      <libOnBtnHint>' . t('open library') . '</libOnBtnHint>
      <libOffBtnHint>' . t('close library') . '</libOffBtnHint>
      <fullScreenBtnHint>' . t('switch full screen') . '</fullScreenBtnHint>
      <backBtnHint>' . t('back to list') . '</backBtnHint>
      <replayBtnHint>' . t('Replay') . '</replayBtnHint>
      <nextBtnHint>' . t('Next') . '</nextBtnHint>
      <appBgColor>' . "0x" . $theme_row->appBgColor . '</appBgColor>
      <vidBgColor>' . "0x" . $theme_row->vidBgColor . '</vidBgColor>
      <framesBgColor>' . "0x" . $theme_row->framesBgColor . '</framesBgColor>
      <framesBgAlpha>' . (($theme_row->framesBgAlpha + 0) / 100) . '</framesBgAlpha>
      <ctrlsMainColor>' . "0x" . $theme_row->ctrlsMainColor . '</ctrlsMainColor>
      <ctrlsMainHoverColor>' . "0x" . $theme_row->ctrlsMainHoverColor . '</ctrlsMainHoverColor>
      <ctrlsMainAlpha>' . (($theme_row->ctrlsMainAlpha + 0) / 100) . '</ctrlsMainAlpha>
      <slideColor>' . "0x" . $theme_row->slideColor . '</slideColor>
      <itemBgHoverColor>' . "0x" . $theme_row->itemBgHoverColor . '</itemBgHoverColor>
      <itemBgSelectedColor>' . "0x" . $theme_row->itemBgSelectedColor . '</itemBgSelectedColor>
      <itemBgAlpha>' . (($theme_row->framesBgAlpha + 0) / 100) . '</itemBgAlpha>
      <textColor>' . "0x" . $theme_row->textColor . '</textColor>
      <textHoverColor>' . "0x" . $theme_row->textHoverColor . '</textHoverColor>
      <textSelectedColor>' . "0x" . $theme_row->textSelectedColor . '</textSelectedColor>
      <embed>' . url('spider_video_player/flash', array('query' => array('theme' => $theme_id, 'playlist' => $playlist_ids), 'absolute' => TRUE)) . '</embed>
    </settings>';
}

/**
 * Change to string.
 */
function spider_video_player_to_str($x) {
  if ($x) {
    return 'true';
  }
  else {
    return 'false';
  }
}

/**
 * Video player playlist.
 */
function spider_video_player_playlist() {
  if (isset($_GET['theme'])) {
    $theme_id = check_plain($_GET['theme']);
  }
  else {
    $theme_id = 1;
  }
  if (isset($_GET['playlist'])) {
    $playlist_ids = check_plain($_GET['playlist']);
  }
  else {
    $playlist_ids = '';
  }
  if ($playlist_ids != '') {
    $playlists = explode(',', $playlist_ids);
  }
  $show_trackid = db_query("SELECT show_trackid FROM {spider_video_player_theme} WHERE id=:id", array(':id' => $theme_id))->fetchField();
  echo '<library>';
  foreach ($playlists as $playlist_id) {
    if ($playlist_id != '') {
      $playlist = db_query("SELECT * FROM {spider_video_player_playlist} WHERE id=:id", array(':id' => $playlist_id))->fetchObject();
      if (!$playlist) {
        continue;
      }
      $videos = explode(',', $playlist->videos);
      echo '
          <albumFree title="' . htmlspecialchars($playlist->title) . '" thumb="' . $playlist->thumb . '" id="' . $playlist->id . '">';
      $i = 0;
      foreach ($videos as $video_id) {
        if ($video_id != '') {
          $video = db_query("SELECT * FROM {spider_video_player_video} WHERE id=:id", array(':id' => $video_id))->fetchObject();
          if (!$video) {
            continue;
          }
          $i++;
          echo '
            <track id="' . $video->id . '" type="' . $video->type . '"';
          echo ' url="' . htmlspecialchars($video->url) . '"';
          echo ' urlHD="' . htmlspecialchars($video->urlHD) . '"';
          echo ' fmsUrl="' . htmlspecialchars($video->fmsUrl) . '"';
          echo ' thumb="';
          if ($video->thumb) {
            echo htmlspecialchars($video->thumb);
          }
          echo '"';
          if ($show_trackid) {
            echo ' trackId="' . $i . '"';
          }
          echo '>' . $video->title . '
            </track>';
        }
      }
      echo '
          </albumFree>';
    }
  }
  echo '</library>';
}
