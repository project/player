<?php
/**
 * @file
 * Theme preview.
 */

/**
 * Theme preview.
 */
function spider_video_player_theme_preview() {
  echo '
    <script type="text/javascript" src="' . base_path() . drupal_get_path('module', 'spider_video_player') . '/js/spider_video_player_swfobject.js"></script>
    <div id="flashcontent"></div>
    <script>
      function get_radio_value(name) {
        for (var i = 0; i < window.parent.document.getElementsByName(name).length; i++) {
          if (window.parent.document.getElementsByName(name)[i].checked) {
            var rad_val = window.parent.document.getElementsByName(name)[i].value;
            return rad_val;
          }
        }
      }
      appWidth = parseInt(window.parent.document.getElementById("edit-appwidth").value);
      appHeight = parseInt(window.parent.document.getElementById("edit-appheight").value);
      document.getElementById("flashcontent").style.width = appWidth +"px";
      document.getElementById("flashcontent").style.height = appHeight + "px";
      playlistWidth = window.parent.document.getElementById("edit-playlistwidth").value;
      startWithLib = get_radio_value("startWithLib");
      show_trackid = get_radio_value("show_trackid");
      autoPlay = get_radio_value("autoPlay");
      autoNext = get_radio_value("autoNext");
      autoNextAlbum = get_radio_value("autoNextAlbum");
      defaultVol = window.parent.document.getElementById("spider_video_player_defaultVol").value;
      defaultRepeat = get_radio_value("defaultRepeat");
      defaultShuffle = get_radio_value("defaultShuffle");
      autohideTime = window.parent.document.getElementById("edit-autohidetime").value;
      centerBtnAlpha = window.parent.document.getElementById("spider_video_player_centerBtnAlpha").value;
      loadinAnimType = get_radio_value("loadinAnimType");
      keepAspectRatio	= get_radio_value("keepAspectRatio");
      clickOnVid = get_radio_value("clickOnVid");
      spaceOnVid = get_radio_value("spaceOnVid");
      mouseWheel = get_radio_value("mouseWheel");
      ctrlsPos = get_radio_value("ctrlsPos");
      ctrlsStack = window.parent.document.getElementById("ctrlsStack").value;
      ctrlsOverVid = get_radio_value("ctrlsOverVid");
      ctrlsSlideOut = get_radio_value("ctrlsSlideOut");
      watermarkUrl = window.parent.document.getElementById("thumb").value;
      watermarkPos = get_radio_value("watermarkPos");
      watermarkSize = window.parent.document.getElementById("edit-watermarksize").value;
      watermarkSpacing = window.parent.document.getElementById("edit-watermarkspacing").value;
      watermarkAlpha = window.parent.document.getElementById("spider_video_player_watermarkAlpha").value;
      playlistPos = get_radio_value("playlistPos");
      playlistOverVid = get_radio_value("playlistOverVid");
      playlistAutoHide = get_radio_value("playlistAutoHide");
      playlistTextSize = window.parent.document.getElementById("edit-playlisttextsize").value;
      libCols = window.parent.document.getElementById("edit-libcols").value;
      libRows = window.parent.document.getElementById("edit-librows").value;
      libListTextSize = window.parent.document.getElementById("edit-liblisttextsize").value;
      libDetailsTextSize = window.parent.document.getElementById("edit-libdetailstextsize").value;
      appBgColor = window.parent.document.getElementById("edit-appbgcolor").value;
      vidBgColor = window.parent.document.getElementById("edit-vidbgcolor").value;
      framesBgColor = window.parent.document.getElementById("edit-framesbgcolor").value;
      ctrlsMainColor = window.parent.document.getElementById("edit-ctrlsmaincolor").value;
      ctrlsMainHoverColor = window.parent.document.getElementById("edit-ctrlsmainhovercolor").value;
      slideColor = window.parent.document.getElementById("edit-slidecolor").value;
      itemBgHoverColor = window.parent.document.getElementById("edit-itembghovercolor").value;
      itemBgSelectedColor = window.parent.document.getElementById("edit-itembgselectedcolor").value;
      textColor = window.parent.document.getElementById("edit-textcolor").value;
      textHoverColor = window.parent.document.getElementById("edit-texthovercolor").value;
      textSelectedColor = window.parent.document.getElementById("edit-textselectedcolor").value;
      framesBgAlpha = window.parent.document.getElementById("spider_video_player_framesBgAlpha").value;
      ctrlsMainAlpha = window.parent.document.getElementById("spider_video_player_ctrlsMainAlpha").value;

      str="' . url('admin/settings/spider_video_player/themes/preview/settings_xml', array('query' => array('appWidth' => ''), 'absolute' => TRUE)) . '"
        + appWidth
        +"@appHeight="+appHeight
        +"@playlistWidth="+playlistWidth
        +"@startWithLib="+startWithLib
        +"@show_trackid="+show_trackid
        +"@autoPlay="+autoPlay
        +"@autoNext="+appHeight
        +"@autoNextAlbum="+autoNextAlbum
        +"@defaultVol="+defaultVol
        +"@defaultRepeat="+defaultRepeat
        +"@defaultShuffle="+defaultShuffle
        +"@autohideTime="+autohideTime
        +"@centerBtnAlpha="+centerBtnAlpha
        +"@loadinAnimType="+loadinAnimType
        +"@keepAspectRatio="+keepAspectRatio
        +"@clickOnVid="+clickOnVid
        +"@spaceOnVid="+spaceOnVid
        +"@mouseWheel="+mouseWheel
        +"@ctrlsPos="+ctrlsPos
        +"@ctrlsStack=["+ctrlsStack
        +"]@ctrlsOverVid="+ctrlsOverVid
        +"@ctrlsSlideOut="+ctrlsSlideOut
        +"@watermarkUrl="+watermarkUrl
        +"@watermarkPos="+watermarkPos
        +"@watermarkSize="+watermarkSize
        +"@watermarkSpacing="+watermarkSpacing
        +"@watermarkAlpha="+watermarkAlpha
        +"@playlistPos="+playlistPos
        +"@playlistOverVid="+playlistOverVid
        +"@playlistAutoHide="+playlistAutoHide
        +"@playlistTextSize="+playlistTextSize
        +"@libCols="+libCols
        +"@libRows="+libRows
        +"@libListTextSize="+libListTextSize
        +"@libDetailsTextSize="+libDetailsTextSize
        +"@appBgColor="+appBgColor
        +"@vidBgColor="+vidBgColor
        +"@framesBgColor="+framesBgColor
        +"@ctrlsMainColor="+ctrlsMainColor
        +"@ctrlsMainHoverColor="+ctrlsMainHoverColor
        +"@slideColor="+slideColor
        +"@itemBgHoverColor="+itemBgHoverColor
        +"@itemBgSelectedColor="+itemBgSelectedColor
        +"@textColor="+textColor
        +"@textHoverColor="+textHoverColor
        +"@textSelectedColor="+textSelectedColor
        +"@framesBgAlpha="+framesBgAlpha
        +"@ctrlsMainAlpha="+ctrlsMainAlpha;
      str = str.replace("&","@");
      var playlist = "' . url('admin/settings/spider_video_player/themes/preview/playlist_xml', array('query' => array('show_trackid' => ''), 'absolute' => TRUE)) . '" + show_trackid;
      playlist = playlist.replace("&","@");
      var so = new SWFObject("' . base_path() . drupal_get_path('module', 'spider_video_player') . '/videoPlayer.swf?wdrand=' . mt_rand() . '", "Player", "100%", "100%", "8", "#000000");
      so.addParam("FlashVars", "settingsUrl=" + str + "&playlistUrl=" + playlist);
      so.addParam("quality", "high");
      so.addParam("menu", "false");
      so.addParam("wmode", "transparent");
      so.addParam("loop", "false");
      so.addParam("allowfullscreen", "true");
      so.write("flashcontent");
    </script>';
}

/**
 * Video player parameters for theme preview.
 */
function spider_video_player_preview_settings() {
  if (isset($_GET['ctrlsStack'])) {
    $ctrls_stack = str_replace("[", "", check_plain($_GET['ctrlsStack']));
  }
  else {
    $ctrls_stack = str_replace("[", "", 'playlist:1,lib:1,playPrev:1,playPause:1,playNext:1,stop:0,time:1,vol:1,+:1,hd:1,repeat:1,shuffle:1,play:0,pause:0,share:1,fullScreen:1');
  }
  $ctrls_stack = str_replace(", :", ", +:", $ctrls_stack);
  $new = '';
  if ($ctrls_stack) {
    $ctrls = explode(",", $ctrls_stack);
    foreach ($ctrls as $key => $x) {
      $y = explode(":", $x);
      $ctrl = $y[0];
      $active = $y[1];
      if ($active == 1) {
        if ($new == '') {
          $new = $y[0];
        }
        else {
          $new = $new . ',' . $y[0];
        }
      }
    }
  }
  echo '
    <settings>
      <appWidth>' . ((isset($_GET['appWidth'])) ? check_plain($_GET['appWidth']) : '') . '</appWidth>
      <appHeight>' . ((isset($_GET['appHeight'])) ? check_plain($_GET['appHeight']) : '') . '</appHeight>
      <playlistWidth>' . ((isset($_GET['playlistWidth'])) ? check_plain($_GET['playlistWidth']) : '') . '</playlistWidth>
      <startWithLib>' . spider_video_player_change_to_str(((isset($_GET['startWithLib'])) ? check_plain($_GET['startWithLib']) : '')) . '</startWithLib>
      <autoPlay>' . spider_video_player_change_to_str(((isset($_GET['autoPlay'])) ? check_plain($_GET['autoPlay']) : '')) . '</autoPlay>
      <autoNext>' . spider_video_player_change_to_str(((isset($_GET['autoNext'])) ? check_plain($_GET['autoNext']) : '')) . '</autoNext>
      <autoNextAlbum>' . spider_video_player_change_to_str(((isset($_GET['autoNextAlbum'])) ? check_plain($_GET[ 'autoNextAlbum']) : '')) . '</autoNextAlbum>
      <defaultVol>' . ((((isset($_GET['defaultVol'])) ? check_plain($_GET['defaultVol']) : '') + 0) / 100) . '</defaultVol>
      <defaultRepeat>' . ((isset($_GET['defaultRepeat'])) ? check_plain($_GET['defaultRepeat']) : '') . '</defaultRepeat>
      <defaultShuffle>' . ((isset($_GET['defaultShuffle'])) ? check_plain($_GET['defaultShuffle']) : '') . '</defaultShuffle>
      <autohideTime>' . ((isset($_GET['autohideTime'])) ? check_plain($_GET[ 'autohideTime']) : '') . '</autohideTime>
      <centerBtnAlpha>' . ((((isset($_GET['centerBtnAlpha'])) ? check_plain($_GET['centerBtnAlpha']) : '') + 0) / 100) . '</centerBtnAlpha>
      <loadinAnimType>' . ((isset($_GET['loadinAnimType'])) ? check_plain($_GET['loadinAnimType']) : '') . '</loadinAnimType>
      <keepAspectRatio>' . spider_video_player_change_to_str(((isset($_GET['keepAspectRatio'])) ? check_plain($_GET[ 'keepAspectRatio']) : '')) . '</keepAspectRatio>
      <clickOnVid>' . spider_video_player_change_to_str(((isset($_GET['clickOnVid'])) ? check_plain($_GET['clickOnVid']) : '')) . '</clickOnVid>
      <spaceOnVid>' . spider_video_player_change_to_str(((isset($_GET['spaceOnVid'])) ? check_plain($_GET['spaceOnVid']) : '')) . '</spaceOnVid>
      <mouseWheel>' . spider_video_player_change_to_str(((isset($_GET['mouseWheel'])) ? check_plain($_GET['mouseWheel']) : '')) . '</mouseWheel>
      <ctrlsPos>' . ((isset($_GET['ctrlsPos'])) ? check_plain($_GET['ctrlsPos']) : '') . '</ctrlsPos>
      <ctrlsStack>' . $new . '</ctrlsStack>
      <ctrlsOverVid>' . spider_video_player_change_to_str(((isset($_GET['ctrlsOverVid'])) ? check_plain($_GET['ctrlsOverVid']) : '')) . '</ctrlsOverVid>
      <ctrlsSlideOut>' . spider_video_player_change_to_str(((isset($_GET['ctrlsSlideOut'])) ? check_plain($_GET['ctrlsSlideOut']) : '')) . '</ctrlsSlideOut>
      <watermarkUrl>' . ((isset($_GET['watermarkUrl'])) ? check_plain($_GET['watermarkUrl']) : '') . '</watermarkUrl>
      <watermarkPos>' . ((isset($_GET['watermarkPos'])) ? check_plain($_GET['watermarkPos']) : '') . '</watermarkPos>
      <watermarkSize>' . ((isset($_GET['watermarkSize'])) ? check_plain($_GET['watermarkSize']) : '') . '</watermarkSize>
      <watermarkSpacing>' . ((isset($_GET['watermarkSpacing'])) ? check_plain($_GET['watermarkSpacing']) : '') . '</watermarkSpacing>
      <watermarkAlpha>' . ((((isset($_GET['watermarkAlpha'])) ? check_plain($_GET['watermarkAlpha']) : '') + 0) / 100) . '</watermarkAlpha>
      <playlistPos>' . ((isset($_GET['playlistPos'])) ? check_plain($_GET['playlistPos']) : '') . '</playlistPos>
      <playlistOverVid>' . spider_video_player_change_to_str(((isset($_GET['playlistOverVid'])) ? check_plain($_GET['playlistOverVid']) : '')) . '</playlistOverVid>
      <playlistAutoHide>' . spider_video_player_change_to_str(((isset($_GET['playlistAutoHide'])) ? check_plain($_GET['playlistAutoHide']) : '')) . '</playlistAutoHide>
      <playlistTextSize>' . ((isset($_GET['playlistTextSize'])) ? check_plain($_GET['playlistTextSize']) : '') . '</playlistTextSize>
      <libCols>' . ((isset($_GET['libCols'])) ? check_plain($_GET['libCols']) : '') . '</libCols>
      <libRows>' . ((isset($_GET['libRows'])) ? check_plain($_GET['libRows']) : '') . '</libRows>
      <libListTextSize>' . ((isset($_GET['libListTextSize'])) ? check_plain($_GET['libListTextSize']) : '') . '</libListTextSize>
      <libDetailsTextSize>' . ((isset($_GET['libDetailsTextSize'])) ? check_plain($_GET['libDetailsTextSize']) : '') . '</libDetailsTextSize>
      <playBtnHint>' . t('play') . '</playBtnHint>
      <pauseBtnHint>' . t('pause') . '</pauseBtnHint>
      <playPauseBtnHint>' . t('toggle pause') . '</playPauseBtnHint>
      <stopBtnHint>' . t('stop') . '</stopBtnHint>
      <playPrevBtnHint>' . t('play previous') . '</playPrevBtnHint>
      <playNextBtnHint>' . t('play next') . '</playNextBtnHint>
      <volBtnHint>' . t('volume') . '</volBtnHint>
      <repeatBtnHint>' . t('repeat') . '</repeatBtnHint>
      <shuffleBtnHint>' . t('shuffle') . '</shuffleBtnHint>
      <hdBtnHint>' . t('HD') . '</hdBtnHint>
      <playlistBtnHint>' . t('open/close playlist') . '</playlistBtnHint>
      <libOnBtnHint>' . t('open library') . '</libOnBtnHint>
      <libOffBtnHint>' . t('close library') . '</libOffBtnHint>
      <fullScreenBtnHint>' . t('switch full screen') . '</fullScreenBtnHint>
      <backBtnHint>' . t('back to list') . '</backBtnHint>
      <replayBtnHint>' . t('Replay') . '</replayBtnHint>
      <nextBtnHint>' . t('Next') . '</nextBtnHint>
      <appBgColor>' . "0x" . ((isset($_GET['appBgColor'])) ? check_plain($_GET['appBgColor']) : '') . '</appBgColor>
      <vidBgColor>' . "0x" . ((isset($_GET['vidBgColor'])) ? check_plain($_GET['vidBgColor']) : '') . '</vidBgColor>
      <framesBgColor>' . "0x" . ((isset($_GET['framesBgColor'])) ? check_plain($_GET['framesBgColor']) : '') . '</framesBgColor>
      <framesBgAlpha>' . ((((isset($_GET['framesBgAlpha'])) ? check_plain($_GET['framesBgAlpha']) : '') + 0) / 100) . '</framesBgAlpha>
      <ctrlsMainColor>' . "0x" . ((isset($_GET['ctrlsMainColor'])) ? check_plain($_GET['ctrlsMainColor']) : '') . '</ctrlsMainColor>
      <ctrlsMainHoverColor>' . "0x" . ((isset($_GET['ctrlsMainHoverColor'])) ? check_plain($_GET['ctrlsMainHoverColor']) : '') . '</ctrlsMainHoverColor>
      <ctrlsMainAlpha>' . ((((isset($_GET['ctrlsMainAlpha'])) ? check_plain($_GET['ctrlsMainAlpha']) : '') + 0) / 100) . '</ctrlsMainAlpha>
      <slideColor>' . "0x" . ((isset($_GET['slideColor'])) ? check_plain($_GET['slideColor']) : '') . '</slideColor>
      <itemBgHoverColor>' . "0x" . ((isset($_GET['itemBgHoverColor'])) ? check_plain($_GET['itemBgHoverColor']) : '') . '</itemBgHoverColor>
      <itemBgSelectedColor>' . "0x" . ((isset($_GET['itemBgSelectedColor'])) ? check_plain($_GET['itemBgSelectedColor']) : '') . '</itemBgSelectedColor>
      <itemBgAlpha>' . ((((isset($_GET['framesBgAlpha'])) ? check_plain($_GET['framesBgAlpha']) : '') + 0) / 100) . '</itemBgAlpha>
      <textColor>' . "0x" . ((isset($_GET['textColor'])) ? check_plain($_GET['textColor']) : '') . '</textColor>
      <textHoverColor>' . "0x" . ((isset($_GET['textHoverColor'])) ? check_plain($_GET['textHoverColor']) : '') . '</textHoverColor>
      <textSelectedColor>' . "0x" . ((isset($_GET['textSelectedColor'])) ? check_plain($_GET['textSelectedColor']) : '') . '</textSelectedColor>
    </settings>';
  exit;
}

function spider_video_player_change_to_str($x) {
  if ($x) {
    return 'true';
  }
  else {
    return 'false';
  }
}

/**
 * Video player playlist for theme preview.
 */
function spider_video_player_preview_playlist() {
  if (isset($_GET['show_trackid'])) {
    $show_trackid = check_plain($_GET['show_trackid']);
  }
  else {
    $show_trackid = 1;
  }
  echo '
    <library>
      <album title="' . t('Nature') . '" thumb="' . base_path() . drupal_get_path('module', 'spider_video_player') . '/upload/sunset4.jpg" id="1">
        <track id="1" type="youtube" url="http://www.youtube.com/watch?v=eaE8N6alY0Y" thumb="' . base_path() . drupal_get_path('module', 'spider_video_player') . '/upload/red-sunset-casey1.jpg" ';
  if ($show_trackid) {
    echo' trackId="1" ';
  }
  echo '>' . t('Sunset 1') . '</track>
        <track id="2" type="youtube" url="http://www.youtube.com/watch?v=y3eFdvDdXx0" thumb="' . base_path() . drupal_get_path('module', 'spider_video_player') . '/upload/sunset10.jpg"';
  if ($show_trackid) {
    echo 'trackId="2"';
  }
  echo '>' . t('Sunset 2') . '</track>
      </album>
    </library>';
  exit;
}
