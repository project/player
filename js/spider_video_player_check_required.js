/**
 * Check required.
 */
function spider_video_player_check_required(title, param, param_string) {
  if (document.getElementById(title).value == "") {
    alert(Drupal.t('"Title *"  field is required.'));
    return false;
  }
  if ((param_string != '') && (param != '')) {
    var string = document.getElementById(param_string).value;
    var ids = string.split(",");
    for (index in ids) {
      var key = ids[index];
      if (key != '') {
        if (document.getElementById(param + key).value  == "") {
          var parent_node = document.getElementById(param + key).parentNode;
          var title = parent_node.getElementsByTagName("label")[0].firstChild.nodeValue;
          alert(Drupal.t('"' + title + '*"  field is required.'));
          return false;
        }
      }
    }
  }
}
