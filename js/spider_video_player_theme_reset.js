/**
 * Select theme.
 */
function spider_video_player_set_theme() {
  themeID = document.getElementById('edit-default-themes').value;
  var if_set;
  switch (themeID) {
    case '1':
      if_set = spider_video_player_reset_theme_1();
      if (if_set) {
        document.getElementById("edit-theme-title").value = "new Theme 1";
      }
      break;

    case '2':
      if_set = spider_video_player_reset_theme_2();
      if (if_set) {
        document.getElementById("edit-theme-title").value = "new Theme 2";
      }
      break;

    case '3':
      if_set = spider_video_player_reset_theme_3();
      if (if_set) {
        document.getElementById("edit-theme-title").value = "new Theme 3";
      }
      break;

    case '4':
      if_set = spider_video_player_reset_theme_4();
      if (if_set) {
        document.getElementById("edit-theme-title").value = "new Theme 4";
      }
      break;

    case '5':
      if_set = spider_video_player_reset_theme_5();
      if (if_set) {
        document.getElementById("edit-theme-title").value = "new Theme 5";
      }
      break;

    case '6':
      if_set = spider_video_player_reset_theme_6();
      if (if_set) {
        document.getElementById("edit-theme-title").value = "new Theme 6";
      }
      break;

    case '7':
      if_set = spider_video_player_reset_theme_7();
      if (if_set) {
        document.getElementById("edit-theme-title").value = "new Theme 7";
      }
      break;
  }
}

/**
 * Change slider on reset default theme.
 */
function spider_video_player_reset_theme_slider(idtaginp, idtagdiv, inpvalue) {
  jQuery("#" + idtagdiv).slider({
    range:"min",
    value:inpvalue,
    min:0,
    max:100,
    slide:function (event, ui) {
      jQuery("#" + idtaginp).val("" + ui.value);
    }
  });
  jQuery("#" + idtaginp).val("" + jQuery("#" + idtagdiv).slider("value"));
}

/**
 * Reset default theme.
 */
function spider_video_player_reset_theme_1() {
  if (confirm(Drupal.t('Do you really want to reset theme?'))) {
    // General parametres.
    document.getElementById("edit-appwidth").value = "640";
    document.getElementById("edit-appheight").value = "480";
    document.getElementById("edit-startwithlib-1").checked = false;
    document.getElementById("edit-startwithlib-0").checked = true;
    document.getElementById("edit-show-trackid-1").checked = true;
    document.getElementById("edit-show-trackid-0").checked = false;
    document.getElementById("edit-autohidetime").value = "5";
    document.getElementById("edit-keepaspectratio-1").checked = true;
    document.getElementById("edit-keepaspectratio-0").checked = false;
    document.getElementById("edit-ctrlsovervid-1").checked = true;
    document.getElementById("edit-ctrlsovervid-0").checked = false;
    document.getElementById("thumb").value = "";
    document.getElementById("imagebox").src = "";
    document.getElementById("imagebox").height = "";
    document.getElementById("edit-watermarkpos-1").checked = true;
    document.getElementById("edit-watermarkpos-2").checked = false;
    document.getElementById("edit-watermarkpos-3").checked = false;
    document.getElementById("edit-watermarkpos-4").checked = false;
    document.getElementById("edit-watermarksize").value = "50";
    document.getElementById("edit-watermarkspacing").value = "0";
    document.getElementById("spider_video_player_watermarkAlpha").value = "50";

    // Style parametres.
    document.getElementById("spider_video_player_centerBtnAlpha").value = "50";
    document.getElementById("spider_video_player_framesBgAlpha").value = "50";
    document.getElementById("spider_video_player_ctrlsMainAlpha").value = "79";
    // If jscolor exists.
    if (document.getElementById("edit-appbgcolor").color) {
      document.getElementById("edit-appbgcolor").color.fromString("001326");
      document.getElementById("edit-vidbgcolor").color.fromString("001326");
      document.getElementById("edit-framesbgcolor").color.fromString("3665A3");
      document.getElementById("edit-ctrlsmaincolor").color.fromString("C0B8F2");
      document.getElementById("edit-ctrlsmainhovercolor").color.fromString("000000");
      document.getElementById("edit-slidecolor").color.fromString("00A2FF");
      document.getElementById("edit-itembghovercolor").color.fromString("DAE858");
      document.getElementById("edit-itembgselectedcolor").color.fromString("0C8A58");
      document.getElementById("edit-textcolor").color.fromString("DEDEDE");
      document.getElementById("edit-texthovercolor").color.fromString("000000");
      document.getElementById("edit-textselectedcolor").color.fromString("FFFFFF");
    }
    else {
      document.getElementById("edit-appbgcolor").value = "001326";
      document.getElementById("edit-vidbgcolor").value = "001326";
      document.getElementById("edit-framesbgcolor").value = "3665A3";
      document.getElementById("edit-ctrlsmaincolor").value = "C0B8F2";
      document.getElementById("edit-ctrlsmainhovercolor").value = "000000";
      document.getElementById("edit-slidecolor").value = "00A2FF";
      document.getElementById("edit-itembghovercolor").value = "DAE858";
      document.getElementById("edit-itembgselectedcolor").value = "0C8A58";
      document.getElementById("edit-textcolor").value = "DEDEDE";
      document.getElementById("edit-texthovercolor").value = "000000";
      document.getElementById("edit-textselectedcolor").value = "FFFFFF";
    }
    document.getElementById("edit-loadinanimtype-1").checked = false;
    document.getElementById("edit-loadinanimtype-2").checked = true;

    // Playback parameters.
    document.getElementById("edit-autoplay-1").checked = false;
    document.getElementById("edit-autoplay-0").checked = true;
    document.getElementById("edit-autonext-1").checked = false;
    document.getElementById("edit-autonext-0").checked = true;
    document.getElementById("edit-autonextalbum-1").checked = false;
    document.getElementById("edit-autonextalbum-0").checked = true;
    document.getElementById("spider_video_player_defaultVol").value = "50";
    document.getElementById("edit-defaultrepeat-repeatone").checked = false;
    document.getElementById("edit-defaultrepeat-repeatoff").checked = true;
    document.getElementById("edit-defaultrepeat-repeatall").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleon").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleoff").checked = true;
    document.getElementById("edit-ctrlsslideout-1").checked = true;
    document.getElementById("edit-ctrlsslideout-0").checked = false;

    // Playlist and Library parameters.
    document.getElementById("edit-playlistpos-1").checked = true;
    document.getElementById("edit-playlistpos-2").checked = false;
    document.getElementById("edit-playlistwidth").value = "100";
    document.getElementById("edit-playlistovervid-1").checked = true;
    document.getElementById("edit-playlistovervid-0").checked = false;
    document.getElementById("edit-playlistautohide-1").checked = true;
    document.getElementById("edit-playlistautohide-0").checked = false;
    document.getElementById("edit-playlisttextsize").value = "12";
    document.getElementById("edit-libcols").value = "3";
    document.getElementById("edit-librows").value = "3";
    document.getElementById("edit-liblisttextsize").value = "16";
    document.getElementById("edit-libdetailstextsize").value = "20";

    // Video Control parameters.
    document.getElementById("edit-clickonvid-1").checked = true;
    document.getElementById("edit-clickonvid-0").checked = false;
    document.getElementById("edit-spaceonvid-1").checked = true;
    document.getElementById("edit-spaceonvid-0").checked = false;
    document.getElementById("edit-mousewheel-1").checked = true;
    document.getElementById("edit-mousewheel-0").checked = false;
    document.getElementById("edit-ctrlspos-1").checked = true;
    document.getElementById("edit-ctrlspos-2").checked = false;
    document.getElementById("ctrlsStack").value = "playlist:1,playPrev:0,lib:1,playPause:1,playNext:1,stop:0,time:1,vol:1,+:1,hd:1,repeat:1,shuffle:1,play:0,pause:0,share:1,fullScreen:1";

    // Reset sliders.
    spider_video_player_reset_theme_slider("spider_video_player_watermarkAlpha", "slider-watermarkAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_centerBtnAlpha", "slider-centerBtnAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_framesBgAlpha", "slider-framesBgAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_ctrlsMainAlpha", "slider-ctrlsMainAlpha", "79");
    spider_video_player_reset_theme_slider("spider_video_player_defaultVol", "slider-defaultVol", "50");
    return true;
  }
  else {
    return false;
  }
}

function spider_video_player_reset_theme_2() {
  if (confirm(Drupal.t('Do you really want to reset theme?'))) {
    // General parametres.
    document.getElementById("edit-appwidth").value = "640";
    document.getElementById("edit-appheight").value = "480";
    document.getElementById("edit-startwithlib-1").checked = false;
    document.getElementById("edit-startwithlib-0").checked = true;
    document.getElementById("edit-show-trackid-1").checked = true;
    document.getElementById("edit-show-trackid-0").checked = false;
    document.getElementById("edit-autohidetime").value = "5";
    document.getElementById("edit-keepaspectratio-1").checked = true;
    document.getElementById("edit-keepaspectratio-0").checked = false;
    document.getElementById("edit-ctrlsovervid-1").checked = true;
    document.getElementById("edit-ctrlsovervid-0").checked = false;
    document.getElementById("thumb").value = "";
    document.getElementById("imagebox").src = "";
    document.getElementById("imagebox").height = "";
    document.getElementById("edit-watermarkpos-1").checked = true;
    document.getElementById("edit-watermarkpos-2").checked = false;
    document.getElementById("edit-watermarkpos-3").checked = false;
    document.getElementById("edit-watermarkpos-4").checked = false;
    document.getElementById("edit-watermarksize").value = "50";
    document.getElementById("edit-watermarkspacing").value = "0";
    document.getElementById("spider_video_player_watermarkAlpha").value = "50";

    // Style parametres.
    document.getElementById("spider_video_player_centerBtnAlpha").value = "50";
    document.getElementById("spider_video_player_framesBgAlpha").value = "82";
    document.getElementById("spider_video_player_ctrlsMainAlpha").value = "79";
    // If jscolor exists.
    if (document.getElementById("edit-appbgcolor").color) {
      document.getElementById("edit-appbgcolor").color.fromString("FFBB00");
      document.getElementById("edit-vidbgcolor").color.fromString("001326");
      document.getElementById("edit-framesbgcolor").color.fromString("FFA200");
      document.getElementById("edit-ctrlsmaincolor").color.fromString("030000");
      document.getElementById("edit-ctrlsmainhovercolor").color.fromString("595959");
      document.getElementById("edit-slidecolor").color.fromString("FF0000");
      document.getElementById("edit-itembghovercolor").color.fromString("E8E84D");
      document.getElementById("edit-itembgselectedcolor").color.fromString("FF5500");
      document.getElementById("edit-textcolor").color.fromString("EBEBEB");
      document.getElementById("edit-texthovercolor").color.fromString("000000");
      document.getElementById("edit-textselectedcolor").color.fromString("FFFFFF");
    }
    else {
      document.getElementById("edit-appbgcolor").value = "FFBB00";
      document.getElementById("edit-vidbgcolor").value = "001326";
      document.getElementById("edit-framesbgcolor").value = "FFA200";
      document.getElementById("edit-ctrlsmaincolor").value = "030000";
      document.getElementById("edit-ctrlsmainhovercolor").value = "595959";
      document.getElementById("edit-slidecolor").value = "FF0000";
      document.getElementById("edit-itembghovercolor").value = "E8E84D";
      document.getElementById("edit-itembgselectedcolor").value = "FF5500";
      document.getElementById("edit-textcolor").value = "EBEBEB";
      document.getElementById("edit-texthovercolor").value = "000000";
      document.getElementById("edit-textselectedcolor").value = "FFFFFF";
    }
    document.getElementById("edit-loadinanimtype-1").checked = false;
    document.getElementById("edit-loadinanimtype-2").checked = true;

    // Playback parameters.
    document.getElementById("edit-autoplay-1").checked = false;
    document.getElementById("edit-autoplay-0").checked = true;
    document.getElementById("edit-autonext-1").checked = false;
    document.getElementById("edit-autonext-0").checked = true;
    document.getElementById("edit-autonextalbum-1").checked = false;
    document.getElementById("edit-autonextalbum-0").checked = true;
    document.getElementById("spider_video_player_defaultVol").value = "50";
    document.getElementById("edit-defaultrepeat-repeatone").checked = false;
    document.getElementById("edit-defaultrepeat-repeatoff").checked = true;
    document.getElementById("edit-defaultrepeat-repeatall").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleon").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleoff").checked = true;
    document.getElementById("edit-ctrlsslideout-1").checked = true;
    document.getElementById("edit-ctrlsslideout-0").checked = false;

    // Playlist and Library parameters.
    document.getElementById("edit-playlistpos-1").checked = true;
    document.getElementById("edit-playlistpos-2").checked = false;
    document.getElementById("edit-playlistwidth").value = "120";
    document.getElementById("edit-playlistovervid-1").checked = false;
    document.getElementById("edit-playlistovervid-0").checked = true;
    document.getElementById("edit-playlistautohide-1").checked = true;
    document.getElementById("edit-playlistautohide-0").checked = false;
    document.getElementById("edit-playlisttextsize").value = "12";
    document.getElementById("edit-libcols").value = "3";
    document.getElementById("edit-librows").value = "3";
    document.getElementById("edit-liblisttextsize").value = "16";
    document.getElementById("edit-libdetailstextsize").value = "20";

    // Video Control parameters.
    document.getElementById("edit-clickonvid-1").checked = true;
    document.getElementById("edit-clickonvid-0").checked = false;
    document.getElementById("edit-spaceonvid-1").checked = true;
    document.getElementById("edit-spaceonvid-0").checked = false;
    document.getElementById("edit-mousewheel-1").checked = true;
    document.getElementById("edit-mousewheel-0").checked = false;
    document.getElementById("edit-ctrlspos-1").checked = false;
    document.getElementById("edit-ctrlspos-2").checked = true;
    document.getElementById("ctrlsStack").value = "playPrev:1,play:1,playNext:1,stop:0,playlist:1,lib:1,playPause:0,vol:1,+:1,time:1,hd:1,repeat:1,shuffle:1,pause:0,share:1,fullScreen:1";

    // Reset sliders.
    spider_video_player_reset_theme_slider("spider_video_player_watermarkAlpha", "slider-watermarkAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_centerBtnAlpha", "slider-centerBtnAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_framesBgAlpha", "slider-framesBgAlpha", "82");
    spider_video_player_reset_theme_slider("spider_video_player_ctrlsMainAlpha", "slider-ctrlsMainAlpha", "79");
    spider_video_player_reset_theme_slider("spider_video_player_defaultVol", "slider-defaultVol", "50");
    return true;
  }
  else {
    return false;
  }
}

function spider_video_player_reset_theme_3() {
  if (confirm(Drupal.t('Do you really want to reset theme?'))) {
    // General parametres.
    document.getElementById("edit-appwidth").value = "640";
    document.getElementById("edit-appheight").value = "480";
    document.getElementById("edit-startwithlib-1").checked = false;
    document.getElementById("edit-startwithlib-0").checked = true;
    document.getElementById("edit-show-trackid-1").checked = true;
    document.getElementById("edit-show-trackid-0").checked = false;
    document.getElementById("edit-autohidetime").value = "5";
    document.getElementById("edit-keepaspectratio-1").checked = true;
    document.getElementById("edit-keepaspectratio-0").checked = false;
    document.getElementById("edit-ctrlsovervid-1").checked = true;
    document.getElementById("edit-ctrlsovervid-0").checked = false;
    document.getElementById("thumb").value = "";
    document.getElementById("imagebox").src = "";
    document.getElementById("imagebox").height = "";
    document.getElementById("edit-watermarkpos-1").checked = true;
    document.getElementById("edit-watermarkpos-2").checked = false;
    document.getElementById("edit-watermarkpos-3").checked = false;
    document.getElementById("edit-watermarkpos-4").checked = false;
    document.getElementById("edit-watermarksize").value = "50";
    document.getElementById("edit-watermarkspacing").value = "0";
    document.getElementById("spider_video_player_watermarkAlpha").value = "50";

    // Style parametres.
    document.getElementById("spider_video_player_centerBtnAlpha").value = "50";
    document.getElementById("spider_video_player_framesBgAlpha").value = "65";
    document.getElementById("spider_video_player_ctrlsMainAlpha").value = "99";
    // If jscolor exists.
    if (document.getElementById("edit-appbgcolor").color) {
      document.getElementById("edit-appbgcolor").color.fromString("FF0000");
      document.getElementById("edit-vidbgcolor").color.fromString("070801");
      document.getElementById("edit-framesbgcolor").color.fromString("D10000");
      document.getElementById("edit-ctrlsmaincolor").color.fromString("FFFFFF");
      document.getElementById("edit-ctrlsmainhovercolor").color.fromString("00A2FF");
      document.getElementById("edit-slidecolor").color.fromString("00A2FF");
      document.getElementById("edit-itembghovercolor").color.fromString("F0FF61");
      document.getElementById("edit-itembgselectedcolor").color.fromString("00A2FF");
      document.getElementById("edit-textcolor").color.fromString("DEDEDE");
      document.getElementById("edit-texthovercolor").color.fromString("000000");
      document.getElementById("edit-textselectedcolor").color.fromString("FFFFFF");
    }
    else {
      document.getElementById("edit-appbgcolor").value = "FF0000";
      document.getElementById("edit-vidbgcolor").value = "070801";
      document.getElementById("edit-framesbgcolor").value = "D10000";
      document.getElementById("edit-ctrlsmaincolor").value = "FFFFFF";
      document.getElementById("edit-ctrlsmainhovercolor").value = "00A2FF";
      document.getElementById("edit-slidecolor").value = "00A2FF";
      document.getElementById("edit-itembghovercolor").value = "F0FF61";
      document.getElementById("edit-itembgselectedcolor").value = "00A2FF";
      document.getElementById("edit-textcolor").value = "DEDEDE";
      document.getElementById("edit-texthovercolor").value = "000000";
      document.getElementById("edit-textselectedcolor").value = "FFFFFF";
    }
    document.getElementById("edit-loadinanimtype-1").checked = false;
    document.getElementById("edit-loadinanimtype-2").checked = true;

    // Playback parameters.
    document.getElementById("edit-autoplay-1").checked = false;
    document.getElementById("edit-autoplay-0").checked = true;
    document.getElementById("edit-autonext-1").checked = false;
    document.getElementById("edit-autonext-0").checked = true;
    document.getElementById("edit-autonextalbum-1").checked = false;
    document.getElementById("edit-autonextalbum-0").checked = true;
    document.getElementById("spider_video_player_defaultVol").value = "50";
    document.getElementById("edit-defaultrepeat-repeatone").checked = false;
    document.getElementById("edit-defaultrepeat-repeatoff").checked = true;
    document.getElementById("edit-defaultrepeat-repeatall").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleon").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleoff").checked = true;
    document.getElementById("edit-ctrlsslideout-1").checked = true;
    document.getElementById("edit-ctrlsslideout-0").checked = false;

    // Playlist and Library parameters.
    document.getElementById("edit-playlistpos-1").checked = true;
    document.getElementById("edit-playlistpos-2").checked = false;
    document.getElementById("edit-playlistwidth").value = "140";
    document.getElementById("edit-playlistovervid-1").checked = true;
    document.getElementById("edit-playlistovervid-0").checked = false;
    document.getElementById("edit-playlistautohide-1").checked = true;
    document.getElementById("edit-playlistautohide-0").checked = false;
    document.getElementById("edit-playlisttextsize").value = "12";
    document.getElementById("edit-libcols").value = "3";
    document.getElementById("edit-librows").value = "3";
    document.getElementById("edit-liblisttextsize").value = "16";
    document.getElementById("edit-libdetailstextsize").value = "20";

    // Video Control parameters.
    document.getElementById("edit-clickonvid-1").checked = true;
    document.getElementById("edit-clickonvid-0").checked = false;
    document.getElementById("edit-spaceonvid-1").checked = true;
    document.getElementById("edit-spaceonvid-0").checked = false;
    document.getElementById("edit-mousewheel-1").checked = true;
    document.getElementById("edit-mousewheel-0").checked = false;
    document.getElementById("edit-ctrlspos-1").checked = true;
    document.getElementById("edit-ctrlspos-2").checked = false;
    document.getElementById("ctrlsStack").value = "playPause:1,play:0,playlist:1,lib:1,playPrev:1,playNext:1,stop:0,vol:1,+:1,time:1,hd:1,repeat:1,shuffle:0,pause:0,share:1,fullScreen:1";

    // Reset sliders.
    spider_video_player_reset_theme_slider("spider_video_player_watermarkAlpha", "slider-watermarkAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_centerBtnAlpha", "slider-centerBtnAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_framesBgAlpha", "slider-framesBgAlpha", "65");
    spider_video_player_reset_theme_slider("spider_video_player_ctrlsMainAlpha", "slider-ctrlsMainAlpha", "99");
    spider_video_player_reset_theme_slider("spider_video_player_defaultVol", "slider-defaultVol", "50");
    return true;
  }
  else {
    return false;
  }
}

function spider_video_player_reset_theme_4() {
  if (confirm(Drupal.t('Do you really want to reset theme?'))) {
    // General parametres.
    document.getElementById("edit-appwidth").value = "640";
    document.getElementById("edit-appheight").value = "480";
    document.getElementById("edit-startwithlib-1").checked = false;
    document.getElementById("edit-startwithlib-0").checked = true;
    document.getElementById("edit-show-trackid-1").checked = true;
    document.getElementById("edit-show-trackid-0").checked = false;
    document.getElementById("edit-autohidetime").value = "5";
    document.getElementById("edit-keepaspectratio-1").checked = true;
    document.getElementById("edit-keepaspectratio-0").checked = false;
    document.getElementById("edit-ctrlsovervid-1").checked = true;
    document.getElementById("edit-ctrlsovervid-0").checked = false;
    document.getElementById("thumb").value = "";
    document.getElementById("imagebox").src = "";
    document.getElementById("imagebox").height = "";
    document.getElementById("edit-watermarkpos-1").checked = true;
    document.getElementById("edit-watermarkpos-2").checked = false;
    document.getElementById("edit-watermarkpos-3").checked = false;
    document.getElementById("edit-watermarkpos-4").checked = false;
    document.getElementById("edit-watermarksize").value = "50";
    document.getElementById("edit-watermarkspacing").value = "0";
    document.getElementById("spider_video_player_watermarkAlpha").value = "50";

    // Style parametres.
    document.getElementById("spider_video_player_centerBtnAlpha").value = "60";
    document.getElementById("spider_video_player_framesBgAlpha").value = "71";
    document.getElementById("spider_video_player_ctrlsMainAlpha").value = "82";
    // If jscolor exists.
    if (document.getElementById("edit-appbgcolor").color) {
      document.getElementById("edit-appbgcolor").color.fromString("239DC2");
      document.getElementById("edit-vidbgcolor").color.fromString("000000");
      document.getElementById("edit-framesbgcolor").color.fromString("2E6DFF");
      document.getElementById("edit-ctrlsmaincolor").color.fromString("F5DA51");
      document.getElementById("edit-ctrlsmainhovercolor").color.fromString("FFA64D");
      document.getElementById("edit-slidecolor").color.fromString("BFBA73");
      document.getElementById("edit-itembghovercolor").color.fromString("FF8800");
      document.getElementById("edit-itembgselectedcolor").color.fromString("FFF700");
      document.getElementById("edit-textcolor").color.fromString("FFFFFF");
      document.getElementById("edit-texthovercolor").color.fromString("FFFFFF");
      document.getElementById("edit-textselectedcolor").color.fromString("000000");
    }
    else {
      document.getElementById("edit-appbgcolor").value = "239DC2";
      document.getElementById("edit-vidbgcolor").value = "000000";
      document.getElementById("edit-framesbgcolor").value = "2E6DFF";
      document.getElementById("edit-ctrlsmaincolor").value = "F5DA51";
      document.getElementById("edit-ctrlsmainhovercolor").value = "FFA64D";
      document.getElementById("edit-slidecolor").value = "BFBA73";
      document.getElementById("edit-itembghovercolor").value = "FF8800";
      document.getElementById("edit-itembgselectedcolor").value = "FFF700";
      document.getElementById("edit-textcolor").value = "FFFFFF";
      document.getElementById("edit-texthovercolor").value = "FFFFFF";
      document.getElementById("edit-textselectedcolor").value = "000000";
    }
    document.getElementById("edit-loadinanimtype-1").checked = false;
    document.getElementById("edit-loadinanimtype-2").checked = true;

    // Playback parameters.
    document.getElementById("edit-autoplay-1").checked = false;
    document.getElementById("edit-autoplay-0").checked = true;
    document.getElementById("edit-autonext-1").checked = false;
    document.getElementById("edit-autonext-0").checked = true;
    document.getElementById("edit-autonextalbum-1").checked = false;
    document.getElementById("edit-autonextalbum-0").checked = true;
    document.getElementById("spider_video_player_defaultVol").value = "50";
    document.getElementById("edit-defaultrepeat-repeatone").checked = false;
    document.getElementById("edit-defaultrepeat-repeatoff").checked = true;
    document.getElementById("edit-defaultrepeat-repeatall").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleon").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleoff").checked = true;
    document.getElementById("edit-ctrlsslideout-1").checked = true;
    document.getElementById("edit-ctrlsslideout-0").checked = false;

    // Playlist and Library parameters.
    document.getElementById("edit-playlistpos-1").checked = true;
    document.getElementById("edit-playlistpos-2").checked = false;
    document.getElementById("edit-playlistwidth").value = "150";
    document.getElementById("edit-playlistovervid-1").checked = true;
    document.getElementById("edit-playlistovervid-0").checked = false;
    document.getElementById("edit-playlistautohide-1").checked = true;
    document.getElementById("edit-playlistautohide-0").checked = false;
    document.getElementById("edit-playlisttextsize").value = "14";
    document.getElementById("edit-libcols").value = "4";
    document.getElementById("edit-librows").value = "4";
    document.getElementById("edit-liblisttextsize").value = "14";
    document.getElementById("edit-libdetailstextsize").value = "16";

    // Video Control parameters.
    document.getElementById("edit-clickonvid-1").checked = true;
    document.getElementById("edit-clickonvid-0").checked = false;
    document.getElementById("edit-spaceonvid-1").checked = true;
    document.getElementById("edit-spaceonvid-0").checked = false;
    document.getElementById("edit-mousewheel-1").checked = true;
    document.getElementById("edit-mousewheel-0").checked = false;
    document.getElementById("edit-ctrlspos-1").checked = false;
    document.getElementById("edit-ctrlspos-2").checked = true;
    document.getElementById("ctrlsStack").value = "playPause:1,playlist:1,lib:1,vol:1,playPrev:0,playNext:0,stop:0,+:1,hd:1,repeat:1,shuffle:0,play:0,pause:0,share:1,time:1,fullScreen:1";

    // Reset sliders.
    spider_video_player_reset_theme_slider("spider_video_player_watermarkAlpha", "slider-watermarkAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_centerBtnAlpha", "slider-centerBtnAlpha", "60");
    spider_video_player_reset_theme_slider("spider_video_player_framesBgAlpha", "slider-framesBgAlpha", "71");
    spider_video_player_reset_theme_slider("spider_video_player_ctrlsMainAlpha", "slider-ctrlsMainAlpha", "82");
    spider_video_player_reset_theme_slider("spider_video_player_defaultVol", "slider-defaultVol", "50");
    return true;
  }
  else {
    return false;
  }
}

function spider_video_player_reset_theme_5() {
  if (confirm(Drupal.t('Do you really want to reset theme?'))) {
    // General parametres.
    document.getElementById("edit-appwidth").value = "640";
    document.getElementById("edit-appheight").value = "480";
    document.getElementById("edit-startwithlib-1").checked = false;
    document.getElementById("edit-startwithlib-0").checked = true;
    document.getElementById("edit-show-trackid-1").checked = true;
    document.getElementById("edit-show-trackid-0").checked = false;
    document.getElementById("edit-autohidetime").value = "5";
    document.getElementById("edit-keepaspectratio-1").checked = true;
    document.getElementById("edit-keepaspectratio-0").checked = false;
    document.getElementById("edit-ctrlsovervid-1").checked = true;
    document.getElementById("edit-ctrlsovervid-0").checked = false;
    document.getElementById("thumb").value = "";
    document.getElementById("imagebox").src = "";
    document.getElementById("imagebox").height = "";
    document.getElementById("edit-watermarkpos-1").checked = true;
    document.getElementById("edit-watermarkpos-2").checked = false;
    document.getElementById("edit-watermarkpos-3").checked = false;
    document.getElementById("edit-watermarkpos-4").checked = false;
    document.getElementById("edit-watermarksize").value = "50";
    document.getElementById("edit-watermarkspacing").value = "0";
    document.getElementById("spider_video_player_watermarkAlpha").value = "50";

    // Style parametres.
    document.getElementById("spider_video_player_centerBtnAlpha").value = "50";
    document.getElementById("spider_video_player_framesBgAlpha").value = "100";
    document.getElementById("spider_video_player_ctrlsMainAlpha").value = "75";
    // If jscolor exists.
    if (document.getElementById("edit-appbgcolor").color) {
      document.getElementById("edit-appbgcolor").color.fromString("878787");
      document.getElementById("edit-vidbgcolor").color.fromString("001326");
      document.getElementById("edit-framesbgcolor").color.fromString("FFFFFF");
      document.getElementById("edit-ctrlsmaincolor").color.fromString("000000");
      document.getElementById("edit-ctrlsmainhovercolor").color.fromString("525252");
      document.getElementById("edit-slidecolor").color.fromString("14B1FF");
      document.getElementById("edit-itembghovercolor").color.fromString("CCCCCC");
      document.getElementById("edit-itembgselectedcolor").color.fromString("14B1FF");
      document.getElementById("edit-textcolor").color.fromString("030303");
      document.getElementById("edit-texthovercolor").color.fromString("000000");
      document.getElementById("edit-textselectedcolor").color.fromString("FFFFFF");
    }
    else {
      document.getElementById("edit-appbgcolor").value = "878787";
      document.getElementById("edit-vidbgcolor").value = "001326";
      document.getElementById("edit-framesbgcolor").value = "FFFFFF";
      document.getElementById("edit-ctrlsmaincolor").value = "000000";
      document.getElementById("edit-ctrlsmainhovercolor").value = "525252";
      document.getElementById("edit-slidecolor").value = "14B1FF";
      document.getElementById("edit-itembghovercolor").value = "CCCCCC";
      document.getElementById("edit-itembgselectedcolor").value = "14B1FF";
      document.getElementById("edit-textcolor").value = "030303";
      document.getElementById("edit-texthovercolor").value = "000000";
      document.getElementById("edit-textselectedcolor").value = "FFFFFF";
    }
    document.getElementById("edit-loadinanimtype-1").checked = false;
    document.getElementById("edit-loadinanimtype-2").checked = true;

    // Playback parameters.
    document.getElementById("edit-autoplay-1").checked = false;
    document.getElementById("edit-autoplay-0").checked = true;
    document.getElementById("edit-autonext-1").checked = false;
    document.getElementById("edit-autonext-0").checked = true;
    document.getElementById("edit-autonextalbum-1").checked = false;
    document.getElementById("edit-autonextalbum-0").checked = true;
    document.getElementById("spider_video_player_defaultVol").value = "50";
    document.getElementById("edit-defaultrepeat-repeatone").checked = false;
    document.getElementById("edit-defaultrepeat-repeatoff").checked = true;
    document.getElementById("edit-defaultrepeat-repeatall").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleon").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleoff").checked = true;
    document.getElementById("edit-ctrlsslideout-1").checked = true;
    document.getElementById("edit-ctrlsslideout-0").checked = false;

    // Playlist and Library parameters.
    document.getElementById("edit-playlistpos-1").checked = true;
    document.getElementById("edit-playlistpos-2").checked = false;
    document.getElementById("edit-playlistwidth").value = "140";
    document.getElementById("edit-playlistovervid-1").checked = true;
    document.getElementById("edit-playlistovervid-0").checked = false;
    document.getElementById("edit-playlistautohide-1").checked = true;
    document.getElementById("edit-playlistautohide-0").checked = false;
    document.getElementById("edit-playlisttextsize").value = "14";
    document.getElementById("edit-libcols").value = "4";
    document.getElementById("edit-librows").value = "4";
    document.getElementById("edit-liblisttextsize").value = "14";
    document.getElementById("edit-libdetailstextsize").value = "16";

    // Video Control parameters.
    document.getElementById("edit-clickonvid-1").checked = true;
    document.getElementById("edit-clickonvid-0").checked = false;
    document.getElementById("edit-spaceonvid-1").checked = true;
    document.getElementById("edit-spaceonvid-0").checked = false;
    document.getElementById("edit-mousewheel-1").checked = true;
    document.getElementById("edit-mousewheel-0").checked = false;
    document.getElementById("edit-ctrlspos-1").checked = false;
    document.getElementById("edit-ctrlspos-2").checked = true;
    document.getElementById("ctrlsStack").value = "playPrev:0,playPause:1,playlist:1,lib:1,playNext:0,stop:0,time:1,vol:1,+:1,hd:1,repeat:1,shuffle:1,play:0,pause:0,share:1,fullScreen:1";

    // Reset sliders.
    spider_video_player_reset_theme_slider("spider_video_player_watermarkAlpha", "slider-watermarkAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_centerBtnAlpha", "slider-centerBtnAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_framesBgAlpha", "slider-framesBgAlpha", "100");
    spider_video_player_reset_theme_slider("spider_video_player_ctrlsMainAlpha", "slider-ctrlsMainAlpha", "75");
    spider_video_player_reset_theme_slider("spider_video_player_defaultVol", "slider-defaultVol", "50");
    return true;
  }
  else {
    return false;
  }
}

function spider_video_player_reset_theme_6() {
  if (confirm(Drupal.t('Do you really want to reset theme?'))) {
    // General parametres.
    document.getElementById("edit-appwidth").value = "640";
    document.getElementById("edit-appheight").value = "480";
    document.getElementById("edit-startwithlib-1").checked = false;
    document.getElementById("edit-startwithlib-0").checked = true;
    document.getElementById("edit-show-trackid-1").checked = true;
    document.getElementById("edit-show-trackid-0").checked = false;
    document.getElementById("edit-autohidetime").value = "5";
    document.getElementById("edit-keepaspectratio-1").checked = true;
    document.getElementById("edit-keepaspectratio-0").checked = false;
    document.getElementById("edit-ctrlsovervid-1").checked = true;
    document.getElementById("edit-ctrlsovervid-0").checked = false;
    document.getElementById("thumb").value = "";
    document.getElementById("imagebox").src = "";
    document.getElementById("imagebox").height = "";
    document.getElementById("edit-watermarkpos-1").checked = true;
    document.getElementById("edit-watermarkpos-2").checked = false;
    document.getElementById("edit-watermarkpos-3").checked = false;
    document.getElementById("edit-watermarkpos-4").checked = false;
    document.getElementById("edit-watermarksize").value = "50";
    document.getElementById("edit-watermarkspacing").value = "0";
    document.getElementById("spider_video_player_watermarkAlpha").value = "50";

    // Style parametres.
    document.getElementById("spider_video_player_centerBtnAlpha").value = "50";
    document.getElementById("spider_video_player_framesBgAlpha").value = "61";
    document.getElementById("spider_video_player_ctrlsMainAlpha").value = "79";
    // If jscolor exists.
    if (document.getElementById("edit-appbgcolor").color) {
      document.getElementById("edit-appbgcolor").color.fromString("080808");
      document.getElementById("edit-vidbgcolor").color.fromString("000000");
      document.getElementById("edit-framesbgcolor").color.fromString("1C1C1C");
      document.getElementById("edit-ctrlsmaincolor").color.fromString("FFFFFF");
      document.getElementById("edit-ctrlsmainhovercolor").color.fromString("40C6FF");
      document.getElementById("edit-slidecolor").color.fromString("00A2FF");
      document.getElementById("edit-itembghovercolor").color.fromString("E8E8E8");
      document.getElementById("edit-itembgselectedcolor").color.fromString("40C6FF");
      document.getElementById("edit-textcolor").color.fromString("DEDEDE");
      document.getElementById("edit-texthovercolor").color.fromString("2E2E2E");
      document.getElementById("edit-textselectedcolor").color.fromString("FFFFFF");
    }
    else {
      document.getElementById("edit-appbgcolor").value = "080808";
      document.getElementById("edit-vidbgcolor").value = "000000";
      document.getElementById("edit-framesbgcolor").value = "1C1C1C";
      document.getElementById("edit-ctrlsmaincolor").value = "FFFFFF";
      document.getElementById("edit-ctrlsmainhovercolor").value = "40C6FF";
      document.getElementById("edit-slidecolor").value = "00A2FF";
      document.getElementById("edit-itembghovercolor").value = "E8E8E8";
      document.getElementById("edit-itembgselectedcolor").value = "40C6FF";
      document.getElementById("edit-textcolor").value = "DEDEDE";
      document.getElementById("edit-texthovercolor").value = "2E2E2E";
      document.getElementById("edit-textselectedcolor").value = "FFFFFF";
    }
    document.getElementById("edit-loadinanimtype-1").checked = false;
    document.getElementById("edit-loadinanimtype-2").checked = true;

    // Playback parameters.
    document.getElementById("edit-autoplay-1").checked = false;
    document.getElementById("edit-autoplay-0").checked = true;
    document.getElementById("edit-autonext-1").checked = false;
    document.getElementById("edit-autonext-0").checked = true;
    document.getElementById("edit-autonextalbum-1").checked = false;
    document.getElementById("edit-autonextalbum-0").checked = true;
    document.getElementById("spider_video_player_defaultVol").value = "50";
    document.getElementById("edit-defaultrepeat-repeatone").checked = false;
    document.getElementById("edit-defaultrepeat-repeatoff").checked = true;
    document.getElementById("edit-defaultrepeat-repeatall").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleon").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleoff").checked = true;
    document.getElementById("edit-ctrlsslideout-1").checked = true;
    document.getElementById("edit-ctrlsslideout-0").checked = false;

    // Playlist and Library parameters.
    document.getElementById("edit-playlistpos-1").checked = true;
    document.getElementById("edit-playlistpos-2").checked = false;
    document.getElementById("edit-playlistwidth").value = "120";
    document.getElementById("edit-playlistovervid-1").checked = true;
    document.getElementById("edit-playlistovervid-0").checked = false;
    document.getElementById("edit-playlistautohide-1").checked = true;
    document.getElementById("edit-playlistautohide-0").checked = false;
    document.getElementById("edit-playlisttextsize").value = "14";
    document.getElementById("edit-libcols").value = "3";
    document.getElementById("edit-librows").value = "3";
    document.getElementById("edit-liblisttextsize").value = "16";
    document.getElementById("edit-libdetailstextsize").value = "16";

    // Video Control parameters.
    document.getElementById("edit-clickonvid-1").checked = true;
    document.getElementById("edit-clickonvid-0").checked = false;
    document.getElementById("edit-spaceonvid-1").checked = true;
    document.getElementById("edit-spaceonvid-0").checked = false;
    document.getElementById("edit-mousewheel-1").checked = true;
    document.getElementById("edit-mousewheel-0").checked = false;
    document.getElementById("edit-ctrlspos-1").checked = false;
    document.getElementById("edit-ctrlspos-2").checked = true;
    document.getElementById("ctrlsStack").value = "playPause:1,playlist:1,lib:1,vol:1,playPrev:0,playNext:0,stop:0,+:1,repeat:0,shuffle:0,play:0,pause:0,hd:1,share:1,time:1,fullScreen:1";

    // Reset sliders.
    spider_video_player_reset_theme_slider("spider_video_player_watermarkAlpha", "slider-watermarkAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_centerBtnAlpha", "slider-centerBtnAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_framesBgAlpha", "slider-framesBgAlpha", "61");
    spider_video_player_reset_theme_slider("spider_video_player_ctrlsMainAlpha", "slider-ctrlsMainAlpha", "79");
    spider_video_player_reset_theme_slider("spider_video_player_defaultVol", "slider-defaultVol", "50");
    return true;
  }
  else {
    return false;
  }
}

function spider_video_player_reset_theme_7() {
  if (confirm(Drupal.t('Do you really want to reset theme?'))) {
    // General parametres.
    document.getElementById("edit-appwidth").value = "640";
    document.getElementById("edit-appheight").value = "480";
    document.getElementById("edit-startwithlib-1").checked = false;
    document.getElementById("edit-startwithlib-0").checked = true;
    document.getElementById("edit-show-trackid-1").checked = true;
    document.getElementById("edit-show-trackid-0").checked = false;
    document.getElementById("edit-autohidetime").value = "5";
    document.getElementById("edit-keepaspectratio-1").checked = true;
    document.getElementById("edit-keepaspectratio-0").checked = false;
    document.getElementById("edit-ctrlsovervid-1").checked = true;
    document.getElementById("edit-ctrlsovervid-0").checked = false;
    document.getElementById("thumb").value = "";
    document.getElementById("imagebox").src = "";
    document.getElementById("imagebox").height = "";
    document.getElementById("edit-watermarkpos-1").checked = true;
    document.getElementById("edit-watermarkpos-2").checked = false;
    document.getElementById("edit-watermarkpos-3").checked = false;
    document.getElementById("edit-watermarkpos-4").checked = false;
    document.getElementById("edit-watermarksize").value = "50";
    document.getElementById("edit-watermarkspacing").value = "0";
    document.getElementById("spider_video_player_watermarkAlpha").value = "50";

    // Style parametres.
    document.getElementById("spider_video_player_centerBtnAlpha").value = "50";
    document.getElementById("spider_video_player_framesBgAlpha").value = "90";
    document.getElementById("spider_video_player_ctrlsMainAlpha").value = "78";
    // If jscolor exists.
    if (document.getElementById("edit-appbgcolor").color) {
      document.getElementById("edit-appbgcolor").color.fromString("212121");
      document.getElementById("edit-vidbgcolor").color.fromString("000000");
      document.getElementById("edit-framesbgcolor").color.fromString("222424");
      document.getElementById("edit-ctrlsmaincolor").color.fromString("FFCC00");
      document.getElementById("edit-ctrlsmainhovercolor").color.fromString("FFFFFF");
      document.getElementById("edit-slidecolor").color.fromString("ABABAB");
      document.getElementById("edit-itembghovercolor").color.fromString("B8B8B8");
      document.getElementById("edit-itembgselectedcolor").color.fromString("EEFF00");
      document.getElementById("edit-textcolor").color.fromString("DEDEDE");
      document.getElementById("edit-texthovercolor").color.fromString("000000");
      document.getElementById("edit-textselectedcolor").color.fromString("000000");
    }
    else {
      document.getElementById("edit-appbgcolor").value = "212121";
      document.getElementById("edit-vidbgcolor").value = "000000";
      document.getElementById("edit-framesbgcolor").value = "222424";
      document.getElementById("edit-ctrlsmaincolor").value = "FFCC00";
      document.getElementById("edit-ctrlsmainhovercolor").value = "FFFFFF";
      document.getElementById("edit-slidecolor").value = "ABABAB";
      document.getElementById("edit-itembghovercolor").value = "B8B8B8";
      document.getElementById("edit-itembgselectedcolor").value = "EEFF00";
      document.getElementById("edit-textcolor").value = "DEDEDE";
      document.getElementById("edit-texthovercolor").value = "000000";
      document.getElementById("edit-textselectedcolor").value = "000000";
    }
    document.getElementById("edit-loadinanimtype-1").checked = false;
    document.getElementById("edit-loadinanimtype-2").checked = true;

    // Playback parameters.
    document.getElementById("edit-autoplay-1").checked = false;
    document.getElementById("edit-autoplay-0").checked = true;
    document.getElementById("edit-autonext-1").checked = false;
    document.getElementById("edit-autonext-0").checked = true;
    document.getElementById("edit-autonextalbum-1").checked = false;
    document.getElementById("edit-autonextalbum-0").checked = true;
    document.getElementById("spider_video_player_defaultVol").value = "50";
    document.getElementById("edit-defaultrepeat-repeatone").checked = false;
    document.getElementById("edit-defaultrepeat-repeatoff").checked = true;
    document.getElementById("edit-defaultrepeat-repeatall").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleon").checked = false;
    document.getElementById("edit-defaultshuffle-shuffleoff").checked = true;
    document.getElementById("edit-ctrlsslideout-1").checked = true;
    document.getElementById("edit-ctrlsslideout-0").checked = false;

    // Playlist and Library parameters.
    document.getElementById("edit-playlistpos-1").checked = true;
    document.getElementById("edit-playlistpos-2").checked = false;
    document.getElementById("edit-playlistwidth").value = "120";
    document.getElementById("edit-playlistovervid-1").checked = true;
    document.getElementById("edit-playlistovervid-0").checked = false;
    document.getElementById("edit-playlistautohide-1").checked = true;
    document.getElementById("edit-playlistautohide-0").checked = false;
    document.getElementById("edit-playlisttextsize").value = "12";
    document.getElementById("edit-libcols").value = "3";
    document.getElementById("edit-librows").value = "3";
    document.getElementById("edit-liblisttextsize").value = "16";
    document.getElementById("edit-libdetailstextsize").value = "16";

    // Video Control parameters.
    document.getElementById("edit-clickonvid-1").checked = true;
    document.getElementById("edit-clickonvid-0").checked = false;
    document.getElementById("edit-spaceonvid-1").checked = true;
    document.getElementById("edit-spaceonvid-0").checked = false;
    document.getElementById("edit-mousewheel-1").checked = true;
    document.getElementById("edit-mousewheel-0").checked = false;
    document.getElementById("edit-ctrlspos-1").checked = false;
    document.getElementById("edit-ctrlspos-2").checked = true;
    document.getElementById("ctrlsStack").value = "playPause:1,playlist:1,lib:1,playPrev:0,playNext:0,stop:0,vol:1,+:1,hd:0,repeat:0,shuffle:0,play:0,pause:0,share:1,fullScreen:1,time:1";

    // Reset sliders.
    spider_video_player_reset_theme_slider("spider_video_player_watermarkAlpha", "slider-watermarkAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_centerBtnAlpha", "slider-centerBtnAlpha", "50");
    spider_video_player_reset_theme_slider("spider_video_player_framesBgAlpha", "slider-framesBgAlpha", "90");
    spider_video_player_reset_theme_slider("spider_video_player_ctrlsMainAlpha", "slider-ctrlsMainAlpha", "78");
    spider_video_player_reset_theme_slider("spider_video_player_defaultVol", "slider-defaultVol", "50");
    return true;
  }
  else {
    return false;
  }
}

/**
 * Change slider deppend on input percent.
 */
function spider_video_player_change_theme_slider(idtaginp, idtagdiv) {
  var inpvalue = jQuery("#" + idtaginp).val();
  if (inpvalue == "") {
    inpvalue = 28;
  }
  jQuery("#" + idtagdiv).slider({
    range:"min",
    value:inpvalue,
    min:0,
    max:100,
    slide:function (event, ui) {
      jQuery("#" + idtaginp).val("" + ui.value);
    }
  });
  jQuery("#" + idtaginp).val("" + jQuery("#" + idtagdiv).slider("value"));
}

/**
 * Changed sequence write in hidden input value.
 */
function spider_video_player_refresh_ctrl() {
  var ctrlStack = "";
  var w = document.getElementById("tr_arr").childNodes;
  for (var i in w) {
    if (IsNumeric(i)) {
      if (w[i].nodeType != 3) {
        if (ctrlStack == "") {
          ctrlStack = w[i].getAttribute("value") + ":" + w[i].getAttribute("active");
        }
        else {
          ctrlStack = ctrlStack + "," + w[i].getAttribute("value") + ":" + w[i].getAttribute("active");
        }
      }
    }
  }
  document.getElementById("ctrlsStack").value = ctrlStack;
}

function IsNumeric(input) {
  var RE = /^-{0,1}\d*\.{0,1}\d+$/;
  return (RE.test(input));
}

/**
 * Open iframe.
 */
function spider_video_player_iframe_preview(iframe_div, iframe_src, close_url) {
  var width = parseInt(document.getElementById('edit-appwidth').value);
  var height = parseInt(document.getElementById('edit-appheight').value);
  if (!height) {
    height = 400;
  }
  if (!width) {
    width = 700;
  }
  height = height + 20;
  width = width + 20;
  var div = document.getElementById(iframe_div);
  div.setAttribute('style', 'position:fixed; width:100%; height:100%; top:50px; left:0;');
  div.innerHTML = '<div id="div_black" style="position:fixed; top:0; left:0; width:100%; height:100%; background-color:#000000; opacity:0.80; z-index:10000000;"></div><div id="div_content" style="position:relative; top:70px; width:' + width + 'px; height:' + height + 'px; margin:0 auto;z-index:10000001;background-color:#FFFFFF;"><iframe width="' + width + '" height="' + height + '" src="' + iframe_src + '"></iframe></div><div onclick=\'spider_video_player_remove_iframe_preview("' + iframe_div + '")\' style=\'position:fixed; z-index:10000002; margin: 0 auto; right:20%; top:80px; width:32px; height:32px; background-image:url("' + close_url + '"); \'></div>';
}

/**
 * Close iframe.
 */
function spider_video_player_remove_iframe_preview(iframe_div) {
  var div = document.getElementById(iframe_div);
  div.setAttribute('style', '');
  div.innerHTML = '';
}
