/**
 * Change action type.
 */
function spider_video_player_change_video_type(checked_radio_id) {
  switch (checked_radio_id) {
    case "edit-video-type-http":
      document.getElementById("video_http").style.display = "";      document.getElementById("video_youtube").style.display = "none";      document.getElementById("video_rtmp").style.display = "none";
      document.getElementById("video_radio_type_hidden").value = "http";
      break;
    case "edit-video-type-youtube":
      document.getElementById("video_http").style.display = "none";      document.getElementById("video_youtube").style.display = "";      document.getElementById("video_rtmp").style.display = "none";      document.getElementById("video_radio_type_hidden").value = "youtube";
      break;
    case "edit-video-type-rtmp":
      document.getElementById("video_http").style.display = "none";      document.getElementById("video_youtube").style.display = "none";      document.getElementById("video_rtmp").style.display = "";      document.getElementById("video_radio_type_hidden").value = "rtmp";
      break;  }
}
/**
 * Show/hide file uploader.
 */
function spider_video_player_file_upload(is_visible) {
  document.getElementById("video_iframe").style.display = is_visible;
}

/**
 * Show/hide image uploader.
 */
function spider_video_player_image_upload(is_visible) {
  document.getElementById("image_iframe").style.display = is_visible;
}

/**
 * Set video type.
 */
function spider_video_player_set_video_type(type) {
  document.getElementById("video_type_hidden").value = type;
}

/**
 * Remove video.
 */
function spider_video_player_remove_video(id) {
  document.getElementById(id + "_link").innerHTML = 'Select Video';
  document.getElementById(id).value = '';
}

/**
 * Remove image.
 */
function spider_video_player_remove_image(id) {
  document.getElementById("imagebox").style.display = "none";
  document.getElementById("thumb").value = "";
}
