/**
 * Open iframe.
 */
function spider_video_player_iframe(iframe_div, iframe_src, close_url) {
  var height = 400 + 20;
  var width = 700 + 20;
  var div = document.getElementById(iframe_div);
  div.setAttribute('style', 'position:fixed; width:100%; height:100%; top:50px; left:0;z-index: 10000001;');
  div.innerHTML = '<div id="div_black" style="position:fixed; top:0; left:0; width:100%; height:100%; background-color:#000000; opacity:0.80; z-index:10000000;"></div><div id="div_content" style="position:relative; top:70px; width:' + width + 'px; height:' + height + 'px; margin:0 auto;z-index:10000001;background-color:#FFFFFF;"><iframe width="' + width + '" height="' + height + '" src="' + iframe_src + '"></iframe></div><div onclick=\'spider_video_player_remove_iframe(document.getElementById("' + iframe_div + '"))\' style=\'position:fixed; z-index:10000002; margin: 0 auto; right:20%; top:80px; width:32px; height:32px; background-image:url("' + close_url + '"); \'></div>';
}

/**
 * Close iframe.
 */
function spider_video_player_remove_iframe(iframe_div) {
  iframe_div.setAttribute('style', '');
  iframe_div.innerHTML = '';
}
